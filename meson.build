project('glib-android-ssl', 'c',
        version: '2.50.8',
        license: 'LGPL2+',
        meson_version: '>= 0.47.0')

cc = meson.get_compiler('c')

glib_req = '2.46.0'
glib = dependency('glib-2.0', version: '>=' + glib_req)
gobject = dependency('gobject-2.0', version: '>=' + glib_req)
gio = dependency('gio-2.0', version: '>=' + glib_req)

giomoduledir = gio.get_pkgconfig_variable('giomoduledir',
                                            define_variable: ['libdir', get_option('libdir')])
assert(giomoduledir != '', 'GIO_MODULE_DIR is missing from gio-2.0.pc')

gio_querymodules = find_program('gio-querymodules')


test_cflags = [
  '-ffast-math',
  '-fstrict-aliasing',
  '-Wpointer-arith',
  '-Wmissing-declarations',
  '-Wformat=2',
  '-Wstrict-prototypes',
  '-Wmissing-prototypes',
  '-Wnested-externs',
  '-Wold-style-definition',
  '-Wdeclaration-after-statement',
  '-Wunused',
  '-Wuninitialized',
  '-Wshadow',
  '-Wmissing-noreturn',
  '-Wmissing-format-attribute',
  '-Wredundant-decls',
  '-Wlogical-op',
  '-Wcast-align',
  '-Wno-unused-local-typedefs',
  '-Werror=implicit',
  '-Werror=init-self',
  '-Werror=main',
  '-Werror=missing-braces',
  '-Werror=return-type',
  '-Werror=array-bounds',
  '-Werror=write-strings'
]

common_flags = []
foreach cflag: test_cflags
  if cc.has_argument(cflag)
    common_flags += [ cflag ]
  endif
endforeach

extra_args= []
# Detect and set symbol visibility
if cc.get_id() == 'msvc'
  extra_args += ['-D_GLIB_EXTERN=__declspec (dllexport) extern']
endif

config_h = configuration_data()
config_h.set_quoted('GETTEXT_PACKAGE', meson.project_name())
config_h.set_quoted('LOCALE_DIR', join_paths(get_option('prefix'), get_option('localedir')))
config_h.set('G_DISABLE_DEPRECATED', true)
config_h.set_quoted('G_LOG_DOMAIN', 'GLib-AndroidSSL')

configure_file(output: 'config.h',
               configuration: config_h)
config_h_include = include_directories('.')

subdir('tls/base')
subdir('tls/android')
subdir('tls/tests')

meson.add_install_script(join_paths('meson_post_install.py'), gio_querymodules.path(), giomoduledir)
