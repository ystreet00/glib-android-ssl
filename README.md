# glib-android-ssl
GLib GIO TLS backend using the JSSE API

[glib-networking](https://git.gnome.org/browse/glib-networking) already provides a GIO module that implements a [GIO TLS backend](https://developer.gnome.org/gio/stable/GTlsBackend.html), but it uses [GnuTLS](https://en.wikipedia.org/wiki/GnuTLS).

This project builds a GIO module that uses the [TLS implementation provided by Java as used/provided by Android and Oracle/Sun](https://docs.oracle.com/javase/8/docs/technotes/guides/security/jsse/JSSERefGuide.html).

Advantages include reduced external dependencies, easier security updates, and better integration with the Android certificate store.

See also [glib-openssl](https://gitlab.gnome.org/GNOME/glib-openssl/) which does the same but uses OpenSSL.

# Issues

PKCS interface compatibility, this implementation only provides support for
PKCS#8 keys and certificates ("BEGIN PRIVATE KEY").

There are various issues around client authentication and interface mismatches
between glib and JSSE which means client authenticiation is a work in progress.

Server Name Identification on Android requires API Level >= 24 to implement and
is currently not implemented.  The Oracle/Sun implementation automatically adds
SNI already.
