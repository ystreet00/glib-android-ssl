/*
 * gtlscontext-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlscontext-android.h"

#include "gtlsjniutils.h"

static GTlsContextClass default_context;

gboolean
context_initialize (GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *my_error = NULL;

  /* javax.net.ssl.SSLContext */
  if (!(default_context.klass =
      g_tls_jni_get_class (env,
                           &my_error,
                           "javax/net/ssl/SSLContext"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'javax.net.ssl.SSLContext\' class");
    return FALSE;
  }

  if (!(default_context.get_instance =
      g_tls_jni_get_static_method_id (env,
                               &my_error,
                               default_context.klass,
                               "getInstance",
                               "(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'SSLContext getInstance(String)\' on "
        "\'javax.net.ssl.SSLContext\'");
    return FALSE;
  }

  if (!(default_context.init =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_context.klass,
                               "init",
                               "([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'void init(KeyManager[], TrustManager[], SecureRandom)\' on "
        "\'javax.net.ssl.SSLContext\'");
    return FALSE;
  }

  if (!(default_context.create_ssl_engine =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_context.klass,
                               "createSSLEngine",
                               "()Ljavax/net/ssl/SSLEngine;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'SSLEngine createSSLEngine()\' on "
        "\'javax.net.ssl.SSLContext\'");
    return FALSE;
  }

  if (!(default_context.create_ssl_engine_with_peer =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_context.klass,
                               "createSSLEngine",
                               "(Ljava/lang/String;I)Ljavax/net/ssl/SSLEngine;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'SSLEngine createSSLEngine(String, int)\' on "
        "\'javax.net.ssl.SSLContext\'");
    return FALSE;
  }

  return TRUE;
}

GTlsContextClass *
context_get (void)
{
  return &default_context;
}

jobject
context_get_instance (jobject name)
{
  JNIEnv *env = g_tls_jni_get_env ();
  jobject instance;
  GError *error = NULL;

  if (!g_tls_jni_call_static_object_method (env, &error,
            default_context.klass,
            default_context.get_instance,
            &instance,
            name)) {
    g_critical ("Failed to retrieve SSLContext instance: %s", error->message);
    return NULL;
  }

  return instance;
}

void
context_init (jobject context, jobjectArray keymanagers, jobjectArray trustmanagers, jobject secure_random)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env, &error,
            context,
            default_context.init,
            keymanagers,
            trustmanagers,
            secure_random)) {
    g_critical ("Failed to initialize SSLContext: %s", error->message);
    return;
  }
}

jobject
context_create_ssl_engine (jobject context)
{
  JNIEnv *env = g_tls_jni_get_env ();
  jobject engine;
  GError *error = NULL;

  if (!g_tls_jni_call_object_method (env, &error,
            context,
            default_context.create_ssl_engine,
            &engine)) {
    g_critical ("Failed to create SSLEngine: %s", error->message);
    return NULL;
  }

  return engine;
}

jobject
context_create_ssl_engine_with_peer (jobject context, jobject host, jint port)
{
  JNIEnv *env = g_tls_jni_get_env ();
  jobject engine;
  GError *error = NULL;

  if (!g_tls_jni_call_object_method (env, &error,
            context,
            default_context.create_ssl_engine_with_peer,
            &engine,
            host,
            port)) {
    g_critical ("Failed to create SSLEngine: %s", error->message);
    return NULL;
  }

  return engine;
}
