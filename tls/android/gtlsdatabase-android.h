/*
 * gtlsdatabase-android.h
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __G_TLS_DATABASE_ANDROID_H__
#define __G_TLS_DATABASE_ANDROID_H__

#include <gio/gio.h>

#include "gtlscertificate-android.h"
#include "gtlskeystore-android.h"
#include <jni.h>

G_BEGIN_DECLS

#define G_TYPE_TLS_DATABASE_ANDROID            (g_tls_database_android_get_type ())
#define G_TLS_DATABASE_ANDROID(inst)           (G_TYPE_CHECK_INSTANCE_CAST ((inst), G_TYPE_TLS_DATABASE_ANDROID, GTlsDatabaseAndroid))
#define G_TLS_DATABASE_ANDROID_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class), G_TYPE_TLS_DATABASE_ANDROID, GTlsDatabaseAndroidClass))
#define G_IS_TLS_DATABASE_ANDROID(inst)        (G_TYPE_CHECK_INSTANCE_TYPE ((inst), G_TYPE_TLS_DATABASE_ANDROID))
#define G_IS_TLS_DATABASE_ANDROID_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class), G_TYPE_TLS_DATABASE_ANDROID))
#define G_TLS_DATABASE_ANDROID_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS ((inst), G_TYPE_TLS_DATABASE_ANDROID, GTlsDatabaseAndroidClass))

typedef struct _GTlsDatabaseAndroidClass GTlsDatabaseAndroidClass;
typedef struct _GTlsDatabaseAndroid      GTlsDatabaseAndroid;

struct _GTlsDatabaseAndroidClass
{
  GTlsDatabaseClass parent_class;
};

struct _GTlsDatabaseAndroid
{
  GTlsDatabase parent_instance;

  /* java.security.KeyStore */
  jobject keystore;

  /* protected by mutex */
  GMutex mutex;

  /*
   * These are hash tables of gulong -> GPtrArray<GBytes>. The values of
   * the ptr array are full DER encoded certificate values. The keys are byte
   * arrays containing either subject DNs, issuer DNs, or full DER encoded certs
   */
  GHashTable *subjects;
  GHashTable *issuers;

  /*
   * This is a table of GBytes -> GBytes. The values and keys are
   * DER encoded certificate values.
   */
  GHashTable *complete;

  /*
   * This is a table of gchar * -> GTlsCertificate.
   */
  GHashTable *certs_by_handle;
};

GType           g_tls_database_android_get_type             (void) G_GNUC_CONST;

gboolean        g_tls_database_android_load_keystore       (GTlsDatabaseAndroid * android,
                                                            jobject keystore,
                                                            const gchar *handle_prefix,
                                                            GError ** error);

void            g_tls_database_android_copy_certs_to_keystore   (GTlsDatabaseAndroid * android,
                                                                jobject keystore);

gchar *         g_tls_database_android_create_handle_for_certificate (const gchar *prefix, GBytes *der);

G_END_DECLS

#endif /* __G_TLS_DATABASE_ANDROID_H___ */
