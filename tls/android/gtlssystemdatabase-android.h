/*
 * gtlssystemdatabase-android.h
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This system is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __G_TLS_SYSTEM_DATABASE_ANDROID_H__
#define __G_TLS_SYSTEM_DATABASE_ANDROID_H__

#include <gio/gio.h>

#include "gtlsdatabase-android.h"

G_BEGIN_DECLS

#define G_TYPE_TLS_SYSTEM_DATABASE_ANDROID            (g_tls_system_database_android_get_type ())
#define G_TLS_SYSTEM_DATABASE_ANDROID(inst)           (G_TYPE_CHECK_INSTANCE_CAST ((inst), G_TYPE_TLS_SYSTEM_DATABASE_ANDROID, GTlsSystemDatabaseAndroid))
#define G_TLS_SYSTEM_DATABASE_ANDROID_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class), G_TYPE_TLS_SYSTEM_DATABASE_ANDROID, GTlsSystemDatabaseAndroidClass))
#define G_IS_TLS_SYSTEM_DATABASE_ANDROID(inst)        (G_TYPE_CHECK_INSTANCE_TYPE ((inst), G_TYPE_TLS_SYSTEM_DATABASE_ANDROID))
#define G_IS_TLS_SYSTEM_DATABASE_ANDROID_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class), G_TYPE_TLS_SYSTEM_DATABASE_ANDROID))
#define G_TLS_SYSTEM_DATABASE_ANDROID_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS ((inst), G_TYPE_TLS_SYSTEM_DATABASE_ANDROID, GTlsSystemDatabaseAndroidClass))

typedef struct _GTlsSystemDatabaseAndroidClass GTlsSystemDatabaseAndroidClass;
typedef struct _GTlsSystemDatabaseAndroid      GTlsSystemDatabaseAndroid;

struct _GTlsSystemDatabaseAndroidClass
{
  GTlsDatabaseAndroidClass parent_class;
};

struct _GTlsSystemDatabaseAndroid
{
  GTlsDatabaseAndroid parent_instance;
};

GType                        g_tls_system_database_android_get_type              (void) G_GNUC_CONST;
GTlsDatabase *               g_tls_system_database_android_new (void);

G_END_DECLS

#endif /* __G_TLS_SYSTEM_DATABASE_ANDROID_H___ */
