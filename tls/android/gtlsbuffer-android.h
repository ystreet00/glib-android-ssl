/*
 * gtlsbuffer-android.h
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __G_TLS_BUFFER_ANDROID_H__
#define __G_TLS_BUFFER_ANDROID_H__

#include <gio/gio.h>
#include <jni.h>

G_BEGIN_DECLS

typedef struct _GTlsBufferClass
{
  jclass klass;
  jmethodID set_limit;
  jmethodID get_limit;
  jmethodID get_position;
  jmethodID set_position;

  jclass byte_buffer_klass;
  jmethodID compact;
} GTlsBufferClass;

typedef struct _GTlsJniBuffer
{
  jobject           buffer;       /* A direct ByteBuffer */

  guint8           *data;
  gsize             size;
  GDestroyNotify    free_func;
} GTlsJniBuffer;

gboolean            buffer_initialize     (GError ** error);
GTlsBufferClass *   buffer_get            (void);

GTlsJniBuffer *     g_tls_jni_buffer_new            (gsize size);
GTlsJniBuffer *     g_tls_jni_buffer_new_wrapped    (guint8 * data, gsize size, GDestroyNotify free_func);
void                g_tls_jni_buffer_free           (GTlsJniBuffer * buffer);

void                g_tls_jni_buffer_set_limit      (GTlsJniBuffer * buffer, gsize limit);
gsize               g_tls_jni_buffer_get_limit      (GTlsJniBuffer * buffer);
void                g_tls_jni_buffer_set_position   (GTlsJniBuffer * buffer, gsize position);
gsize               g_tls_jni_buffer_get_position   (GTlsJniBuffer * buffer);
void                g_tls_jni_buffer_compact        (GTlsJniBuffer * buffer);

guint8 *            g_tls_jni_buffer_get_data       (GTlsJniBuffer * buffer, gsize * size);

G_END_DECLS

#endif /* __G_TLS_BUFFER_ANDROID_H___ */
