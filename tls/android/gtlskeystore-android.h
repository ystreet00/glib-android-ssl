/*
 * gtlskeystore-android.h
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __G_TLS_KEYSTORE_ANDROID_H__
#define __G_TLS_KEYSTORE_ANDROID_H__

#include <gio/gio.h>
#include <jni.h>

G_BEGIN_DECLS

typedef struct _GTlsKeystoreClass
{
  jclass klass;
  jmethodID get_default_type;
  jmethodID get_instance;
  jmethodID load;
  jmethodID load_stream;
  jmethodID aliases;
  jmethodID set_certificate_entry;
  jmethodID is_certificate_entry;
  jmethodID get_certificate;
  jmethodID set_key_entry;
  jmethodID is_key_entry;
  jmethodID get_key;
} GTlsKeystoreClass;

gboolean            keystore_initialize     (GError ** error);
GTlsKeystoreClass * keystore_get            (void);

jobject         keystore_get_default_type           (void);
jobject         keystore_get_instance               (jobject type);

void            keystore_load                       (jobject keystore, jobject params);
void            keystore_load_stream                (jobject keystore, jobject input_stream, jcharArray password);
jobject         keystore_aliases                    (jobject keystore);

void            keystore_set_certificate_entry      (jobject keystore, jobject alias, jobject cert);
gboolean        keystore_is_certificate_entry       (jobject keystore, jobject alias);
jobject         keystore_get_certificate            (jobject keystore, jobject alias);

void            keystore_set_key_entry              (jobject keystore, jobject alias, jobject key, jcharArray password, jobjectArray certificates);
gboolean        keystore_is_key_entry               (jobject keystore, jobject alias);
jobject         keystore_get_key                    (jobject keystore, jobject alias, jcharArray password);

G_END_DECLS

#endif /* __G_TLS_KEYSTORE_ANDROID_H___ */
