/*
 * gtlsutils-android.h
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __G_TLS_UTILS_ANDROID_H__
#define __G_TLS_UTILS_ANDROID_H__

#include <gio/gio.h>
#include <jni.h>

G_BEGIN_DECLS

gchar *         g_tls_android_socket_connectable_to_string (GSocketConnectable *identity);
gint            g_tls_android_socket_connectable_get_port  (GSocketConnectable *identity);

jbyteArray      jni_byte_array_from_data                    (guint8 * data, gsize size);
GBytes *        bytes_from_jni_byte_array                   (jbyteArray array);
gboolean        pem_to_der                                  (const guint8 * data, gsize length, jbyteArray * out);
guint8 *        der_to_pem                                  (GByteArray * der, gsize * length);

G_END_DECLS

#endif /* __G_TLS_UTILS_ANDROID_H__ */

