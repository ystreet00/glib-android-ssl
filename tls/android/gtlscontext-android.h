/*
 * gtlscontext-android.h
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __G_TLS_CONTEXT_ANDROID_H__
#define __G_TLS_CONTEXT_ANDROID_H__

#include <gio/gio.h>
#include <jni.h>

G_BEGIN_DECLS

typedef struct _GTlsContextClass
{
  jclass klass;
  jmethodID get_instance;
  jmethodID init;
  jmethodID create_ssl_engine;
  jmethodID create_ssl_engine_with_peer;
} GTlsContextClass;

gboolean            context_initialize                  (GError ** error);
GTlsContextClass *  context_get                         (void);

jobject         context_get_instance                (jobject name);
void            context_init                        (jobject context, jobjectArray keymanagers, jobjectArray trustmanagers, jobject secure_random);
jobject         context_create_ssl_engine           (jobject context);
jobject         context_create_ssl_engine_with_peer (jobject context, jobject host, jint port);

G_END_DECLS

#endif /* __G_TLS_CONTEXT_ANDROID_H___ */
