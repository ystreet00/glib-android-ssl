/*
 * gtlskeymanagerfactory-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlskeymanagerfactory-android.h"

#include "gtlsjniutils.h"

static GTlsKeyManagerFactoryClass default_key_manager_factory;

gboolean
key_manager_factory_initialize (GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *my_error = NULL;

  /* javax.net.ssl.KeyManagerFactory */
  if (!(default_key_manager_factory.klass =
      g_tls_jni_get_class (env,
                           &my_error,
                           "javax/net/ssl/KeyManagerFactory"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_key_manager_factory.get_default_algorithm =
      g_tls_jni_get_static_method_id (env,
                               &my_error,
                               default_key_manager_factory.klass,
                               "getDefaultAlgorithm",
                               "()Ljava/lang/String;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_key_manager_factory.get_instance =
      g_tls_jni_get_static_method_id (env,
                               &my_error,
                               default_key_manager_factory.klass,
                               "getInstance",
                               "(Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_key_manager_factory.init =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_key_manager_factory.klass,
                               "init",
                               "(Ljava/security/KeyStore;[C)V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_key_manager_factory.get_key_managers =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_key_manager_factory.klass,
                               "getKeyManagers",
                               "()[Ljavax/net/ssl/KeyManager;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_critical ("Could not find \'void init(KeyStore)\' on "
        "\'javax.net.ssl.KeyManagerFactory\'");
    return FALSE;
  }

  return TRUE;
}

GTlsKeyManagerFactoryClass *
key_manager_factory_get (void)
{
  return &default_key_manager_factory;
}

jobject
key_manager_factory_get_default_algorithm (void)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject algo;

  if (!g_tls_jni_call_static_object_method (env,
            &error,
            default_key_manager_factory.klass,
            default_key_manager_factory.get_default_algorithm,
            &algo)) {
    g_critical ("Failed to get default KeyManagerFactory algorithm: %s",
        error->message);
    return NULL;
  }

  return algo;
}

jobject
key_manager_factory_get_instance (jobject algo)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject instance;

  if (!g_tls_jni_call_static_object_method (env,
            &error,
            default_key_manager_factory.klass,
            default_key_manager_factory.get_instance,
            &instance,
            algo)) {
    g_critical ("Failed to get instance of KeyManagerFactory: %s",
        error->message);
    return NULL;
  }

  return instance;
}

void
key_manager_factory_init (jobject factory, jobject keystore, jcharArray password)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
            &error,
            factory,
            default_key_manager_factory.init,
            keystore,
            password)) {
    g_critical ("Failed to get initialize KeyManagerFactory with keystore: %s",
        error->message);
    return;
  }
}

jobjectArray
key_manager_factory_get_key_managers (jobject factory)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobjectArray managers;

  if (!g_tls_jni_call_object_method (env,
            &error,
            factory,
            default_key_manager_factory.get_key_managers,
            &managers)) {
    g_critical ("Failed to get default KeyManagerFactory algorithm: %s",
        error->message);
    return NULL;
  }

  return managers;
}
