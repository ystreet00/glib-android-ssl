/*
 * gtlsdatabase-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlsdatabase-android.h"
#include "gtlsenumeration-android.h"
#include "gtlsjniutils.h"

typedef struct _GTlsDatabaseAndroidPrivate
{
  gint dummy;
} GTlsDatabaseAndroidPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (GTlsDatabaseAndroid, g_tls_database_android, G_TYPE_TLS_DATABASE,
                                 G_ADD_PRIVATE (GTlsDatabaseAndroid))

static GHashTable *
bytes_multi_table_new (void)
{
  return g_hash_table_new_full (g_bytes_hash, g_bytes_equal,
                                (GDestroyNotify)g_bytes_unref,
                                (GDestroyNotify)g_ptr_array_unref);
}

static void
bytes_multi_table_insert (GHashTable *table, GBytes *key, GBytes *value)
{
  GPtrArray *multi;

  g_return_if_fail (key != NULL);

  multi = g_hash_table_lookup (table, key);
  if (multi == NULL) {
    multi = g_ptr_array_new_with_free_func ((GDestroyNotify)g_bytes_unref);
    g_hash_table_insert (table, key, multi);
  }
  g_ptr_array_add (multi, g_bytes_ref (value));
}

static GBytes *
bytes_multi_table_lookup_ref_one (GHashTable *table, const GBytes *key)
{
  GPtrArray *multi;

  multi = g_hash_table_lookup (table, key);
  if (multi == NULL)
    return NULL;

  g_assert (multi->len > 0);
  return g_bytes_ref (multi->pdata[0]);
}

static GList *
bytes_multi_table_lookup_ref_all (GHashTable *table, const GBytes *key)
{
  GPtrArray *multi;
  GList *list = NULL;
  guint i;

  multi = g_hash_table_lookup (table, key);
  if (multi == NULL)
    return NULL;

  for (i = 0; i < multi->len; i++)
    list = g_list_prepend (list, g_bytes_ref (multi->pdata[i]));

  return g_list_reverse (list);
}

gchar *
g_tls_database_android_create_handle_for_certificate (const gchar *prefix, GBytes *der)
{
  gchar *bookmark;
  gchar *uri;

  /*
   * Here we create a URI that looks like:
   * file:///etc/ssl/certs/ca-certificates.crt#11b2641821252596420e468c275771f5e51022c121a17bd7a89a2f37b6336c8f
   */

  bookmark = g_compute_checksum_for_bytes (G_CHECKSUM_SHA256, der);
  if (prefix) {
    uri = g_strconcat (prefix, "#", bookmark, NULL);
    g_free (bookmark);
  } else {
    uri = bookmark;
  }

  return uri;
}

static GTlsCertificate *
g_tls_database_android_lookup_certificate_for_handle (GTlsDatabase *database,
    const gchar *handle, GTlsInteraction *interaction,
    GTlsDatabaseLookupFlags flags, GCancellable *cancellable, GError **error)
{
  GTlsDatabaseAndroid *android_database = G_TLS_DATABASE_ANDROID (database);
  GTlsCertificate *cert;

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return NULL;

  if (!handle)
    return NULL;

  g_mutex_lock (&android_database->mutex);

  cert = g_hash_table_lookup (android_database->certs_by_handle, handle);

  g_mutex_unlock (&android_database->mutex);

  return cert ? g_object_ref (cert) : NULL;
}

static GTlsCertificate *
g_tls_database_android_lookup_certificate_issuer (GTlsDatabase *database,
    GTlsCertificate *certificate, GTlsInteraction *interaction,
    GTlsDatabaseLookupFlags flags, GCancellable *cancellable, GError **error)
{
  GTlsDatabaseAndroid *android_database = G_TLS_DATABASE_ANDROID (database);
  GBytes *der, *issuer_bytes;
  GTlsCertificate *issuer = NULL;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_ANDROID (certificate), NULL);

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return NULL;

  if (flags & G_TLS_DATABASE_LOOKUP_KEYPAIR)
    return NULL;

  /* Dig out the issuer of this certificate */
  issuer_bytes = g_tls_certificate_android_get_issuer_sequence (G_TLS_CERTIFICATE_ANDROID (certificate));

  g_mutex_lock (&android_database->mutex);
  der = bytes_multi_table_lookup_ref_one (android_database->subjects, issuer_bytes);
  g_mutex_unlock (&android_database->mutex);

  if (g_cancellable_set_error_if_cancelled (cancellable, error)) {
    issuer = NULL;
  } else if (der != NULL) {
    issuer = g_tls_certificate_android_new (der, NULL);
  }

  if (der != NULL)
    g_bytes_unref (der);
  if (issuer_bytes != NULL)
    g_bytes_unref (issuer_bytes);

  return issuer;
}

static GList *
g_tls_database_android_lookup_certificates_issued_by (GTlsDatabase *database,
    GByteArray *issuer_raw_dn, GTlsInteraction *interaction,
    GTlsDatabaseLookupFlags flags, GCancellable *cancellable, GError **error)
{
  GTlsDatabaseAndroid *android_database = G_TLS_DATABASE_ANDROID (database);
  GQueue issued = G_QUEUE_INIT;
  GBytes *issuer = NULL;
  GList *ders, *l;

  if (flags & G_TLS_DATABASE_LOOKUP_KEYPAIR)
    return NULL;

  issuer = g_bytes_new (issuer_raw_dn->data, issuer_raw_dn->len);

  /* Find the full DER value of the certificate */
  g_mutex_lock (&android_database->mutex);
  ders = bytes_multi_table_lookup_ref_all (android_database->issuers, issuer);
  g_mutex_unlock (&android_database->mutex);

  g_bytes_unref (issuer);
  issuer = NULL;

  for (l = ders; l != NULL; l = g_list_next (l)) {
    if (g_cancellable_set_error_if_cancelled (cancellable, error)) {
      g_queue_free_full (&issued, g_object_unref);
      break;
    }

    g_queue_push_tail (&issued, g_tls_certificate_android_new (l->data, NULL));
  }

  g_list_free_full (ders, (GDestroyNotify) g_bytes_unref);

  return issued.head;
}

static GTlsCertificateFlags
g_tls_database_android_verify_chain (GTlsDatabase *database,
    GTlsCertificate *chain, const gchar *purpose, GSocketConnectable *identity,
    GTlsInteraction *interaction, GTlsDatabaseVerifyFlags flags,
    GCancellable *cancellable, GError **error)
{
  GTlsDatabaseAndroid *android_database;
  GTlsCertificateAndroid *cert_android;
  GTlsCertificateFlags result = 0;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_ANDROID (chain),
                        G_TLS_CERTIFICATE_GENERIC_ERROR);

  android_database = G_TLS_DATABASE_ANDROID (database);

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return G_TLS_CERTIFICATE_GENERIC_ERROR;

  cert_android = G_TLS_CERTIFICATE_ANDROID (chain);

  result = g_tls_certificate_android_verify_with_keystore (cert_android,
      identity, android_database->keystore, purpose, error);

  return result;
}

gboolean
g_tls_database_android_load_keystore (GTlsDatabaseAndroid * android,
    jobject keystore, const gchar * handle_prefix, GError ** error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  jobject aliases;
  gboolean ret = TRUE;

  g_debug ("%p loading keystore %p into db", android, keystore);

  g_return_val_if_fail (android->keystore == NULL, FALSE);
  g_return_val_if_fail (keystore != NULL, FALSE);

  aliases = keystore_aliases (keystore);

  while (enumeration_has_more_elements (aliases)) {
    jobject alias = enumeration_next_element (aliases);

    if (keystore_is_certificate_entry (keystore, alias)) {
      GTlsCertificateAndroid *certificate;
      GBytes *subject;
      GBytes *issuer;
      GBytes *der;
      jobject cert;
      gchar *handle;

      cert = keystore_get_certificate (keystore, alias);

      certificate = G_TLS_CERTIFICATE_ANDROID (g_tls_certificate_android_new_with_cert (cert, NULL));

      der = g_tls_certificate_android_get_cert_bytes (certificate);
      g_return_val_if_fail (der != NULL, FALSE);

      handle = g_tls_database_android_create_handle_for_certificate (handle_prefix, der);
      g_debug ("%p loading certificate %s into db", android, handle);

      subject = g_tls_certificate_android_get_subject_sequence (certificate);
      issuer = g_tls_certificate_android_get_issuer_sequence (certificate);

      g_hash_table_insert (android->complete, g_bytes_ref (der),
                           g_bytes_ref (der));

      bytes_multi_table_insert (android->subjects, subject, der);
      bytes_multi_table_insert (android->issuers, issuer, der);

      g_hash_table_insert (android->certs_by_handle, handle, g_object_ref (certificate));

      g_bytes_unref (der);

      g_object_unref (certificate);
      g_tls_jni_object_local_unref (env, cert);
    }

    g_tls_jni_object_local_unref (env, alias);
  }

  android->keystore = g_tls_jni_object_ref (env, keystore);

  if (aliases)
    g_tls_jni_object_local_unref (env, aliases);

  return ret;
}

static void
g_tls_database_android_finalize (GObject * object)
{
  GTlsDatabaseAndroid *database = G_TLS_DATABASE_ANDROID (object);
  JNIEnv *env = g_tls_jni_get_env ();

  g_clear_pointer (&database->subjects, g_hash_table_destroy);
  g_clear_pointer (&database->issuers, g_hash_table_destroy);
  g_clear_pointer (&database->complete, g_hash_table_destroy);
  g_clear_pointer (&database->certs_by_handle, g_hash_table_destroy);

  g_mutex_clear (&database->mutex);

  if (database->keystore) {
    g_tls_jni_object_unref (env, database->keystore);
    database->keystore = NULL;
  }

  G_OBJECT_CLASS (g_tls_database_android_parent_class)->finalize (object);
}

static void
g_tls_database_android_class_init (GTlsDatabaseAndroidClass *klass)
{
  GTlsDatabaseClass *database_class = G_TLS_DATABASE_CLASS (klass);
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->finalize = g_tls_database_android_finalize;

  database_class->lookup_certificate_for_handle = g_tls_database_android_lookup_certificate_for_handle;
  database_class->lookup_certificate_issuer = g_tls_database_android_lookup_certificate_issuer;
  database_class->lookup_certificates_issued_by = g_tls_database_android_lookup_certificates_issued_by;
  database_class->verify_chain = g_tls_database_android_verify_chain;
}

static void
g_tls_database_android_init (GTlsDatabaseAndroid *android)
{
  android->subjects = bytes_multi_table_new ();
  android->issuers = bytes_multi_table_new ();

  android->complete = g_hash_table_new_full (g_bytes_hash, g_bytes_equal,
                                    (GDestroyNotify)g_bytes_unref,
                                    (GDestroyNotify)g_bytes_unref);

  android->certs_by_handle = g_hash_table_new_full (g_str_hash, g_str_equal,
                                           (GDestroyNotify)g_free,
                                           (GDestroyNotify)g_object_unref);

  g_mutex_init (&android->mutex);
}

void
g_tls_database_android_copy_certs_to_keystore (GTlsDatabaseAndroid * android,
    jobject keystore)
{
  JNIEnv *env = g_tls_jni_get_env ();
  jobject aliases;

  g_return_if_fail (android->keystore != NULL);
  g_return_if_fail (keystore != NULL);

  aliases = keystore_aliases (android->keystore);

  while (enumeration_has_more_elements (aliases)) {
    jobject alias = enumeration_next_element (aliases);

    if (keystore_is_certificate_entry (android->keystore, alias)) {
      jobject cert;

      cert = keystore_get_certificate (android->keystore, alias);

      keystore_set_certificate_entry (keystore, alias, cert);

      g_tls_jni_object_local_unref (env, cert);
    }

    g_tls_jni_object_local_unref (env, alias);
  }

  if (aliases)
    g_tls_jni_object_local_unref (env, aliases);
}
