/*
 * gtlscustomtrustmanager-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlscustomtrustmanager-android.h"

#include "gtlsjniutils.h"

#define GET_CUSTOM_DATA(env, thiz, fieldID) (void *)(gintptr)(*env)->GetLongField (env, thiz, fieldID)
#define SET_CUSTOM_DATA(env, thiz, fieldID, data) (*env)->SetLongField (env, thiz, fieldID, (jlong)(gintptr)data)

static GTlsCustomTrustManagerClass default_custom_trust_manager;

static void
native_check_client_trusted (JNIEnv * env, jobject j_manager, jobjectArray certs, jstring auth_type)
{
  GTlsCustomTrustManager *manager = GET_CUSTOM_DATA (env, j_manager, default_custom_trust_manager.native_manager_field_id);
  GTlsCertificateFlags flags = 0;

  if (manager->check_client_trusted)
    flags = manager->check_client_trusted (manager, certs, auth_type, manager->user_data);

  g_info ("checking client trusted");

  if (flags)
    g_print ("client trust flags failed 0x%x\n", flags);
  /* TODO: throw an exception when we fail validation */
}

static void
native_check_server_trusted (JNIEnv * env, jobject j_manager, jobjectArray certs, jstring auth_type)
{
  GTlsCustomTrustManager *manager = GET_CUSTOM_DATA (env, j_manager, default_custom_trust_manager.native_manager_field_id);
  GTlsCertificateFlags flags = 0;

  if (manager->check_server_trusted)
    flags = manager->check_server_trusted (manager, certs, auth_type, manager->user_data);

  g_info ("checking server trusted");

  if (flags)
    g_print ("server trust flags failed 0x%x\n", flags);
  /* TODO: throw an exception when we fail validation */
}

static jobjectArray
native_get_accepted_issuers (JNIEnv * env, jobject j_manager)
{
  GTlsCustomTrustManager *manager = GET_CUSTOM_DATA (env, j_manager, default_custom_trust_manager.native_manager_field_id);
  jobjectArray ret = NULL;

  g_info ("native get accepted issuers");

  if (manager->get_accepted_issuers) {
    ret = manager->get_accepted_issuers (manager, manager->user_data);
  }

  if (!ret) {
    /* default case */
    ret = (*env)->NewObjectArray (env, 0, g_tls_jni_get_class (env, NULL, "java/security/cert/X509Certificate"), NULL);
  }

  return ret;
}

static void
native_free (JNIEnv * env, jobject j_manager)
{
  GTlsCustomTrustManager *manager = GET_CUSTOM_DATA (env, j_manager, default_custom_trust_manager.native_manager_field_id);

  if (manager->notify)
    manager->notify (manager->user_data);

  /* FIXME: this is never called due to a reference cycle between java and jni */
  g_tls_jni_object_unref (env, j_manager);

  g_free (manager);
}

/* List of implemented native methods */
static JNINativeMethod native_methods[] = {
  {"nativeCheckClientTrusted", "([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V", (void *) native_check_client_trusted},
  {"nativeCheckServerTrusted", "([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V", (void *) native_check_server_trusted},
  {"nativeGetAcceptedIssuers", "()[Ljava/security/cert/X509Certificate;", (void *) native_get_accepted_issuers},
  {"nativeFree", "()V", (void *) native_free},
};

gboolean
custom_trust_manager_initialize (GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *my_error = NULL;

  if (!(default_custom_trust_manager.klass =
        g_tls_jni_get_class (env,
                            &my_error,
                            "org/gnome/glib/ssl/CustomTrustManager"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not retrieve CustomTrustManager class");
    return FALSE;
  }
  if ((*env)->RegisterNatives (env,
                            default_custom_trust_manager.klass,
                            native_methods,
                            G_N_ELEMENTS (native_methods))) {
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not register native methods for CustomTrustManager class");
    return FALSE;
  }

  if (!(default_custom_trust_manager.native_manager_field_id =
            g_tls_jni_get_field_id (env,
                                    &my_error,
                                    default_custom_trust_manager.klass,
                                    "native_manager",
                                    "J"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not retrieve native_manager field ID");
    return FALSE;
  }

  if (!(default_custom_trust_manager.constructor =
            g_tls_jni_get_method_id (env,
                                    &my_error,
                                    default_custom_trust_manager.klass,
                                    "<init>",
                                    "()V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not retrieve native_manager field ID");
    return FALSE;
  }

  return TRUE;
}

GTlsCustomTrustManagerClass *
custom_trust_manager_get (void)
{
  return &default_custom_trust_manager;
}

GTlsCustomTrustManager *
custom_trust_manager_new (GTlsCustomTrustManagerCheckTrustedFunc server,
    GTlsCustomTrustManagerCheckTrustedFunc client,
    GTlsCustomTrustManagerGetAcceptedIssuersFunc get_issuers, gpointer user_data,
    GDestroyNotify notify)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsCustomTrustManager *ret;
  GError *my_error = NULL;
  jobject manager;

  if (!(manager = g_tls_jni_new_object (env,
                                    &my_error,
                                    TRUE,
                                    default_custom_trust_manager.klass,
                                    default_custom_trust_manager.constructor,
                                    &manager))) {
    g_critical ("Failed to create CustomTrustManager: %s", my_error->message);
    g_clear_error (&my_error);
    return NULL;
  }

  ret = g_new0 (GTlsCustomTrustManager, 1);
  SET_CUSTOM_DATA (env, manager, default_custom_trust_manager.native_manager_field_id, ret);
  ret->manager = manager;
  ret->check_server_trusted = server;
  ret->check_client_trusted = client;
  ret->get_accepted_issuers = get_issuers;
  ret->user_data = user_data;
  ret->notify = notify;

  return ret;
}
