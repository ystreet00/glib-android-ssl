/*
 * gtlsengine-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlsengine-android.h"

#include "gtlsjniutils.h"

#define MAKE_JAVA_DEBUG_READABLE 0

static GTlsEngineClass default_engine;

#if MAKE_JAVA_DEBUG_READABLE
G_LOCK_DEFINE_STATIC (engine);
#endif

gboolean
engine_initialize (GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *my_error = NULL;

  /* javax.net.ssl.SSLEngine */
  if (!(default_engine.klass =
      g_tls_jni_get_class (env,
                           &my_error,
                           "javax/net/ssl/SSLEngine"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find class \'javax.net.ssl.SSLEngine\'");
    return FALSE;
  }

  if (!(default_engine.set_need_client_auth =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "setNeedClientAuth",
                               "(Z)V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'void setNeedClientAuth(boolean)\' on "
        "\'javax.net.ssl.SSLEngine\'");
    return FALSE;
  }

  if (!(default_engine.set_want_client_auth =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "setWantClientAuth",
                               "(Z)V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'void setWantClientAuth(boolean)\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.set_use_client_mode =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "setUseClientMode",
                               "(Z)V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'void setUseClientMode(boolean)\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.begin_handshake =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "beginHandshake",
                               "()V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'void beginHandshake()\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.get_session =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "getSession",
                               "()Ljavax/net/ssl/SSLSession;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'SSLSession getSession()\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.get_delegated_task =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "getDelegatedTask",
                               "()Ljava/lang/Runnable;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'Runnable getDelegatedTask()\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.get_handshake_status =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "getHandshakeStatus",
                               "()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'SSLEngineResult.HandshakeStatus getHandshakeStatus()\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.wrap =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "wrap",
                               "(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'SSLEngineResult wrap(ByteBuffer, ByteBuffer)\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.unwrap =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "unwrap",
                               "(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'SSLEngineResult unwrap(ByteBuffer, ByteBuffer)\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.close_outbound =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "closeOutbound",
                               "()V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'void closeOutbound()\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.close_inbound =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "closeInbound",
                               "()V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'void closeInbound()\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.is_outbound_done =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "isOutboundDone",
                               "()Z"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'bool isOutboundDone()\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  if (!(default_engine.is_inbound_done =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.klass,
                               "isInboundDone",
                               "()Z"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'bool isInboundDone()\' on "
        "\'javax.net.ssl.SSLengine\'");
    return FALSE;
  }

  /* javax.net.ssl.SSLEngineResult */
  if (!(default_engine.result_klass =
      g_tls_jni_get_class (env,
                               &my_error,
                           "javax/net/ssl/SSLEngineResult"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find class \'javax.net.ssl.SSLEngineResult\'");
    return FALSE;
  }

  if (!(default_engine.result_get_status =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.result_klass,
                               "getStatus",
                               "()Ljavax/net/ssl/SSLEngineResult$Status;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'SSLEngineResult.Status getStatus()\' on "
        "\'javax.net.ssl.SSLEngineResult\'");
    return FALSE;
  }

  if (!(default_engine.result_get_handshake_status =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.result_klass,
                               "getHandshakeStatus",
                               "()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'SSLEngineResult.HandshakeStatus getHandshakeStatus()\' on "
        "\'javax.net.ssl.SSLEngineResult\'");
    return FALSE;
  }

  if (!(default_engine.result_bytes_consumed =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.result_klass,
                               "bytesConsumed",
                               "()I"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'int bytesConsumed()\' on "
        "\'javax.net.ssl.SSLEngineResult\'");
    return FALSE;
  }

  if (!(default_engine.result_bytes_produced =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_engine.result_klass,
                               "bytesProduced",
                               "()I"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'int bytesProduced()\' on "
        "\'javax.net.ssl.SSLEngineResult\'");
    return FALSE;
  }

  /* javax.net.ssl.SSLEngineResult$Status */
  if (!(default_engine.result_status_klass =
      g_tls_jni_get_class (env,
                               &my_error,
                           "javax/net/ssl/SSLEngineResult$Status"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find class \'javax.net.ssl.SSLEngineResult$Status\'");
    return FALSE;
  }

  if (!(default_engine.result_status_field_buffer_overflow =
      g_tls_jni_get_static_field_id (env,
                               &my_error,
                               default_engine.result_status_klass,
                               "BUFFER_OVERFLOW",
                               "Ljavax/net/ssl/SSLEngineResult$Status;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find field \'BUFFER_OVERFLOW\' on "
        "\'javax.net.ssl.SSLEngineResult$Status\'");
    return FALSE;
  }

  {
    jobject local;

    if (!g_tls_jni_get_static_object_field (env,
                                 &my_error,
                                 default_engine.result_status_klass,
                                 default_engine.result_status_field_buffer_overflow,
                                 &local)) {
      g_critical ("%s", my_error->message);
      g_clear_error (&my_error);
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Could not find \'BUFFER_OVERFLOW\' on "
          "\'javax.net.ssl.SSLEngineResult$Status\'");
      return FALSE;
    }

    default_engine.result_status_buffer_overflow = g_tls_jni_object_make_global (env, local);
  }

  if (!(default_engine.result_status_field_buffer_underflow =
      g_tls_jni_get_static_field_id (env,
                               &my_error,
                               default_engine.result_status_klass,
                               "BUFFER_UNDERFLOW",
                               "Ljavax/net/ssl/SSLEngineResult$Status;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find field \'BUFFER_UNDERFLOW\' on "
        "\'javax.net.ssl.SSLEngineResult$Status\'");
    return FALSE;
  }

  {
    jobject local;

    if (!g_tls_jni_get_static_object_field (env,
                                 &my_error,
                                 default_engine.result_status_klass,
                                 default_engine.result_status_field_buffer_underflow,
                                 &local)) {
      g_critical ("%s", my_error->message);
      g_clear_error (&my_error);
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Could not find \'BUFFER_UNDERFLOW\' on "
          "\'javax.net.ssl.SSLEngineResult$Status\'");
      return FALSE;
    }

    default_engine.result_status_buffer_underflow = g_tls_jni_object_make_global (env, local);
  }

  if (!(default_engine.result_status_field_closed =
      g_tls_jni_get_static_field_id (env,
                               &my_error,
                               default_engine.result_status_klass,
                               "CLOSED",
                               "Ljavax/net/ssl/SSLEngineResult$Status;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find field \'CLOSED\' on "
        "\'javax.net.ssl.SSLEngineResult$Status\'");
    return FALSE;
  }

  {
    jobject local;

    if (!g_tls_jni_get_static_object_field (env,
                                 &my_error,
                                 default_engine.result_status_klass,
                                 default_engine.result_status_field_closed,
                                 &local)) {
      g_critical ("%s", my_error->message);
      g_clear_error (&my_error);
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Could not find \'CLOSED\' on "
          "\'javax.net.ssl.SSLEngineResult$Status\'");
      return FALSE;
    }

    default_engine.result_status_closed = g_tls_jni_object_make_global (env, local);
  }

  if (!(default_engine.result_status_field_ok =
      g_tls_jni_get_static_field_id (env,
                               &my_error,
                               default_engine.result_status_klass,
                               "OK",
                               "Ljavax/net/ssl/SSLEngineResult$Status;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find field \'OK\' on "
        "\'javax.net.ssl.SSLEngineResult$Status\'");
    return FALSE;
  }

  {
    jobject local;

    if (!g_tls_jni_get_static_object_field (env,
                                 &my_error,
                                 default_engine.result_status_klass,
                                 default_engine.result_status_field_ok,
                                 &local)) {
      g_critical ("%s", my_error->message);
      g_clear_error (&my_error);
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Could not find \'OK\' on "
          "\'javax.net.ssl.SSLEngineResult$Status\'");
      return FALSE;
    }

    default_engine.result_status_ok = g_tls_jni_object_make_global (env, local);
  }

  /* javax.net.ssl.SSLEngineResult$HandshakeStatus */
  if (!(default_engine.result_handshake_status_klass =
      g_tls_jni_get_class (env,
                           &my_error,
                           "javax/net/ssl/SSLEngineResult$HandshakeStatus"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find class \'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
    return FALSE;
  }

  if (!(default_engine.result_handshake_status_field_finished =
      g_tls_jni_get_static_field_id (env,
                               &my_error,
                               default_engine.result_handshake_status_klass,
                               "FINISHED",
                               "Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find field \'FINISHED\' on "
        "\'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
    return FALSE;
  }

  {
    jobject local;

    if (!g_tls_jni_get_static_object_field (env,
                                 &my_error,
                                 default_engine.result_handshake_status_klass,
                                 default_engine.result_handshake_status_field_finished,
                                 &local)) {
      g_critical ("%s", my_error->message);
      g_clear_error (&my_error);
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Could not find \'FINISHED\' on "
          "\'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
      return FALSE;
    }

    default_engine.result_handshake_status_finished = g_tls_jni_object_make_global (env, local);
  }

  if (!(default_engine.result_handshake_status_field_need_task =
      g_tls_jni_get_static_field_id (env,
                               &my_error,
                               default_engine.result_handshake_status_klass,
                               "NEED_TASK",
                               "Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find field \'NEED_TASK\' on "
        "\'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
    return FALSE;
  }

  {
    jobject local;

    if (!g_tls_jni_get_static_object_field (env,
                                 &my_error,
                                 default_engine.result_handshake_status_klass,
                                 default_engine.result_handshake_status_field_need_task,
                                 &local)) {
      g_critical ("%s", my_error->message);
      g_clear_error (&my_error);
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Could not find \'NEED_TASK\' on "
          "\'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
      return FALSE;
    }

    default_engine.result_handshake_status_need_task = g_tls_jni_object_make_global (env, local);
  }

  if (!(default_engine.result_handshake_status_field_need_unwrap =
      g_tls_jni_get_static_field_id (env,
                               &my_error,
                               default_engine.result_handshake_status_klass,
                               "NEED_UNWRAP",
                               "Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find field \'NEED_UNWRAP\' on "
        "\'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
    return FALSE;
  }

  {
    jobject local;

    if (!g_tls_jni_get_static_object_field (env,
                                 &my_error,
                                 default_engine.result_handshake_status_klass,
                                 default_engine.result_handshake_status_field_need_unwrap,
                                 &local)) {
      g_critical ("%s", my_error->message);
      g_clear_error (&my_error);
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Could not find \'NEED_UNWRAP\' on "
          "\'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
      return FALSE;
    }

    default_engine.result_handshake_status_need_unwrap = g_tls_jni_object_make_global (env, local);
  }

  if (!(default_engine.result_handshake_status_field_need_wrap =
      g_tls_jni_get_static_field_id (env,
                               &my_error,
                               default_engine.result_handshake_status_klass,
                               "NEED_WRAP",
                               "Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find field \'NEED_WRAP\' on "
        "\'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
    return FALSE;
  }

  {
    jobject local;

    if (!g_tls_jni_get_static_object_field (env,
                                 &my_error,
                                 default_engine.result_handshake_status_klass,
                                 default_engine.result_handshake_status_field_need_wrap,
                                 &local)) {
      g_critical ("%s", my_error->message);
      g_clear_error (&my_error);
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Could not find \'NEED_WRAP\' on "
          "\'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
      return FALSE;
    }

    default_engine.result_handshake_status_need_wrap = g_tls_jni_object_make_global (env, local);
  }

  if (!(default_engine.result_handshake_status_field_not_handshaking =
      g_tls_jni_get_static_field_id (env,
                               &my_error,
                               default_engine.result_handshake_status_klass,
                               "NOT_HANDSHAKING",
                               "Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find field \'NOT_HANDSHAKING\' on "
        "\'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
    return FALSE;
  }

  {
    jobject local;

    if (!g_tls_jni_get_static_object_field (env,
                                 &my_error,
                                 default_engine.result_handshake_status_klass,
                                 default_engine.result_handshake_status_field_not_handshaking,
                                 &local)) {
      g_critical ("%s", my_error->message);
      g_clear_error (&my_error);
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Could not find \'NOT_HANDSHAKING\' on "
          "\'javax.net.ssl.SSLEngineResult$HandshakeStatus\'");
      return FALSE;
    }

    default_engine.result_handshake_status_not_handshaking = g_tls_jni_object_make_global (env, local);
  }

  if (!(default_engine.ssl_exception_klass =
      g_tls_jni_get_class (env,
                           &my_error,
                           "javax/net/ssl/SSLException"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find class \'javax.net.ssl.SSLException\'");
    return FALSE;
  }

  return TRUE;
}

GTlsEngineClass *
engine_get (void)
{
  return &default_engine;
}

void
engine_begin_handshake (jobject engine)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
        &error,
        engine,
        default_engine.begin_handshake)) {
    g_critical ("Failed to begin handshake: %s", error->message);
    return;
  }
}

void
engine_set_need_client_auth (jobject engine, gboolean value)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
        &error,
        engine,
        default_engine.set_need_client_auth,
        value)) {
    g_critical ("Failed to set need client auth: %s", error->message);
    return;
  }
}

void
engine_set_want_client_auth (jobject engine, gboolean value)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
        &error,
        engine,
        default_engine.set_want_client_auth,
        value)) {
    g_critical ("Failed to set want client auth: %s", error->message);
    return;
  }
}

void
engine_set_use_client_mode (jobject engine, gboolean value)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
        &error,
        engine,
        default_engine.set_use_client_mode,
        value)) {
    g_critical ("Failed to set client mode: %s", error->message);
    return;
  }
}

EngineHandshakeStatus
engine_get_handshake_status (jobject engine)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  EngineHandshakeStatus ret;
  jobject value;

  if (!g_tls_jni_call_object_method (env,
        &error,
        engine,
        default_engine.get_handshake_status,
        &value)) {
    g_critical ("Failed to get engine status: %s", error->message);
    return ENGINE_HANDSHAKE_ERROR;
  }

  if ((*env)->IsSameObject (env, value, default_engine.result_handshake_status_finished)) {
    ret = ENGINE_HANDSHAKE_FINISHED;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_handshake_status_need_task)) {
    ret = ENGINE_HANDSHAKE_NEED_TASK;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_handshake_status_need_unwrap)) {
    ret = ENGINE_HANDSHAKE_NEED_UNWRAP;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_handshake_status_need_wrap)) {
    ret = ENGINE_HANDSHAKE_NEED_WRAP;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_handshake_status_not_handshaking)) {
    ret = ENGINE_HANDSHAKE_NOT_HANDSHAKING;
  } else {
    g_critical ("Unknown engine status value");
    ret = ENGINE_HANDSHAKE_ERROR;
  }

  g_tls_jni_object_local_unref (env, value);
  return ret;
}

jobject
engine_get_session (jobject engine)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject session;

  if (!g_tls_jni_call_object_method (env,
        &error,
        engine,
        default_engine.get_session,
        &session)) {
    g_critical ("Failed to get session: %s", error->message);
    return NULL;
  }

  return session;
}

jobject
engine_wrap (jobject engine, jobject src, jobject dst)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject result;

#if MAKE_JAVA_DEBUG_READABLE
  G_LOCK (engine);
  g_usleep (1000);
#endif

  if (!g_tls_jni_call_object_method (env,
        &error,
        engine,
        default_engine.wrap,
        &result,
        src,
        dst)) {
    g_critical ("Failed to wrap data: %s", error->message);

#if MAKE_JAVA_DEBUG_READABLE
    G_UNLOCK (engine);
#endif

    return NULL;
  }

#if MAKE_JAVA_DEBUG_READABLE
  g_usleep (1000);
  G_UNLOCK (engine);
#endif

  return result;
}

jobject
engine_unwrap (jobject engine, jobject src, jobject dst)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject result;

#if MAKE_JAVA_DEBUG_READABLE
  G_LOCK (engine);
  g_usleep (1000);
#endif
  if (!g_tls_jni_call_object_method (env,
        &error,
        engine,
        default_engine.unwrap,
        &result,
        src,
        dst)) {
    g_critical ("Failed to unwrap data: %s", error->message);
#if MAKE_JAVA_DEBUG_READABLE
    G_UNLOCK (engine);
#endif
    return NULL;
  }
#if MAKE_JAVA_DEBUG_READABLE
  g_usleep (1000);
  G_UNLOCK (engine);
#endif
  return result;
}

EngineStatus
engine_result_get_status (jobject result)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  EngineStatus ret;
  jobject value;

  if (!g_tls_jni_call_object_method (env,
        &error,
        result,
        default_engine.result_get_status,
        &value)) {
    g_critical ("Failed to get engine status from result: %s", error->message);
    return ENGINE_ERROR;
  }

  if ((*env)->IsSameObject (env, value, default_engine.result_status_ok)) {
    ret = ENGINE_OK;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_status_closed)) {
    ret = ENGINE_CLOSED;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_status_buffer_overflow)) {
    ret = ENGINE_BUFFER_OVERFLOW;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_status_buffer_underflow)) {
    ret = ENGINE_BUFFER_UNDERFLOW;
  } else {
    g_critical ("Unknown engine status value");
    ret = ENGINE_ERROR;
  }

  g_tls_jni_object_local_unref (env, value);
  return ret;
}

EngineHandshakeStatus
engine_result_get_handshake_status (jobject result)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  EngineHandshakeStatus ret;
  jobject value;

  if (!g_tls_jni_call_object_method (env,
        &error,
        result,
        default_engine.result_get_handshake_status,
        &value)) {
    g_critical ("Failed to get engine handshake status from result: %s", error->message);
    return ENGINE_HANDSHAKE_ERROR;
  }

  if ((*env)->IsSameObject (env, value, default_engine.result_handshake_status_finished)) {
    ret = ENGINE_HANDSHAKE_FINISHED;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_handshake_status_need_task)) {
    ret = ENGINE_HANDSHAKE_NEED_TASK;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_handshake_status_need_unwrap)) {
    ret = ENGINE_HANDSHAKE_NEED_UNWRAP;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_handshake_status_need_wrap)) {
    ret = ENGINE_HANDSHAKE_NEED_WRAP;
  } else if ((*env)->IsSameObject (env, value, default_engine.result_handshake_status_not_handshaking)) {
    ret = ENGINE_HANDSHAKE_NOT_HANDSHAKING;
  } else {
    g_critical ("Unknown engine status value");
    ret = ENGINE_HANDSHAKE_ERROR;
  }

  g_tls_jni_object_local_unref (env, value);
  return ret;
}

int
engine_result_bytes_produced (jobject result)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  int res;

  if (!g_tls_jni_call_int_method (env,
        &error,
        result,
        default_engine.result_bytes_produced,
        &res)) {
    g_critical ("Failed to get bytes produced: %s", error->message);
    return 0;
  }

  return res;
}

int
engine_result_bytes_consumed (jobject result)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  int res;

  if (!g_tls_jni_call_int_method (env,
        &error,
        result,
        default_engine.result_bytes_consumed,
        &res)) {
    g_critical ("Failed to get bytes consumed: %s", error->message);
    return 0;
  }

  return res;
}

static void
runnable_run (jobject runnable)
{
  JNIEnv *env = g_tls_jni_get_env ();
  static jclass runnable_klass;
  static jmethodID runnable_run;
  GError *error = NULL;

  if (!runnable_klass) {
    if (!(runnable_klass = g_tls_jni_get_class (env,
                           &error,
                           "java/lang/Runnable"))) {
      g_critical ("Could not find class \'java.lang.Runnable\': %s",
          error->message);
      return;
    }

    if (!(runnable_run = g_tls_jni_get_method_id (env,
                    &error,
                    runnable_klass,
                    "run",
                    "()V"))) {
      g_critical ("Could not find method \'void run\' on "
          "\'java.lang.Runnable\': %s", error->message);
      return;
    }
  }

  if (!g_tls_jni_call_void_method (env,
            &error,
            runnable,
            runnable_run)) {
    g_critical ("Failed to call \'void run\' on "
        "\'java.lang.Runnable\': %s", error->message);
    return;
  } 
}

jobject
engine_get_delegated_task (jobject engine)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject result;

  if (!g_tls_jni_call_object_method (env,
        &error,
        engine,
        default_engine.get_delegated_task,
        &result)) {
    g_critical ("Failed to get delegated task: %s", error->message);
    return NULL;
  }

  return result;
}

void
engine_run_delegated_task (jobject engine)
{
  JNIEnv *env = g_tls_jni_get_env ();
  jobject runnable;

#if MAKE_JAVA_DEBUG_READABLE
  G_LOCK (engine);
  g_usleep (1000);
#endif

  while ((runnable = engine_get_delegated_task (engine))) {
    g_debug ("running delegated task %p", runnable);

#if MAKE_JAVA_DEBUG_READABLE
    g_usleep (1000);
#endif

    runnable_run (runnable);

#if MAKE_JAVA_DEBUG_READABLE
    g_usleep (1000);
#endif

    g_tls_jni_object_local_unref (env, runnable);
  }

#if MAKE_JAVA_DEBUG_READABLE
  g_usleep (1000);
  G_UNLOCK (engine);
#endif
}

void
engine_close_outbound (jobject engine)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
        &error,
        engine,
        default_engine.close_outbound)) {
    g_critical ("Failed to close outbound: %s", error->message);
    return;
  }

  g_debug ("%p closed outbound", engine);
}

gboolean
engine_close_inbound (jobject engine)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
        &error,
        engine,
        default_engine.close_inbound)) {
    g_debug ("Failed to close inbound: %s", error->message);
    return FALSE;
  }

  g_debug ("%p closed inbound", engine);

  return TRUE;
}

gboolean
engine_is_outbound_done (jobject engine)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  gboolean ret;

  if (!g_tls_jni_call_boolean_method (env,
        &error,
        engine,
        default_engine.is_outbound_done,
        &ret)) {
    g_critical ("Failed to check if output is done: %s", error->message);
    return FALSE;
  }

  return ret;
}

gboolean
engine_is_inbound_done (jobject engine)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  gboolean ret;

  if (!g_tls_jni_call_boolean_method (env,
        &error,
        engine,
        default_engine.is_inbound_done,
        &ret)) {
    g_critical ("Failed to check if input is done: %s", error->message);
    return FALSE;
  }

  return ret;
}
