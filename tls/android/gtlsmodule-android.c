/*
 * gtlsmodule-android.c
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include <gio/gio.h>

#include "gtlsmodule-android.h"
#include "gtlsbackend-android.h"

void
g_io_android_load (GIOModule * module)
{
  g_tls_backend_android_register (module);
}

void
g_io_android_unload (GIOModule *module)
{
}

gchar **
g_io_android_query (void)
{
  gchar *eps[] = {
    (gchar *) G_TLS_BACKEND_EXTENSION_POINT_NAME,
    NULL
  };
  return g_strdupv (eps);
}
