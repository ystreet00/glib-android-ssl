/*
 * gtlsthrowable-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlsthrowable-android.h"

#include "gtlsjniutils.h"

static GTlsThrowableClass default_throwable;

gboolean
throwable_initialize (GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *my_error = NULL;

  if (!(default_throwable.klass =
        g_tls_jni_get_class (env,
                &my_error,
                "java/lang/Throwable"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_throwable.get_cause =
            g_tls_jni_get_method_id (env,
                    &my_error,
                    default_throwable.klass,
                    "getCause",
                    "()Ljava/lang/Throwable;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  return TRUE;
}

GTlsThrowableClass *
throwable_get (void)
{
  return &default_throwable;
}

jobject
throwable_get_cause (jobject throwable)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject cause;

  if (!g_tls_jni_call_object_method (env, &error,
            throwable,
            default_throwable.get_cause,
            &cause)) {
    g_critical ("Failed to get throwable cause: %s", error->message);
    return NULL;
  }

  return cause;
}
