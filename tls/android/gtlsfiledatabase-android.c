/*
 * gtlsfiledatabase-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlsfiledatabase-android.h"

#include <gio/gio.h>

#include "gtlskeystore-android.h"
#include "gtlsjniutils.h"

typedef struct _GTlsFileDatabaseAndroidPrivate
{
  /* read-only after construct */
  gchar *anchor_filename;
} GTlsFileDatabaseAndroidPrivate;

enum
{
  PROP_0,
  PROP_ANCHORS,
};

static void g_tls_file_database_android_file_database_interface_init (GTlsFileDatabaseInterface *iface);

static void g_tls_file_database_android_initable_interface_init (GInitableIface *iface);

G_DEFINE_TYPE_WITH_CODE (GTlsFileDatabaseAndroid, g_tls_file_database_android, G_TYPE_TLS_DATABASE_ANDROID,
                         G_ADD_PRIVATE (GTlsFileDatabaseAndroid)
                         G_IMPLEMENT_INTERFACE (G_TYPE_TLS_FILE_DATABASE,
                                                g_tls_file_database_android_file_database_interface_init)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                g_tls_file_database_android_initable_interface_init))

static gboolean
load_anchor_file (GTlsFileDatabaseAndroid *file_database,
    const gchar * filename, GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsDatabaseAndroid *android_database = G_TLS_DATABASE_ANDROID (file_database);
  GTlsFileDatabaseAndroidPrivate *priv;
  GError *my_error = NULL;
  gchar *prefix = NULL;
  jobject default_type;
  jobject keystore;
  GList *list, *l;

  g_debug ("%p load anchor file: %s", file_database, filename);

  priv = g_tls_file_database_android_get_instance_private (file_database);

  list = g_tls_certificate_list_new_from_file (filename, &my_error);
  if (my_error) {
    g_propagate_error (error, my_error);
    return FALSE;
  }

  default_type = keystore_get_default_type ();
  keystore = keystore_get_instance (default_type);
  keystore_load (keystore, NULL);

  g_debug ("%p load %s into keystore: %p", file_database, filename, keystore);

  for (l = list; l; l = l->next) {
    jobject cert = g_tls_certificate_android_get_cert (l->data);
    jobject alias;
    gchar *handle;
    GBytes *der;

    der = g_tls_certificate_android_get_cert_bytes (l->data);

    /* only use hashes for the keystore as the keystore seems to perform some
     * alias mangling like changing everything to lowercase ASCII which will
     * not work well as handles for us with filenames */
    handle = g_tls_database_android_create_handle_for_certificate (NULL, der);
    alias = g_tls_jni_string_from_gchar (env, error, FALSE, handle);

    keystore_set_certificate_entry (keystore, alias, cert);

    g_free (handle);
    g_object_unref (l->data);

    g_tls_jni_object_local_unref (env, alias);
  }
  g_list_free (list);

  prefix = g_filename_to_uri (priv->anchor_filename, NULL, NULL);
  if (!prefix)
    goto out;

  g_tls_database_android_load_keystore (android_database, keystore,
      prefix, error);

out:
  g_tls_jni_object_local_unref (env, keystore);
  g_tls_jni_object_local_unref (env, default_type);

  g_free (prefix);

  return TRUE;
}

static void
g_tls_file_database_android_finalize (GObject *object)
{
  GTlsFileDatabaseAndroid *file_database = G_TLS_FILE_DATABASE_ANDROID (object);
  GTlsFileDatabaseAndroidPrivate *priv;

  g_debug ("file db finalize");

  priv = g_tls_file_database_android_get_instance_private (file_database);

  g_free (priv->anchor_filename);
  priv->anchor_filename = NULL;

  G_OBJECT_CLASS (g_tls_file_database_android_parent_class)->finalize (object);
}

static void
g_tls_file_database_android_get_property (GObject *object, guint prop_id,
    GValue *value, GParamSpec *pspec)
{
  GTlsFileDatabaseAndroid *file_database = G_TLS_FILE_DATABASE_ANDROID (object);
  GTlsFileDatabaseAndroidPrivate *priv;

  g_debug ("set property");

  priv = g_tls_file_database_android_get_instance_private (file_database);

  switch (prop_id) {
    case PROP_ANCHORS:
      g_value_set_string (value, priv->anchor_filename);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
g_tls_file_database_android_set_property (GObject *object,
    guint prop_id, const GValue *value, GParamSpec   *pspec)
{
  GTlsFileDatabaseAndroid *file_database = G_TLS_FILE_DATABASE_ANDROID (object);
  GTlsFileDatabaseAndroidPrivate *priv;
  const gchar *anchor_path;

  g_debug ("set property");

  priv = g_tls_file_database_android_get_instance_private (file_database);

  switch (prop_id) {
    case PROP_ANCHORS:
      g_debug ("anchors set");
      anchor_path = g_value_get_string (value);
      if (anchor_path && !g_path_is_absolute (anchor_path)) {
        g_warning ("The anchor file name used with a GTlsFileDatabase "
                   "must be an absolute path, and not relative: %s", anchor_path);
        return;
      }

      if (priv->anchor_filename) {
        g_free (priv->anchor_filename);
      }

      priv->anchor_filename = g_strdup (anchor_path);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
g_tls_file_database_android_init (GTlsFileDatabaseAndroid *file_database)
{
}

static gchar *
g_tls_file_database_android_create_certificate_handle (GTlsDatabase *database,
    GTlsCertificate *certificate)
{
  GTlsFileDatabaseAndroid *file_database = G_TLS_FILE_DATABASE_ANDROID (database);
  GTlsDatabaseAndroid *android_database = G_TLS_DATABASE_ANDROID (database);
  GTlsFileDatabaseAndroidPrivate *priv;
  GBytes *der;
  gboolean contains;
  gchar *handle = NULL;

  priv = g_tls_file_database_android_get_instance_private (file_database);

  der = g_tls_certificate_android_get_cert_bytes (G_TLS_CERTIFICATE_ANDROID (certificate));
  g_return_val_if_fail (der != NULL, FALSE);

  g_mutex_lock (&android_database->mutex);
  /* At the same time look up whether this certificate is in list */
  contains = g_hash_table_lookup (android_database->complete, der) ? TRUE : FALSE;
  g_mutex_unlock (&android_database->mutex);

  /* Certificate is in the database */
  if (contains) {
    gchar *prefix = g_filename_to_uri (priv->anchor_filename, NULL, NULL);

    if (!prefix)
      goto out;

    handle = g_tls_database_android_create_handle_for_certificate (prefix, der);

    g_free (prefix);
  }

out:
  g_bytes_unref (der);
  return handle;
}

static void
g_tls_file_database_android_class_init (GTlsFileDatabaseAndroidClass *klass)
{
  GTlsDatabaseClass *database_class = G_TLS_DATABASE_CLASS (klass);
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->get_property = g_tls_file_database_android_get_property;
  gobject_class->set_property = g_tls_file_database_android_set_property;
  gobject_class->finalize = g_tls_file_database_android_finalize;

  database_class->create_certificate_handle = g_tls_file_database_android_create_certificate_handle;

  g_object_class_override_property (gobject_class, PROP_ANCHORS, "anchors");
}

static void
g_tls_file_database_android_file_database_interface_init (GTlsFileDatabaseInterface *iface)
{
}

static gboolean
g_tls_file_database_android_initable_init (GInitable *initable,
    GCancellable *cancellable, GError **error)
{
  GTlsFileDatabaseAndroid *file_database = G_TLS_FILE_DATABASE_ANDROID (initable);
  GTlsFileDatabaseAndroidPrivate *priv;
  gboolean result;

  priv = g_tls_file_database_android_get_instance_private (file_database);

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return FALSE;

  if (priv->anchor_filename)
    result = load_anchor_file (file_database, priv->anchor_filename, error);
  else
    result = TRUE;

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    result = FALSE;

  return result;
}

static void
g_tls_file_database_android_initable_interface_init (GInitableIface *iface)
{
  iface->init = g_tls_file_database_android_initable_init;
}
