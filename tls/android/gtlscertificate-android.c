/*
 * gtlscertificate-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <string.h>

#include "gtlscertificate-android.h"

#include "gtlsenumeration-android.h"
#include "gtlskeystore-android.h"
#include "gtlstrustmanagerfactory-android.h"

#include "gtlsjniutils.h"
#include "gtlsutils-android.h"
#include "gtlsthrowable-android.h"

#define CERT_PREFIX "-----BEGIN CERTIFICATE-----\n"
#define CERT_SUFFIX "\n-----END CERTIFICATE-----"
#define KEY_PREFIX "-----BEGIN PRIVATE KEY-----\n"
#define KEY_SUFFIX "\n-----END PRIVATE KEY-----"
/* XXX: PKCS#1 (BEGIN/END RSA PRIVATE KEY) doesn't seem to work!
 * Can't find API for PKCS#1 */

static struct {
  /* method/class caching */
  jclass klass;
  jmethodID get_encoded;
  jmethodID to_string;

  jclass x509_certificate_class;
  jmethodID x509_certificate_get_subject_x500_principal;
  jmethodID x509_certificate_get_issuer_x500_principal;
  jmethodID x509_certificate_check_validity;
  jmethodID x509_certificate_get_subject_alternative_names;

  jclass x500_principal_class;
  jmethodID x500_principal_get_encoded;
  jmethodID x500_principal_get_name;

  jclass pkcs8_encoded_key_spec_class;
  jmethodID pkcs8_encoded_key_spec_constructor;

  jclass key_factory_class;
  jmethodID key_factory_get_instance;
  jmethodID key_factory_generate_private;

  jclass certificate_factory_class;
  jmethodID certificate_factory_get_instance;
  jmethodID certificate_factory_generate_certificate;

  jclass byte_array_input_stream_class;
  jmethodID byte_array_input_stream_constructor;

  jclass x509_trust_manager_class;
  jmethodID x509_trust_manager_check_client_trusted;
  jmethodID x509_trust_manager_check_server_trusted;

  jclass certificate_exception_class;
  jclass certificate_expired_exception_class;
  jclass certificate_not_yet_valid_exception_class;
  /* XXX: API 24 level
  jclass certificate_revoked_exception_class; */

  jclass sun_security_validator_validator_exception_class;

  jclass collection_class;
  jmethodID collection_iterator;
  jmethodID collection_size;

  jclass iterator_class;
  jmethodID iterator_has_next;
  jmethodID iterator_next;

  jclass list_class;
  jmethodID list_size;
  jmethodID list_get;

  jclass number_class;
  jmethodID number_int_value;
} default_certificate;

gboolean
certificate_initialize (GError ** error)
{
  JNIEnv *env = g_tls_jni_get_env ();

  /* java.security.cert.Certificate */
  if (!(default_certificate.klass =
      g_tls_jni_get_class (env,
                           error,
                           "java/security/cert/Certificate"))) {
    g_critical ("Could not find \'java.security.cert.Certificate\' class");
    return FALSE;
  }

  if (!(default_certificate.get_encoded =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.klass,
                               "getEncoded",
                               "()[B"))) {
    g_critical ("Could not find \'byte[] getEncoded()\' on "
        "\'java.security.cert.Certificate\'");
    return FALSE;
  }

  if (!(default_certificate.to_string =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.klass,
                               "toString",
                               "()Ljava/lang/String;"))) {
    g_critical ("Could not find \'String toString()\' on "
        "\'java.security.cert.Certificate\'");
    return FALSE;
  }

  /* java.security.spec.PKCS8EncodedKeySpec */
  if (!(default_certificate.pkcs8_encoded_key_spec_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/security/spec/PKCS8EncodedKeySpec"))) {
    g_critical ("Could not find \'java.security.spec.PKCS8EncodedKeySpec\' class");
    return FALSE;
  }

  if (!(default_certificate.pkcs8_encoded_key_spec_constructor =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.pkcs8_encoded_key_spec_class,
                               "<init>",
                               "([B)V"))) {
    g_critical ("Could not find \'constructor\' on "
        "\'java.security.spec.PKCS8EncodedKeySpec\'");
    return FALSE;
  }

  /* java.security.KeyFactory */
  if (!(default_certificate.key_factory_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/security/KeyFactory"))) {
    g_critical ("Could not find \'java.security.KeyFactory\' class");
    return FALSE;
  }

  if (!(default_certificate.key_factory_get_instance =
      g_tls_jni_get_static_method_id (env,
                               error,
                               default_certificate.key_factory_class,
                               "getInstance",
                               "(Ljava/lang/String;)Ljava/security/KeyFactory;"))) {
    g_critical ("Could not find \'KeyFactory getInstance(String)\' on "
        "\'java.security.KeyFactory\'");
    return FALSE;
  }

  if (!(default_certificate.key_factory_generate_private =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.key_factory_class,
                               "generatePrivate",
                               "(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;"))) {
    g_critical ("Could not find \'PrivateKey generatePrivate(KeySpec)\' on "
        "\'java.security.PrivateKey\'");
    return FALSE;
  }

  /* java.security.cert.CertificateFactory */
  if (!(default_certificate.certificate_factory_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/security/cert/CertificateFactory"))) {
    g_critical ("Could not find \'java.security.cert.CertificateFactory\' class");
    return FALSE;
  }

  if (!(default_certificate.certificate_factory_get_instance =
      g_tls_jni_get_static_method_id (env,
                               error,
                               default_certificate.certificate_factory_class,
                               "getInstance",
                               "(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;"))) {
    g_critical ("Could not find \'CertificateFactory getInstance(String)\' on "
        "\'java.security.cert.CertificateFactory\'");
    return FALSE;
  }

  if (!(default_certificate.certificate_factory_generate_certificate =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.certificate_factory_class,
                               "generateCertificate",
                               "(Ljava/io/InputStream;)Ljava/security/cert/Certificate;"))) {
    g_critical ("Could not find \'Certificate generateCertificate(InputStream)\' on "
        "\'java.security.cert.CertificateFactory\'");
    return FALSE;
  }

  /* java.io.ByteArrayInputStream */
  if (!(default_certificate.byte_array_input_stream_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/io/ByteArrayInputStream"))) {
    g_critical ("Could not find \'java.io.ByteArrayInputStream\' class");
    return FALSE;
  }

  if (!(default_certificate.byte_array_input_stream_constructor =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.byte_array_input_stream_class,
                               "<init>",
                               "([B)V"))) {
    g_critical ("Could not find \'constructor\' on "
        "\'java.io.ByteArrayInputStream\'");
    return FALSE;
  }

  /* java.security.cert.X509Certificate */
  if (!(default_certificate.x509_certificate_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/security/cert/X509Certificate"))) {
    g_critical ("Could not find \'java.security.cert.X509Certificate\' class");
    return FALSE;
  }

  if (!(default_certificate.x509_certificate_get_subject_x500_principal =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.x509_certificate_class,
                               "getSubjectX500Principal",
                               "()Ljavax/security/auth/x500/X500Principal;"))) {
    g_critical ("Could not find \'X500Principal getSubjectX500Principal()\' on "
        "\'java.security.cert.X509Certificate\'");
    return FALSE;
  }

  if (!(default_certificate.x509_certificate_get_issuer_x500_principal =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.x509_certificate_class,
                               "getIssuerX500Principal",
                               "()Ljavax/security/auth/x500/X500Principal;"))) {
    g_critical ("Could not find \'X500Principal getIssuerX500Principal()\' on "
        "\'java.security.cert.X509Certificate\'");
    return FALSE;
  }

  if (!(default_certificate.x509_certificate_check_validity =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.x509_certificate_class,
                               "checkValidity",
                               "()V"))) {
    g_critical ("Could not find \'void checkValidity()\' on "
        "\'java.security.cert.X509Certificate\'");
    return FALSE;
  }

  if (!(default_certificate.x509_certificate_get_subject_alternative_names =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.x509_certificate_class,
                               "getSubjectAlternativeNames",
                               "()Ljava/util/Collection;"))) {
    g_critical ("Could not find \'Collection getSubjectAlternativeNames()\' on "
        "\'java.security.cert.X509Certificate\'");
    return FALSE;
  }

  /* javax.security.auth.x500.X500Principal */
  if (!(default_certificate.x500_principal_class =
      g_tls_jni_get_class (env,
                           error,
                           "javax/security/auth/x500/X500Principal"))) {
    g_critical ("Could not find \'javax.security.auth.x500.X500Principal\' class");
    return FALSE;
  }

  if (!(default_certificate.x500_principal_get_encoded =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.x500_principal_class,
                               "getEncoded",
                               "()[B"))) {
    g_critical ("Could not find \'byte[] getEncoded()\' on "
        "\'javax.security.auth.x500.X500Principal\'");
    return FALSE;
  }

  if (!(default_certificate.x500_principal_get_name =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.x500_principal_class,
                               "getName",
                               "()Ljava/lang/String;"))) {
    g_critical ("Could not find \'String getName()\' on "
        "\'javax.security.auth.x500.X500Principal\'");
    return FALSE;
  }

  /* javax.net.ssl.X509TrustManager */
  if (!(default_certificate.x509_trust_manager_class =
      g_tls_jni_get_class (env,
                           error,
                           "javax/net/ssl/X509TrustManager"))) {
    g_critical ("Could not find \'javax.net.ssl.X509TrustManager\' class");
    return FALSE;
  }

  if (!(default_certificate.x509_trust_manager_check_client_trusted =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.x509_trust_manager_class,
                               "checkClientTrusted",
                               "([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V"))) {
    g_critical ("Could not find \'void checkClientTrusted(X509Certificate[], String)\' on "
        "\'javax.net.ssl.X509TrustManager\'");
    return FALSE;
  }

  if (!(default_certificate.x509_trust_manager_check_server_trusted =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.x509_trust_manager_class,
                               "checkServerTrusted",
                               "([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V"))) {
    g_critical ("Could not find \'void checkServerTrusted(X509Certificate[], String)\' on "
        "\'javax.net.ssl.X509TrustManager\'");
    return FALSE;
  }

  /* java.security.cert.CertificateException */
  if (!(default_certificate.certificate_exception_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/security/cert/CertificateException"))) {
    g_critical ("Could not find \'java.security.cert.CertificateException\' class");
    return FALSE;
  }

  /* java.security.cert.CertificateExpiredException */
  if (!(default_certificate.certificate_expired_exception_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/security/cert/CertificateExpiredException"))) {
    g_critical ("Could not find \'java.security.cert.CertificateExpiredException\' class");
    return FALSE;
  }

  /* java.security.cert.CertificateNotYetValidException */
  if (!(default_certificate.certificate_not_yet_valid_exception_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/security/cert/CertificateNotYetValidException"))) {
    g_critical ("Could not find \'java.security.cert.CertificateNotYetValidException\' class");
    return FALSE;
  }

  /* java.security.cert.CertificateRevokedException */
/* API 24
  if (!(default_certificate.certificate_revoked_exception_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/security/cert/CertificateRevokedException"))) {
    g_critical ("Could not find \'java.security.cert.CertificateRevokedException\' class");
    return FALSE;
  }*/

  /* sun.security.validator.ValidatorException */
  if (!(default_certificate.sun_security_validator_validator_exception_class =
      g_tls_jni_get_class (env,
                           error,
                           "sun/security/validator/ValidatorException"))) {
    g_debug ("Could not find \'sun.security.validator.ValidatorException\' class");
    /* non-fatal */
  }

  if (!(default_certificate.collection_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/util/Collection"))) {
    g_critical ("Could not find \'java.util.Collection\' class");
    return FALSE;
  }

  if (!(default_certificate.collection_iterator =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.collection_class,
                               "iterator",
                               "()Ljava/util/Iterator;"))) {
    g_critical ("Could not find \'Iterator iterator()\' on "
        "\'java.util.Collection\'");
    return FALSE;
  }

  if (!(default_certificate.collection_size =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.collection_class,
                               "size",
                               "()I"))) {
    g_critical ("Could not find \'int size()\' on "
        "\'java.util.Collection\'");
    return FALSE;
  }

  if (!(default_certificate.iterator_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/util/Iterator"))) {
    g_critical ("Could not find \'java.util.Iterator\' class");
    return FALSE;
  }

  if (!(default_certificate.iterator_has_next =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.iterator_class,
                               "hasNext",
                               "()Z"))) {
    g_critical ("Could not find \'boolean hasNext()\' on "
        "\'java.util.Iterator\'");
    return FALSE;
  }

  if (!(default_certificate.iterator_next =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.iterator_class,
                               "next",
                               "()Ljava/lang/Object;"))) {
    g_critical ("Could not find \'Object next()\' on "
        "\'java.util.Iterator\'");
    return FALSE;
  }

  if (!(default_certificate.list_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/util/List"))) {
    g_critical ("Could not find \'java.util.List\' class");
    return FALSE;
  }

  if (!(default_certificate.list_size =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.list_class,
                               "size",
                               "()I"))) {
    g_critical ("Could not find \'int size()\' on "
        "\'java.util.List\'");
    return FALSE;
  }

  if (!(default_certificate.list_get =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.list_class,
                               "get",
                               "(I)Ljava/lang/Object;"))) {
    g_critical ("Could not find \'Object get(int)\' on "
        "\'java.util.List\'");
    return FALSE;
  }

  if (!(default_certificate.number_class =
      g_tls_jni_get_class (env,
                           error,
                           "java/lang/Number"))) {
    g_critical ("Could not find \'java.lang.Number\' class");
    return FALSE;
  }

  if (!(default_certificate.number_int_value =
      g_tls_jni_get_method_id (env,
                               error,
                               default_certificate.number_class,
                               "intValue",
                               "()I"))) {
    g_critical ("Could not find \'int intValue()\' on "
        "\'java.lang.Number\'");
    return FALSE;
  }

  return TRUE;
}

typedef struct _GTlsCertificateAndroidPrivate
{
  GTlsCertificate *issuer;

  gchar *cert_pem;

  jobject certificate;
  jobject private_key;

  GError *construct_error;
} GTlsCertificateAndroidPrivate;

enum
{
  PROP_0,

  PROP_CERTIFICATE,
  PROP_CERTIFICATE_PEM,
  PROP_PRIVATE_KEY,
  PROP_PRIVATE_KEY_PEM,
  PROP_ISSUER
};

static void     g_tls_certificate_android_initable_iface_init (GInitableIface  *iface);

G_DEFINE_TYPE_WITH_CODE (GTlsCertificateAndroid, g_tls_certificate_android, G_TYPE_TLS_CERTIFICATE,
                         G_ADD_PRIVATE (GTlsCertificateAndroid)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                g_tls_certificate_android_initable_iface_init))

static void
g_tls_certificate_android_finalize (GObject *object)
{
  GTlsCertificateAndroid *android = G_TLS_CERTIFICATE_ANDROID (object);
  GTlsCertificateAndroidPrivate *priv;
  JNIEnv *env = g_tls_jni_get_env ();

  priv = g_tls_certificate_android_get_instance_private (android);

  g_clear_object (&priv->issuer);

  if (priv->certificate)
    g_tls_jni_object_unref (env, priv->certificate);
  priv->certificate = NULL;

  if (priv->private_key)
    g_tls_jni_object_unref (env, priv->private_key);
  priv->private_key = NULL;

  g_free (priv->cert_pem);
  priv->cert_pem = NULL;

  G_OBJECT_CLASS (g_tls_certificate_android_parent_class)->finalize (object);
}

static GByteArray *
_get_certificate_encoded (GTlsCertificateAndroid *android)
{
  GTlsCertificateAndroidPrivate *priv;
  GByteArray *certificate = NULL;
  JNIEnv *env = g_tls_jni_get_env ();

  priv = g_tls_certificate_android_get_instance_private (android);

  if (priv->certificate) {
    gint8 *cert_data = NULL;
    gsize len = 0;

    if (!g_tls_jni_call_byte_array_method (env, NULL, priv->certificate,
          default_certificate.get_encoded, &len, &cert_data, NULL)) {
      g_warn_if_reached ();
      return NULL;
    }

    certificate = g_byte_array_new_take ((guint8 *) cert_data, len);
  }

  return certificate;
}

static void
g_tls_certificate_android_get_property (GObject *object, guint prop_id,
    GValue *value, GParamSpec *pspec)
{
  GTlsCertificateAndroid *android = G_TLS_CERTIFICATE_ANDROID (object);
  GTlsCertificateAndroidPrivate *priv;

  priv = g_tls_certificate_android_get_instance_private (android);

  switch (prop_id)
    {
    case PROP_CERTIFICATE:{
      g_value_take_boxed (value, _get_certificate_encoded (android));
      break;
    }
    case PROP_CERTIFICATE_PEM:{
      if (priv->cert_pem) {
        g_value_set_string (value, priv->cert_pem);
      } else if (priv->certificate) {
        GByteArray *encoded = _get_certificate_encoded (android);
        gchar *ret, *pem;

        pem = (gchar *) der_to_pem (encoded, NULL);
        ret =  g_strconcat (CERT_PREFIX, pem, CERT_SUFFIX, NULL);
        g_free (pem);

        g_value_take_string (value, ret);
      }
      break;
    }
    case PROP_ISSUER:
      g_value_set_object (value, priv->issuer);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
_load_cert_from_der (GTlsCertificateAndroid * android, jbyteArray der)
{
  GTlsCertificateAndroidPrivate *priv;
  JNIEnv *env = g_tls_jni_get_env ();
  jobject data_stream = NULL;
  jobject cf = NULL;
  jstring instance = NULL;
  jobject cert;

  priv = g_tls_certificate_android_get_instance_private (android);

  data_stream = g_tls_jni_new_object (env, NULL, FALSE,
      default_certificate.byte_array_input_stream_class,
      default_certificate.byte_array_input_stream_constructor, der);
  if (!data_stream)
    goto out;

  instance = g_tls_jni_string_from_gchar (env, NULL, FALSE, "X.509");

  if (!g_tls_jni_call_static_object_method (env, NULL,
            default_certificate.certificate_factory_class,
            default_certificate.certificate_factory_get_instance,
            &cf,
            instance)) {
    g_critical ("Failed to get CertificateFactory \'X.509\' instance");
    goto out;
  }

  if (!g_tls_jni_call_object_method (env, NULL,
            cf,
            default_certificate.certificate_factory_generate_certificate,
            &cert,
            data_stream)) {
    g_critical ("Failed to get generate certificate");
    goto out;
  }

  priv->certificate = g_tls_jni_object_make_global (env, cert);

out:
  if (data_stream)
    g_tls_jni_object_local_unref (env, data_stream);
  if (instance)
    g_tls_jni_object_local_unref (env, instance);
  if (cf)
    g_tls_jni_object_local_unref (env, cf);
}

static void
_load_private_key_from_der (GTlsCertificateAndroid * android, jbyteArray der)
{
  GTlsCertificateAndroidPrivate *priv;
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject keyspec = NULL;
  jobject key = NULL;
  static const gchar *key_types[] = { "RSA", "DSA", "EC" };
  int i;

  /* FIXME: also do rsa private keys? */

  priv = g_tls_certificate_android_get_instance_private (android);

  keyspec = g_tls_jni_new_object (env, &error, FALSE,
      default_certificate.pkcs8_encoded_key_spec_class,
      default_certificate.pkcs8_encoded_key_spec_constructor,
      der);
  if (!keyspec) {
    g_critical ("Could not construct KeySpec from DER: %s", error->message);
    goto out;
  }

  for (i = 0; i < G_N_ELEMENTS (key_types); i++) {
    const gchar *key_type = key_types[i];
    jobject key_instance;
    jobject keyfactory;

    key_instance = g_tls_jni_string_from_gchar (env, &error, FALSE, key_type);

    if (!g_tls_jni_call_static_object_method (env, NULL,
              default_certificate.key_factory_class,
              default_certificate.key_factory_get_instance,
              &keyfactory,
              key_instance)) {
      g_debug ("Could not get %s instance from KeyFactory: %s", key_type, error->message);
      g_tls_jni_object_local_unref (env, key_instance);
      g_clear_error (&error);
      continue;
    }
    g_tls_jni_object_local_unref (env, key_instance);

    if (!g_tls_jni_call_object_method (env, &error,
              keyfactory,
              default_certificate.key_factory_generate_private,
              &key,
              keyspec)) {
      g_debug ("Could not generate private %s key on KeyFactory: %s", key_type, error->message);
      g_tls_jni_object_local_unref (env, keyfactory);
      g_clear_error (&error);
      continue;
    }
    g_tls_jni_object_local_unref (env, keyfactory);

    if (!key) {
      g_debug ("Invalid key returned");
      continue;
    }

    break;
  }

  if (key) {
    priv->private_key = g_tls_jni_object_make_global (env, key);
  } else {
    g_warning ("Could not load private key");
  }

out:
  if (keyspec)
    g_tls_jni_object_local_unref (env, keyspec);
}

static const char *
strip_prefix_suffix (const char * data, gsize len, const char * prefix, const char * suffix, gsize * out_len)
{
  char *p_begin, *p_end;

  p_begin = g_strstr_len (data, len, prefix);
  if (p_begin) {
    gsize start_index = (data - p_begin) + strlen (prefix);
    p_end = g_strstr_len (&p_begin[strlen(prefix)], len - start_index, suffix);
    if (p_end) {
      *out_len = p_end - p_begin - strlen (prefix);
      return p_begin + strlen(prefix);
    } else {
      *out_len = len;
      return data;
    }
  } else { 
    *out_len = len;
    return data;
  } 
}

static void
g_tls_certificate_android_set_property (GObject *object,
    guint prop_id, const GValue *value, GParamSpec *pspec)
{
  GTlsCertificateAndroid *android = G_TLS_CERTIFICATE_ANDROID (object);
  GTlsCertificateAndroidPrivate *priv;
  JNIEnv *env = g_tls_jni_get_env ();

  priv = g_tls_certificate_android_get_instance_private (android);

  switch (prop_id) {
    case PROP_CERTIFICATE:{
      GByteArray *bytes = g_value_get_boxed (value);
      jbyteArray encoded;

      if (!bytes)
        break;

      g_return_if_fail (priv->certificate == NULL);

      encoded = jni_byte_array_from_data (bytes->data, bytes->len);

      _load_cert_from_der (android, encoded);

      g_tls_jni_object_local_unref (env, encoded);
      break;
    }
    case PROP_CERTIFICATE_PEM:{
      const gchar *pem = g_value_get_string (value);
      gsize stripped_len = 0;
      const gchar *stripped;
      jbyteArray der;

      if (!pem) {
        g_free (priv->cert_pem);
        priv->cert_pem = NULL;
        break;
      }

      stripped = strip_prefix_suffix (pem, strlen(pem), CERT_PREFIX, CERT_SUFFIX, &stripped_len);

      if (!pem_to_der ((const guint8 *) stripped, stripped_len, &der)) {
        g_warn_if_reached ();
        break;
      }

      _load_cert_from_der (android, der);

      g_free (priv->cert_pem);
      priv->cert_pem = g_strdup (pem);

      g_tls_jni_object_local_unref (env, der);
      break;
    }
    case PROP_PRIVATE_KEY:{
      jbyteArray encoded;

      GByteArray *bytes = g_value_get_boxed (value);
      if (!bytes)
        break;

      encoded = jni_byte_array_from_data (bytes->data, bytes->len);

      _load_private_key_from_der (android, encoded);

      g_tls_jni_object_local_unref (env, encoded);
      break;
    }
    case PROP_PRIVATE_KEY_PEM:{
      const gchar *pem = g_value_get_string (value);
      gsize stripped_len = 0;
      const gchar *stripped;
      jbyteArray der = NULL;

      if (priv->private_key)
        return;

      g_assert (!pem || priv->certificate);
      if (!pem)
        return;

      stripped = strip_prefix_suffix (pem, strlen(pem), KEY_PREFIX, KEY_SUFFIX, &stripped_len);

      if (!pem_to_der ((guint8 *) stripped, stripped_len, &der)) {
        g_warn_if_reached ();
        return;
      }

      _load_private_key_from_der (android, der);

      g_tls_jni_object_local_unref (env, der);
      break;
    }
    case PROP_ISSUER:
      priv->issuer = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
g_tls_certificate_android_init (GTlsCertificateAndroid *android)
{
}

static gboolean
g_tls_certificate_android_initable_init (GInitable       *initable,
                                         GCancellable    *cancellable,
                                         GError         **error)
{
  GTlsCertificateAndroid *android = G_TLS_CERTIFICATE_ANDROID (initable);
  GTlsCertificateAndroidPrivate *priv;

  priv = g_tls_certificate_android_get_instance_private (android);

  if (priv->construct_error) {
    g_propagate_error (error, priv->construct_error);
    priv->construct_error = NULL;
    return FALSE;
  } else if (!priv->certificate) {
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_BAD_CERTIFICATE,
                         "No certificate data provided");
    return FALSE;
  } else {
    return TRUE;
  }
}

static jobjectArray
certificate_build_chain (GTlsCertificateAndroid * android)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsCertificate *iter;
  jobjectArray ret;
  gsize i, n = 0;

  for (iter = G_TLS_CERTIFICATE (android); iter; iter = g_tls_certificate_get_issuer (iter)) {
    n++;
  }

  ret = (*env)->NewObjectArray (env, n, default_certificate.x509_certificate_class, NULL);
  if (!ret)
    return NULL;

  iter = G_TLS_CERTIFICATE (android);
  for (i = 0; i < n && iter; i++) {
    (*env)->SetObjectArrayElement (env, ret, i, g_tls_certificate_android_get_cert (G_TLS_CERTIFICATE_ANDROID (iter)));
    iter = g_tls_certificate_get_issuer (iter);
  }

  return ret;
}

static gsize
key_store_get_n_certificates (jobject keystore)
{
  JNIEnv *env = g_tls_jni_get_env ();
  jobject aliases;
  gsize n = 0;

  if (!keystore)
    return 0;

  aliases = keystore_aliases (keystore);
  while (enumeration_has_more_elements (aliases)) {
    jobject alias = enumeration_next_element (aliases);
    if (keystore_is_certificate_entry (keystore, alias))
      n++;
    g_tls_jni_object_local_unref (env, alias);
  }

  g_tls_jni_object_local_unref (env, aliases);

  return n;
}

static jobject
key_store_from_certificate (GTlsCertificate *trusted)
{
  JNIEnv *env = g_tls_jni_get_env ();
  jobject keystore_type;
  jobject keystore;
  jobject certificate;
  jobject alias;

  if (!trusted)
    return NULL;

  keystore_type = keystore_get_default_type ();
  keystore = keystore_get_instance (keystore_type);
  keystore_load (keystore, NULL);

  alias = g_tls_jni_string_from_gchar (env, NULL, FALSE, "cert");
  certificate = g_tls_certificate_android_get_cert (G_TLS_CERTIFICATE_ANDROID (trusted));
  keystore_set_certificate_entry (keystore, alias, certificate);

  g_tls_jni_object_local_unref (env, keystore_type);
  g_tls_jni_object_local_unref (env, alias);

  return keystore;
}

static GTlsCertificateFlags
possible_exception_to_cert_flags (GTlsCertificateAndroid *android)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsCertificateFlags gtls_flags = 0;

  if ((*env)->ExceptionCheck (env)) {
    jthrowable exception;

    if ((exception = (*env)->ExceptionOccurred (env))) {
      gboolean unknown_exception = FALSE;
      gboolean unknown_validator_exception = FALSE;

      (*env)->ExceptionClear (env);

      while (exception) {
        if ((*env)->IsInstanceOf (env, exception, default_certificate.certificate_expired_exception_class)) {
          gtls_flags |= G_TLS_CERTIFICATE_EXPIRED;
          (*env)->DeleteLocalRef (env, exception);
          exception = NULL;
          unknown_validator_exception = unknown_exception = FALSE;
        } else if ((*env)->IsInstanceOf (env, exception, default_certificate.certificate_not_yet_valid_exception_class)) {
          gtls_flags |= G_TLS_CERTIFICATE_NOT_ACTIVATED;
          (*env)->DeleteLocalRef (env, exception);
          exception = NULL;
          unknown_validator_exception = unknown_exception = FALSE;
/* API 24
        } else if ((*env)->IsInstanceOf (env, exception, default_certificate.certificate_revoked_exception_class)) {
          gtls_flags |= G_TLS_CERTIFICATE_REVOKED;
          (*env)->DeleteLocalRef (env, exception);
          exception = NULL;
          unknown_validator_exception = unknown_exception = FALSE;*/
        } else {
          /* some other error? */
          gchar *exception_description = getExceptionSummary (env, exception);
          gchar *exception_stacktrace = getStackTrace (env, exception);
          jthrowable cause;

          g_debug ("exception: %s\n%s",
              exception_description, exception_stacktrace);

          if (default_certificate.sun_security_validator_validator_exception_class &&
              (*env)->IsInstanceOf (env, exception, default_certificate.sun_security_validator_validator_exception_class)) {
            if (!unknown_exception) {
              g_debug ("Sun path validator: assuming UNKNOWN_CA unless cause shows something different");
              unknown_validator_exception = TRUE;
            }
          } else {
            if (!unknown_validator_exception)
              unknown_exception = TRUE;
          }

          cause = throwable_get_cause (exception);
          (*env)->DeleteLocalRef (env, exception);
          exception = cause;

          g_free (exception_description);
          g_free (exception_stacktrace);
        }
      }

      if (unknown_validator_exception)
        gtls_flags |= G_TLS_CERTIFICATE_UNKNOWN_CA;
      if (unknown_exception)
        gtls_flags |= G_TLS_CERTIFICATE_GENERIC_ERROR;

      if (exception)
        g_tls_jni_object_local_unref (env, exception);
    } else {
      (*env)->ExceptionClear (env);
      g_critical ("Unknown exception thrown!");
      return G_TLS_CERTIFICATE_GENERIC_ERROR;
    }
  }

  return gtls_flags;
}

static GTlsCertificateFlags
certificate_chain_verify_validity (GTlsCertificateAndroid *android,
    jobjectArray certificate_chain)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsCertificateFlags gtls_flags = 0;
  gsize i, n_certs;

  n_certs = (*env)->GetArrayLength (env, certificate_chain);
  for (i = 0; i < n_certs; i++) {
    jobject cert = (*env)->GetObjectArrayElement (env, certificate_chain, i);

    (*env)->CallVoidMethod (env, cert, default_certificate.x509_certificate_check_validity);
    gtls_flags |= possible_exception_to_cert_flags (android);
  }

  return gtls_flags;
}

static gboolean
is_ipv4_address (const gchar ** components)
{
  gsize i;

  if (g_strv_length ((gchar **) components) != 4)
    return FALSE;

  for (i = 0; i < 4; i++) {
    const gchar *comp = components[i];
    gsize len = strlen (comp);
    gchar *end = NULL;
    guint64 val;

    if (len > 3 || len == 0)
      return FALSE;

    if (!g_ascii_isdigit (comp[0]))
      return FALSE;

    val = g_ascii_strtoull (comp, &end, 10);
    if (val > 255)
      return FALSE;
    if (!end)
      return FALSE;
    if (end - comp < len)
      /* didn't consume all the component */
      return FALSE;
  }

  return TRUE;
}

static gboolean
rfc2253_is_special (gchar c)
{
  return c == ',' || c == '=' || c == '+' || c == '<' || c == '>' || c == '#' || c == ';';
}

static GHashTable *
rfc2253_string_parse (gchar * from)
{
  GHashTable *ret;
  gchar *pos = from;

  if (!from)
    return NULL;

  ret = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
      (GDestroyNotify) g_strfreev);

  while (pos && pos[0]) {
    gchar *start, *end;
    gchar *key;
    gchar **values;

    /* BNF for an attribute:
     *
     * distinguishedName = [name]                    ; may be empty string
     *
     * name       = name-component *("," name-component)
     *
     * name-component = attributeTypeAndValue *("+" attributeTypeAndValue)
     *
     * attributeTypeAndValue = attributeType "=" attributeValue
     */

    /* start off parsing a type given by the BNF:
     *
     * attributeType = (ALPHA 1*keychar) / oid
     *
     * keychar    = ALPHA / DIGIT / "-"
     *
     * oid        = 1*DIGIT *("." 1*DIGIT)
     */
    while (g_ascii_isspace (pos[0]))
      pos++;
    start = pos;
    if (g_ascii_isalpha (pos[0])) {
      /* have a keychar */
      while (g_ascii_isalnum (pos[0]) || pos[0] == '-')
        pos++;
    } else if (g_ascii_isdigit (pos[0])) {
      /* have an oid */
      while (g_ascii_isdigit (pos[0]) || pos[0] == '.')
        pos++;
    } else {
      goto error;
    }
    end = pos;

    key = g_strndup (start, end - start);
    if (!g_utf8_validate (key, -1, NULL)) {
      /* invalid UTF-8. Try making valid */
      gchar *new = g_utf8_make_valid (key, -1);

      g_free (key);
      key = new;
    }
    {
      /* XXX: depends on current locale */
      gchar *new = g_utf8_strdown (key, -1);
      g_free (key);
      key = new;
    }

    while (g_ascii_isspace (pos[0]))
      pos++;
    if (pos[0] != '=')
      goto error;
    pos++;
    while (g_ascii_isspace (pos[0]))
      pos++;

    /* now parse a value given by the BNF:
     *
     * attributeValue = string
     *
     * string     = *( stringchar / pair )
     *              / "#" hexstring
     *              / QUOTATION *( quotechar / pair ) QUOTATION ; only from v2
     *
     * quotechar     = <any character except "\" or QUOTATION >
     *
     * special    = "," / "=" / "+" / "<" /  ">" / "#" / ";"
     *
     * pair       = "\" ( special / "\" / QUOTATION / hexpair )
     * stringchar = <any character except one of special, "\" or QUOTATION >
     * 
     * hexstring  = 1*hexpair
     * hexpair    = hexchar hexchar
     * 
     * hexchar    = DIGIT / "A" / "B" / "C" / "D" / "E" / "F"
     *              / "a" / "b" / "c" / "d" / "e" / "f"
     *
     * ALPHA      =  <any ASCII alphabetic character>
     *                                         ; (decimal 65-90 and 97-122)
     * DIGIT      =  <any ASCII decimal digit>  ; (decimal 48-57)
     * QUOTATION  =  <the ASCII double quotation mark character '"' decimal 34>
     */
    values = g_new0 (gchar *, 32);
    while (pos[0] && pos[0] != ',') {
      GArray *value_stack;
      gint value_i = 0;

      value_stack = g_array_new (TRUE, TRUE, sizeof (char));

      while (g_ascii_isspace (pos[0]))
        pos++;

      if (pos[0] == '"') {
        while (pos[0] && pos[0] != '+' && pos[0] != ',') {
          if (pos[0] == '"') {
            /* end of the quote */
            break;
          } else if (pos[0] == '\\') {
            /* escape code */
            if (rfc2253_is_special (pos[1]) || pos[1] == '\\' || pos[1] == '"') {
              g_array_append_val (value_stack, pos[1]);
            } else if (g_ascii_isxdigit (pos[1]) && g_ascii_isxdigit (pos[2])) {
              gint high = g_ascii_xdigit_value (pos[1]);
              gint low = g_ascii_xdigit_value (pos[2]);
              gchar hex_value = ((high << 4) | (low & 0xf)) & 0xff;
              g_array_append_val (value_stack, hex_value);
              pos += 2;
            } else {
              /* unknown escape */
              goto error;
            }
            pos++;
          } else {
            g_array_append_val (value_stack, pos[0]);
          }
          pos++;
        }
        if (pos[0] != '"')
          /* error: quote doesn't end */
          goto error;
      } else if (pos[0] == '#') {
        /* FIXME: deal with hex encoded dn */
        goto error;
      } else {
        while (pos[0] && pos[0] != '+' && pos[0] != ',') {
          if (pos[0] == '\\') {
            /* escape code */
            if (rfc2253_is_special (pos[1]) || pos[1] == '\\' || pos[1] == '"') {
              g_array_append_val (value_stack, pos[1]);
            } else if (g_ascii_isxdigit (pos[1]) && g_ascii_isxdigit (pos[2])) {
              gint high = g_ascii_xdigit_value (pos[1]);
              gint low = g_ascii_xdigit_value (pos[2]);
              gchar hex_value = ((high << 4) | (low & 0xf)) & 0xff;
              g_array_append_val (value_stack, hex_value);
              pos += 2;
            } else {
              /* unknown escape */
              goto error;
            }
          } else if (pos[0] == '"') {
            /* FIXME: deal with partial quoting */
            goto error;
          } else if (rfc2253_is_special (pos[0])) {
            break;
          } else {
            g_array_append_val (value_stack, pos[0]);
          }
          pos++;
        }
      }

      values[value_i] = g_array_free (value_stack, FALSE);
      if (!g_utf8_validate (values[value_i], -1, NULL)) {
        /* invalid UTF-8. Try making valid */
        gchar *new = g_utf8_make_valid (values[value_i], -1);

        g_free (values[value_i]);
        values[value_i] = new;
      }
      {
        /* XXX: depends on current locale */
        gchar *new = g_utf8_strdown (values[value_i], -1);
        g_free (values[value_i]);
        values[value_i] = new;
      }
      g_debug ("found %s -> %s", key, values[value_i]);
      while (g_ascii_isspace (pos[0]))
        pos++;

      value_i++;
      g_assert (value_i < 32);
    }

    g_debug ("inserting %s -> %p", key, values);
    g_hash_table_insert (ret, key, values);

    if (pos && pos[0])
      pos++;
  }

out:
  return ret;

error:
  g_hash_table_destroy (ret);
  ret = NULL;
  goto out;
}

static gchar *
certificate_get_common_name (jobject cert)
{
  JNIEnv *env = g_tls_jni_get_env ();
  jobject principal;
  jobject j_name;
  GHashTable *rdns;
  gchar **cns;
  gchar *cn = NULL, *name;
  int cn_len;

  if (!g_tls_jni_call_object_method (env, NULL,
                cert,
                default_certificate.x509_certificate_get_subject_x500_principal,
                &principal)) {
    g_critical ("getSubjectX500Principal() call failed");
    return NULL;
  }

  if (!g_tls_jni_call_object_method (env, NULL,
                principal,
                default_certificate.x500_principal_get_name,
                &j_name))
    return NULL;

  g_tls_jni_object_local_unref (env, principal);

  name = g_tls_jni_string_to_gchar (env, j_name, TRUE);
  rdns = rfc2253_string_parse (name);
  g_free (name);

  if (!rdns)
    return NULL;

  cns = g_hash_table_lookup (rdns, "cn");
  if (!cns) {
    g_hash_table_destroy (rdns);
    return NULL;
  }
  cn_len = g_strv_length (cns);
  if (cn_len > 0)
    cn = g_strdup (cns[cn_len-1]);

  g_hash_table_destroy (rdns);

  return cn;
}

static gboolean
certificate_subject_alt_names (jobject cert, gchar *** ips, gchar *** dns)
{
  JNIEnv *env = g_tls_jni_get_env ();
  gboolean res = TRUE;
  jobject alt_names = NULL, iter = NULL, list = NULL;
  gchar ** res_ip = NULL;
  gchar ** res_dns = NULL;
  int alt_size = 0, res_ip_i = 0, res_dns_i = 0;
  gboolean have_next = FALSE;

  if (!g_tls_jni_call_object_method (env,
                                NULL,
                                cert,
                                default_certificate.x509_certificate_get_subject_alternative_names,
                                &alt_names)) {
    goto error;
  }

  if (!alt_names) {
  /* +2 for the CN and the NULL senitel */
    if (ips)
      res_ip = g_new0 (gchar *, 1);
    if (dns)
      res_dns = g_new0 (gchar *, 2);
  } else {
    if (!g_tls_jni_call_int_method (env,
                                    NULL,
                                    alt_names,
                                    default_certificate.collection_size,
                                    &alt_size)) {
      goto error;
    }
    if (alt_size < 0) {
      goto error;
    }

    /* +2 for the CN and the NULL senitel */
    if (ips)
      res_ip = g_new0 (gchar *, alt_size + 2);
    if (dns)
      res_dns = g_new0 (gchar *, alt_size + 2);

    if (!g_tls_jni_call_object_method (env,
                                      NULL,
                                      alt_names,
                                      default_certificate.collection_iterator,
                                      &iter)) {
      goto error;
    }

    do {
      int list_size;

      if (!g_tls_jni_call_object_method (env,
                                        NULL,
                                        iter,
                                        default_certificate.iterator_next,
                                        &list)) {
        goto error;
      }

      if (!g_tls_jni_call_int_method (env,
                                        NULL,
                                        list,
                                        default_certificate.list_size,
                                        &list_size)) {
        goto error;
      }

      if (list_size == 2) {
        jobject type = NULL, value = NULL;
        int type_int;

        if (!g_tls_jni_call_object_method (env,
                                        NULL,
                                        list,
                                        default_certificate.list_get,
                                        &type,
                                        0)) {
          goto error;
        }

        if (!g_tls_jni_call_object_method (env,
                                        NULL,
                                        list,
                                        default_certificate.list_get,
                                        &value,
                                        1)) {
          if (type)
            g_tls_jni_object_local_unref (env, type);
          goto error;
        }

        if (g_tls_jni_call_int_method (env,
                                        NULL,
                                        type,
                                        default_certificate.number_int_value,
                                        &type_int)) {
          gchar *alt_name = NULL;
          if (type_int == 2 && res_dns) {
            /* dns name */
            if ((alt_name = g_tls_jni_string_to_gchar (env, value, TRUE))) {
              res_dns[res_dns_i++] = alt_name;
            }
          } else if (type_int == 7 && res_ip) {
            /* ip address */
            if ((alt_name = g_tls_jni_string_to_gchar (env, value, TRUE))) {
              res_ip[res_ip_i++] = alt_name;
            }
          }
        }
        if (type)
          g_tls_jni_object_local_unref (env, type);
      }

      g_tls_jni_object_local_unref (env, list);
      list = NULL;

      if (!g_tls_jni_call_boolean_method (env,
                                        NULL,
                                        iter,
                                        default_certificate.iterator_has_next,
                                        &have_next)) {
        goto error;
      }
    } while (have_next);
  }

  if (res_dns && res_dns_i == 0) {
    /* add the common name from the cert if there are no dns subject alts */
    res_dns[res_dns_i++] = certificate_get_common_name (cert);
  }

  g_assert (res_ip_i < alt_size + 2);
  g_assert (res_dns_i < alt_size + 2);

  if (ips) {
    if (res_ip)
      res_ip[res_ip_i] = NULL;
    *ips = res_ip;
  }
  if (dns) {
    if (res_dns)
      res_dns[res_dns_i] = NULL;
    *dns = res_dns;
  }

out:
  if (list)
    g_tls_jni_object_local_unref (env, list);
  if (alt_names)
    g_tls_jni_object_local_unref (env, alt_names);
  if (iter)
    g_tls_jni_object_local_unref (env, iter);
  return res;

error:
  if (res_ip)
    g_strfreev (res_ip);
  if (res_dns)
    g_strfreev (res_dns);
  res = FALSE;
  goto out;
}

static GTlsCertificateFlags
dns_match_with_wildcards (gchar ** cert_dns, const gchar * server_name)
{
  gchar **server_components = NULL;
  GTlsCertificateFlags ret = 0;
  gint i, comp_length, dns_length;

  if (!cert_dns)
    return G_TLS_CERTIFICATE_BAD_IDENTITY;

  server_components = g_strsplit (server_name, ".", -1);
  if (!server_components)
    goto failed;

  comp_length = g_strv_length (server_components);
  dns_length = g_strv_length (cert_dns);

  for (i = 0; i < dns_length; i++) {
    const gchar *dns_entry = cert_dns[i];
    gchar **dns_comp = NULL;
    gint dns_comp_length = 0;
    int j;

    if (g_strcmp0 (dns_entry, "") == 0)
      continue;

    dns_comp = g_strsplit (dns_entry, ".", -1);
    if (!dns_comp)
      goto failed;

    dns_comp_length = g_strv_length (dns_comp);

    if (dns_comp_length < 2)
      /* require at least two components */
      goto next;

    if (dns_comp_length != comp_length)
      goto next;

    /* all but the first component must be equal */
    for (j = 1; j < dns_comp_length; j++) {
      if (g_strcmp0 (dns_comp[j], server_components[j]) != 0) {
        goto next;
      }
    }

    /* deal with the first entry specially for wildcards */

    if (g_strcmp0 (dns_comp[0], server_components[0]) == 0)
      /* success! all components are equal */
      break;

    if (g_strcmp0 (dns_comp[0], "*") == 0)
      /* success! have a wildcard that matches anything */
      break;

    {
      const gchar *wildcard;
      gchar *prefix, *suffix;
      gsize wildcard_offset;

      wildcard = g_strstr_len (dns_comp[0], -1, "*");
      if (!wildcard)
        /* failure, no wildcard found, everything will fail */
        goto next;

      if (g_strstr_len (&wildcard[1], -1, "*"))
        /* failure, invalid wildcard spec - multiple wildcards */
        goto next;

      if (g_str_has_prefix (server_components[0], "xn--"))
        /* failure. A-labels are not allowed to have wildcards */
        goto next;

      wildcard_offset = wildcard - dns_comp[0];
      prefix = g_strndup (dns_comp[0], wildcard_offset);
      if (strlen (prefix) > 0 && !g_str_has_prefix (server_components[0], prefix)) {
        /* failure, the dns prefix doesn't match */
        g_free (prefix);
        goto next;
      }
      g_free (prefix);

      suffix = g_strndup (&wildcard[1], strlen(dns_comp[0]) - wildcard_offset);
      if (strlen (suffix) > 0 && !g_str_has_prefix (&wildcard[1], suffix)) {
        /* failure, the dns suffix doesn't match */
        g_free (suffix);
        goto next;
      }
      g_free (suffix);

      /* success! no other failure cases */
      break;
    }

next:
    g_strfreev (dns_comp);
  }

  /* exhausted all options and could not find a match */
  if (i >= dns_length)
    ret = G_TLS_CERTIFICATE_BAD_IDENTITY;

out:
  g_strfreev (server_components);
  return ret;

failed:
  ret = G_TLS_CERTIFICATE_GENERIC_ERROR;
  goto out;
}

static GTlsCertificateFlags
verify_certificate_identity (GTlsCertificateAndroid *cert_android,
    gchar *server_name)
{
  GTlsCertificateAndroidPrivate *priv;
  GTlsCertificateFlags ret = 0;
  gchar ** components = NULL;

  priv = g_tls_certificate_android_get_instance_private (cert_android);

  if (!server_name)
    return 0;
  if (g_strcmp0 (server_name, "") == 0)
    goto error;

  components = g_strsplit (server_name, ".", -1);

  if (is_ipv4_address ((const gchar **) components)) {
    gchar **cert_ips;

    if (!certificate_subject_alt_names (priv->certificate, &cert_ips, NULL))
      goto error;

    /* ip addresses must match exactly, no wildcards */
    if (!g_strv_contains ((const gchar * const *) cert_ips, server_name))
      goto failed;

    g_strfreev (cert_ips);
  } else {
    gchar **cert_dns;

    if (!certificate_subject_alt_names (priv->certificate, NULL, &cert_dns))
      goto error;

    ret = dns_match_with_wildcards (cert_dns, server_name);

    g_strfreev (cert_dns);
  }

out:
  g_strfreev (components);
  return ret;

failed:
  ret = G_TLS_CERTIFICATE_BAD_IDENTITY;
  goto out;

error:
  ret = G_TLS_CERTIFICATE_GENERIC_ERROR;
  goto out;
}

GTlsCertificateFlags
g_tls_certificate_android_verify_with_keystore (GTlsCertificateAndroid *cert_android,
    GSocketConnectable *identity, jobject key_store, const gchar * purpose,
    GError ** error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsCertificateFlags gtls_flags = 0;
  gchar *server_name = NULL;
  jobject default_algorithm = NULL;
  jobject trust_factory = NULL;
  jobjectArray managers = NULL;
  jcharArray password = NULL;
  jobject manager = NULL;
  jobject allocated_key_store = NULL;

  if (identity)
    server_name = g_tls_android_socket_connectable_to_string (identity);

  default_algorithm = trust_manager_factory_get_default_algorithm ();
  if (!default_algorithm) {
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "TrustManagerFactory.getDefaultAlgorithm() call failed");
    gtls_flags = G_TLS_CERTIFICATE_GENERIC_ERROR;
    goto out;
  }

  trust_factory = trust_manager_factory_get_instance (default_algorithm);
  if (!trust_factory ) {
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "TrustManagerFactory.getInstance() call failed");
    gtls_flags = G_TLS_CERTIFICATE_GENERIC_ERROR;
    goto out;
  }

  /* we need a valid list of certificates in the keystore otherwise
   * java complains */
  if (key_store_get_n_certificates (key_store) < 1)
    key_store = NULL;

  /* initialize with a keystore for the trusted certificates or if NULL, use
   * the last certificate in the chain and add the UNKNOWN_CA flag */
  if (!key_store) {
    GTlsCertificate *trusted = G_TLS_CERTIFICATE (cert_android);
    GTlsCertificate *iter = trusted;

    while ((iter = g_tls_certificate_get_issuer (iter))) {
      trusted = iter;
    }

    allocated_key_store = key_store = key_store_from_certificate (trusted);
    gtls_flags |= G_TLS_CERTIFICATE_UNKNOWN_CA;
  }

  password = (*env)->NewCharArray (env, 0);
  trust_manager_factory_init (trust_factory, key_store, password);

  managers = trust_manager_factory_get_trust_managers (trust_factory);
  if (!managers) {
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "TrustManagerFactory.getTrustManagers() call failed");
    gtls_flags = G_TLS_CERTIFICATE_GENERIC_ERROR;
    goto out;
  }

  manager = (*env)->GetObjectArrayElement(env, managers, 0);

  if (!(*env)->IsInstanceOf (env, manager, default_certificate.x509_trust_manager_class)) {
    g_critical ("Received an unknown trust manager");
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Unknown trust manager found");
    gtls_flags = G_TLS_CERTIFICATE_GENERIC_ERROR;
    goto out;
  }

  /* we need to call this manually to be able to get a hold of the exception
   * that might occur with bad certificate chains */
  {
    jobjectArray certificate_chain;
    jstring key_type;

    certificate_chain = certificate_build_chain (cert_android);
    if (!certificate_chain) {
      g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_BAD_CERTIFICATE,
          "Could not build certificate chain");
      gtls_flags = G_TLS_CERTIFICATE_GENERIC_ERROR;
      goto out;
    }

    /* FIXME: other key types? */
    key_type = g_tls_jni_string_from_gchar (env, NULL, FALSE, "RSA");

    if (g_strcmp0 (purpose, G_TLS_DATABASE_PURPOSE_AUTHENTICATE_CLIENT) == 0) {
      (*env)->CallVoidMethod (env, manager,
                  default_certificate.x509_trust_manager_check_client_trusted,
                  certificate_chain,
                  key_type);
    } else {
      (*env)->CallVoidMethod (env, manager,
                  default_certificate.x509_trust_manager_check_server_trusted,
                  certificate_chain,
                  key_type);
    }

    g_tls_jni_object_local_unref (env, key_type);

    gtls_flags |= possible_exception_to_cert_flags (cert_android);
    gtls_flags |= certificate_chain_verify_validity (cert_android, certificate_chain);
    gtls_flags |= verify_certificate_identity (cert_android, server_name);

    g_tls_jni_object_local_unref (env, certificate_chain);
  }

  g_debug ("%p verify result 0x%x", cert_android, gtls_flags);

  /* FIXME: add hostname verification and try validating the certpath instead?
   * https://stackoverflow.com/questions/18822516/java-android-validating-the-x509certificate-against-ca-certificateissuer-certi */

out:
  if (default_algorithm)
    g_tls_jni_object_local_unref (env, default_algorithm);
  if (trust_factory)
    g_tls_jni_object_local_unref (env, trust_factory);
  if (managers)
    g_tls_jni_object_local_unref (env, managers);
  if (password)
    g_tls_jni_object_local_unref (env, password);
  if (manager)
    g_tls_jni_object_local_unref (env, manager);
  if (allocated_key_store)
    g_tls_jni_object_local_unref (env, allocated_key_store);

  return gtls_flags;
}

static GTlsCertificateFlags
g_tls_certificate_android_verify (GTlsCertificate *cert,
    GSocketConnectable *identity, GTlsCertificate *trusted_ca)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsCertificateAndroid *cert_android;
  GTlsCertificateFlags gtls_flags = 0;
  jobject key_store;

  cert_android = G_TLS_CERTIFICATE_ANDROID (cert);

  key_store = key_store_from_certificate (trusted_ca);

  gtls_flags |=
      g_tls_certificate_android_verify_with_keystore (cert_android,
              identity, key_store, G_TLS_DATABASE_PURPOSE_AUTHENTICATE_SERVER, NULL);

  if (key_store)
    g_tls_jni_object_local_unref (env, key_store);

  return gtls_flags;
}

static void
g_tls_certificate_android_class_init (GTlsCertificateAndroidClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GTlsCertificateClass *certificate_class = G_TLS_CERTIFICATE_CLASS (klass);

  gobject_class->get_property = g_tls_certificate_android_get_property;
  gobject_class->set_property = g_tls_certificate_android_set_property;
  gobject_class->finalize = g_tls_certificate_android_finalize;

  certificate_class->verify = g_tls_certificate_android_verify;

  g_object_class_override_property (gobject_class, PROP_CERTIFICATE, "certificate");
  g_object_class_override_property (gobject_class, PROP_CERTIFICATE_PEM, "certificate-pem");
  g_object_class_override_property (gobject_class, PROP_PRIVATE_KEY, "private-key");
  g_object_class_override_property (gobject_class, PROP_PRIVATE_KEY_PEM, "private-key-pem");
  g_object_class_override_property (gobject_class, PROP_ISSUER, "issuer");
}

static void
g_tls_certificate_android_initable_iface_init (GInitableIface  *iface)
{
  iface->init = g_tls_certificate_android_initable_init;
}

GTlsCertificate *
g_tls_certificate_android_new (GBytes *bytes, GTlsCertificate *issuer)
{
  GByteArray *arr = g_byte_array_sized_new (g_bytes_get_size (bytes));
  GTlsCertificate *ret;

  g_byte_array_append (arr, g_bytes_get_data (bytes, NULL), g_bytes_get_size (bytes));

  ret = g_object_new (G_TYPE_TLS_CERTIFICATE_ANDROID,
                          "issuer", issuer,
                          "certificate", arr,
                          NULL);
  g_byte_array_unref (arr);

  return ret;
}

GTlsCertificate *
g_tls_certificate_android_new_with_cert (jobject cert, GTlsCertificate *issuer)
{
  GTlsCertificate * certificate;
  GTlsCertificateAndroidPrivate *priv;

  certificate = g_object_new (G_TYPE_TLS_CERTIFICATE_ANDROID,
                          "issuer", issuer,
                          NULL);
  priv = g_tls_certificate_android_get_instance_private (G_TLS_CERTIFICATE_ANDROID (certificate));
  priv->certificate = g_tls_jni_object_ref (g_tls_jni_get_env (), cert);

  return certificate;
}

GBytes *
g_tls_certificate_android_get_cert_bytes (GTlsCertificateAndroid *self)
{
  GByteArray *array;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_ANDROID (self), NULL);

  g_object_get (self, "certificate", &array, NULL);
  return g_byte_array_free_to_bytes (array);
}

GBytes *
g_tls_certificate_android_get_issuer_sequence (GTlsCertificateAndroid *self)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsCertificateAndroidPrivate *priv;
  jobject principal;
  jbyteArray encoded;
  GBytes *ret;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_ANDROID (self), NULL);
  priv = g_tls_certificate_android_get_instance_private (self);
  g_return_val_if_fail (priv->certificate != NULL, NULL);
  g_return_val_if_fail (default_certificate.x509_certificate_get_issuer_x500_principal != NULL, NULL);
  g_return_val_if_fail (default_certificate.x500_principal_class != NULL, NULL);
  g_return_val_if_fail (default_certificate.x500_principal_get_encoded != NULL, NULL);

  if (!g_tls_jni_call_object_method (env, NULL,
            priv->certificate,
            default_certificate.x509_certificate_get_issuer_x500_principal,
            &principal)) {
    g_critical ("getIssuerX500Principal() call failed");
    return NULL;
  }

  if (!g_tls_jni_call_object_method (env, NULL,
            principal,
            default_certificate.x500_principal_get_encoded,
            &encoded)) {
    g_tls_jni_object_local_unref (env, principal);
    g_critical ("X500Principal.getEncoded() call failed");
    return NULL;
  }
  g_tls_jni_object_local_unref (env, principal);

  ret = bytes_from_jni_byte_array (encoded);

  g_tls_jni_object_local_unref (env, encoded);

  return ret;
}

GBytes *
g_tls_certificate_android_get_subject_sequence (GTlsCertificateAndroid *self)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsCertificateAndroidPrivate *priv;
  jobject principal;
  jbyteArray encoded;
  GBytes *ret;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_ANDROID (self), NULL);
  priv = g_tls_certificate_android_get_instance_private (self);
  g_return_val_if_fail (priv->certificate != NULL, NULL);
  g_return_val_if_fail (default_certificate.x509_certificate_get_subject_x500_principal != NULL, NULL);
  g_return_val_if_fail (default_certificate.x500_principal_class != NULL, NULL);
  g_return_val_if_fail (default_certificate.x500_principal_get_encoded != NULL, NULL);

  if (!g_tls_jni_call_object_method (env, NULL,
            priv->certificate,
            default_certificate.x509_certificate_get_subject_x500_principal,
            &principal)) {
    g_critical ("getIssuerX500Principal() call failed");
    return NULL;
  }

  if (!g_tls_jni_call_object_method (env, NULL,
            principal,
            default_certificate.x500_principal_get_encoded,
            &encoded)) {
    g_tls_jni_object_local_unref (env, principal);
    g_critical ("X500Principal.getEncoded() call failed");
    return NULL;
  }
  g_tls_jni_object_local_unref (env, principal);

  ret = bytes_from_jni_byte_array (encoded);

  g_tls_jni_object_local_unref (env, encoded);

  return ret;
}

jobject
g_tls_certificate_android_get_cert (GTlsCertificateAndroid * self)
{
  GTlsCertificateAndroidPrivate *priv;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_ANDROID (self), NULL);

  priv = g_tls_certificate_android_get_instance_private (self);

  return priv->certificate;
}

jobject
g_tls_certificate_android_get_key (GTlsCertificateAndroid * self)
{
  GTlsCertificateAndroidPrivate *priv;

  g_return_val_if_fail (G_IS_TLS_CERTIFICATE_ANDROID (self), NULL);

  priv = g_tls_certificate_android_get_instance_private (self);

  return priv->private_key;
}
