/*
 * gtlsutils-android.c
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include "gtlsutils-android.h"

#include "gtlsjniutils.h"

gchar *
g_tls_android_socket_connectable_to_string (GSocketConnectable *identity)
{
  gchar *server_name;

  if (G_IS_NETWORK_ADDRESS (identity)) {
    server_name = g_strdup (g_network_address_get_hostname (G_NETWORK_ADDRESS (identity)));
  } else if (G_IS_NETWORK_SERVICE (identity)) {
    server_name = g_strdup (g_network_service_get_domain (G_NETWORK_SERVICE (identity)));
  } else if (G_IS_INET_SOCKET_ADDRESS (identity)) {
    GInetAddress * addr = g_inet_socket_address_get_address (G_INET_SOCKET_ADDRESS (identity));
    server_name = g_inet_address_to_string (addr);
  } else {
    g_warn_if_reached ();
    server_name = NULL;
  }

  return server_name;
}

gint
g_tls_android_socket_connectable_get_port (GSocketConnectable *identity)
{
  gint port;

  if (G_IS_NETWORK_ADDRESS (identity)) {
    port = g_network_address_get_port (G_NETWORK_ADDRESS (identity));
/* FIXME
  } else if (G_IS_NETWORK_SERVICE (identity)) {
    server_name = g_strdup (g_network_service_get_domain (G_NETWORK_SERVICE (identity)));*/
  } else if (G_IS_INET_SOCKET_ADDRESS (identity)) {
    port = g_inet_socket_address_get_port (G_INET_SOCKET_ADDRESS (identity));
  } else {
    g_warn_if_reached ();
    port = -1;
  }

  return port;
}

jbyteArray
jni_byte_array_from_data (guint8 * data, gsize size)
{
  JNIEnv *env = g_tls_jni_get_env ();
  jbyteArray array;
  jbyte *array_data;
  int i;

  array = (*env)->NewByteArray(env, size);
  if (!array)
    return NULL;

  array_data = (*env)->GetByteArrayElements(env, array, 0);
  for (i = 0; i < size; i++) {
    array_data[i] = data[i];
  }
  (*env)->SetByteArrayRegion(env, array, 0, size, array_data);

  return array;
}

GBytes *
bytes_from_jni_byte_array (jbyteArray array)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GByteArray *mut;
  jbyte *array_data;
  gsize size;

  size = (*env)->GetArrayLength (env, array);

  mut = g_byte_array_sized_new (size);

  array_data = (*env)->GetByteArrayElements(env, array, 0);
  g_byte_array_append (mut, (guint8 *) array_data, size);

  return g_byte_array_free_to_bytes (mut);
}

gboolean
pem_to_der (const guint8 * data, gsize length, jbyteArray * out)
{
  jbyteArray ret = NULL;
  guint8 *der = NULL;
  gsize decoded_length;
  gint state = 0;
  guint save = 0;

  der = g_new (guint8, length);

  decoded_length = g_base64_decode_step ((const gchar *) data, length, der, &state, &save);
  if (!decoded_length) {
    g_free (der);
    return FALSE;
  }

  ret = jni_byte_array_from_data (der, decoded_length);
  g_free (der);

  if (!ret) {
    return FALSE;
  }

  *out = ret;
  return TRUE;
}

guint8 *
der_to_pem (GByteArray * der, gsize * length)
{
  guint8 *ret = NULL;

  if (!der)
    return NULL;

  ret = (guint8 *) g_base64_encode (der->data, der->len);
  if (!ret)
    return NULL;

  if (length)
    *length = strlen((gchar *) ret);
  return ret;
}
