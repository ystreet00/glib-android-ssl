/*
 * gtlsengine-android.h
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __G_TLS_SESSION_ANDROID_H__
#define __G_TLS_SESSION_ANDROID_H__

#include <gio/gio.h>
#include <jni.h>

G_BEGIN_DECLS

typedef struct _GTlsEngineClass
{
  jclass klass;
  jmethodID set_need_client_auth;
  jmethodID set_want_client_auth;
  jmethodID set_use_client_mode;
  jmethodID begin_handshake;
  jmethodID get_delegated_task;
  jmethodID get_handshake_status;
  jmethodID get_session;
  jmethodID get_status;
  jmethodID wrap;
  jmethodID unwrap;
  jmethodID is_inbound_done;
  jmethodID is_outbound_done;
  jmethodID close_inbound;
  jmethodID close_outbound;

  jclass result_klass;
  jmethodID result_get_status;
  jmethodID result_get_handshake_status;
  jmethodID result_bytes_consumed;
  jmethodID result_bytes_produced;

  jclass result_status_klass;
  jfieldID result_status_field_buffer_overflow;
  jfieldID result_status_field_buffer_underflow;
  jfieldID result_status_field_closed;
  jfieldID result_status_field_ok;
  jobject result_status_buffer_overflow;
  jobject result_status_buffer_underflow;
  jobject result_status_closed;
  jobject result_status_ok;

  jclass result_handshake_status_klass;
  jfieldID result_handshake_status_field_finished;
  jfieldID result_handshake_status_field_need_task;
  jfieldID result_handshake_status_field_need_unwrap;
  jfieldID result_handshake_status_field_need_wrap;
  jfieldID result_handshake_status_field_not_handshaking;
  jobject result_handshake_status_finished;
  jobject result_handshake_status_need_task;
  jobject result_handshake_status_need_unwrap;
  jobject result_handshake_status_need_wrap;
  jobject result_handshake_status_not_handshaking;

  jclass ssl_exception_klass;
} GTlsEngineClass;

gboolean            engine_initialize     (GError ** error);
GTlsEngineClass *   engine_get            (void);

typedef enum
{
  ENGINE_HANDSHAKE_FINISHED = 1,
  ENGINE_HANDSHAKE_NEED_TASK,
  ENGINE_HANDSHAKE_NEED_UNWRAP,
  ENGINE_HANDSHAKE_NEED_WRAP,
  ENGINE_HANDSHAKE_NOT_HANDSHAKING,
  ENGINE_HANDSHAKE_ERROR,
} EngineHandshakeStatus;

typedef enum
{
  ENGINE_OK,
  ENGINE_CLOSED,
  ENGINE_BUFFER_UNDERFLOW,
  ENGINE_BUFFER_OVERFLOW,
  ENGINE_ERROR,
} EngineStatus;

void                    engine_begin_handshake              (jobject engine);
void                    engine_set_need_client_auth         (jobject engine, gboolean value);
void                    engine_set_want_client_auth         (jobject engine, gboolean value);
void                    engine_set_use_client_mode          (jobject engine, gboolean value);
EngineHandshakeStatus   engine_get_handshake_status         (jobject engine);
EngineStatus            engine_get_status                   (jobject engine);
jobject                 engine_get_session                  (jobject engine);
jobject                 engine_wrap                         (jobject engine, jobject src, jobject dst);
jobject                 engine_unwrap                       (jobject engine, jobject src, jobject dst);

jobject                 engine_get_delegated_task           (jobject engine);
void                    engine_run_delegated_task           (jobject engine);

EngineStatus            engine_result_get_status            (jobject result);
EngineHandshakeStatus   engine_result_get_handshake_status  (jobject result);
int                     engine_result_bytes_produced    (jobject result);
int                     engine_result_bytes_consumed    (jobject result);

void                    engine_close_outbound               (jobject engine);
gboolean                engine_close_inbound                (jobject engine);
gboolean                engine_is_inbound_done              (jobject engine);
gboolean                engine_is_outbound_done             (jobject engine);


G_END_DECLS

#endif /* __G_TLS_SESSION_ANDROID_H___ */
