/*
 * gtlscontext-android.h
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __G_TLS_ENUMERATION_ANDROID_H__
#define __G_TLS_ENUMERATION_ANDROID_H__

#include <gio/gio.h>
#include <jni.h>

G_BEGIN_DECLS

typedef struct _GTlsEnumerationClass
{
  jclass klass;
  jmethodID has_more_elements;
  jmethodID next_element;
} GTlsEnumerationClass;

gboolean                enumeration_initialize                  (GError ** error);
GTlsEnumerationClass *  enumeration_get                         (void);

gboolean                enumeration_has_more_elements           (jobject enumeration);
jobject                 enumeration_next_element                (jobject enumeration);

G_END_DECLS

#endif /* __G_TLS_ENUMERATION_ANDROID_H___ */
