/*
 * gtlscustomtrustmanager-android.h
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __G_TLS_CUSTOM_TRUST_MANAGER_ANDROID_H__
#define __G_TLS_CUSTOM_TRUST_MANAGER_ANDROID_H__

#include <gio/gio.h>
#include <jni.h>

G_BEGIN_DECLS

typedef struct _GTlsCustomTrustManagerClass GTlsCustomTrustManagerClass;
typedef struct _GTlsCustomTrustManager GTlsCustomTrustManager;

typedef GTlsCertificateFlags (*GTlsCustomTrustManagerCheckTrustedFunc)
                    (GTlsCustomTrustManager * manager, jobjectArray certs, jstring auth_type, gpointer user_data);
typedef jobjectArray (*GTlsCustomTrustManagerGetAcceptedIssuersFunc)
                    (GTlsCustomTrustManager * manager, gpointer user_data);

struct _GTlsCustomTrustManagerClass
{
  jclass klass;

  jfieldID native_manager_field_id;

  jmethodID constructor;
};

struct _GTlsCustomTrustManager
{
  jobject manager;

  GTlsCustomTrustManagerCheckTrustedFunc check_server_trusted;
  GTlsCustomTrustManagerCheckTrustedFunc check_client_trusted;
  GTlsCustomTrustManagerGetAcceptedIssuersFunc get_accepted_issuers;

  gpointer user_data;
  GDestroyNotify notify;
};

gboolean                        custom_trust_manager_initialize     (GError ** error);
GTlsCustomTrustManagerClass *   custom_trust_manager_get            (void);

GTlsCustomTrustManager *        custom_trust_manager_new (GTlsCustomTrustManagerCheckTrustedFunc server, GTlsCustomTrustManagerCheckTrustedFunc client, GTlsCustomTrustManagerGetAcceptedIssuersFunc get_issuers, gpointer user_data, GDestroyNotify notify);

G_END_DECLS

#endif /* __G_TLS_CUSTOM_TRUST_MANAGER_ANDROID_H___ */
