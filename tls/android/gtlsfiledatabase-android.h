/*
 * gtlsfiledatabase-android.h
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __G_TLS_FILE_DATABASE_ANDROID_H__
#define __G_TLS_FILE_DATABASE_ANDROID_H__

#include <gio/gio.h>

#include "gtlsdatabase-android.h"

G_BEGIN_DECLS

#define G_TYPE_TLS_FILE_DATABASE_ANDROID            (g_tls_file_database_android_get_type ())
#define G_TLS_FILE_DATABASE_ANDROID(inst)           (G_TYPE_CHECK_INSTANCE_CAST ((inst), G_TYPE_TLS_FILE_DATABASE_ANDROID, GTlsFileDatabaseAndroid))
#define G_TLS_FILE_DATABASE_ANDROID_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class), G_TYPE_TLS_FILE_DATABASE_ANDROID, GTlsFileDatabaseAndroidClass))
#define G_IS_TLS_FILE_DATABASE_ANDROID(inst)        (G_TYPE_CHECK_INSTANCE_TYPE ((inst), G_TYPE_TLS_FILE_DATABASE_ANDROID))
#define G_IS_TLS_FILE_DATABASE_ANDROID_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class), G_TYPE_TLS_FILE_DATABASE_ANDROID))
#define G_TLS_FILE_DATABASE_ANDROID_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS ((inst), G_TYPE_TLS_FILE_DATABASE_ANDROID, GTlsFileDatabaseAndroidClass))

typedef struct _GTlsFileDatabaseAndroidClass GTlsFileDatabaseAndroidClass;
typedef struct _GTlsFileDatabaseAndroid      GTlsFileDatabaseAndroid;

struct _GTlsFileDatabaseAndroidClass
{
  GTlsDatabaseAndroidClass parent_class;
};

struct _GTlsFileDatabaseAndroid
{
  GTlsDatabaseAndroid parent_instance;
};

GType                        g_tls_file_database_android_get_type              (void) G_GNUC_CONST;

G_END_DECLS

#endif /* __G_TLS_FILE_DATABASE_ANDROID_H___ */
