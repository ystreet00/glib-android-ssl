/*
 * gtlsbuffer-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlsbuffer-android.h"

#include "gtlsjniutils.h"

static GTlsBufferClass default_buffer;

gboolean
buffer_initialize (GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *my_error = NULL;

  if (!(default_buffer.klass =
        g_tls_jni_get_class (env,
                &my_error,
                "java/nio/Buffer"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_buffer.get_limit =
            g_tls_jni_get_method_id (env,
                    &my_error,
                    default_buffer.klass,
                    "limit",
                    "()I"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_buffer.get_position =
            g_tls_jni_get_method_id (env,
                    &my_error,
                    default_buffer.klass,
                    "position",
                    "()I"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_buffer.set_limit =
            g_tls_jni_get_method_id (env,
                    &my_error,
                    default_buffer.klass,
                    "limit",
                    "(I)Ljava/nio/Buffer;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_buffer.set_position =
            g_tls_jni_get_method_id (env,
                    &my_error,
                    default_buffer.klass,
                    "position",
                    "(I)Ljava/nio/Buffer;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_buffer.byte_buffer_klass =
        g_tls_jni_get_class (env,
                &my_error,
                "java/nio/ByteBuffer"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  if (!(default_buffer.compact =
            g_tls_jni_get_method_id (env,
                    &my_error,
                    default_buffer.byte_buffer_klass,
                    "compact",
                    "()Ljava/nio/ByteBuffer;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    return FALSE;
  }

  return TRUE;
}

GTlsBufferClass *
buffer_get (void)
{
  return &default_buffer;
}

GTlsJniBuffer*
g_tls_jni_buffer_new_wrapped (guint8 * data, gsize size, GDestroyNotify free_func)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsJniBuffer *ret;
  jobject buf;

  buf = (*env)->NewDirectByteBuffer (env, (void*) data, size);
  if (!buf)
    return NULL;

  ret = g_new0 (GTlsJniBuffer, 1);
  ret->data = data;
  ret->buffer = g_tls_jni_object_make_global (env, buf);
  ret->size = size;
  ret->free_func = free_func;

  return ret;
}

GTlsJniBuffer *
g_tls_jni_buffer_new (gsize size)
{
  guint8 *data = g_new0 (guint8, size);

  return g_tls_jni_buffer_new_wrapped (data, size, g_free);
}

void
g_tls_jni_buffer_free (GTlsJniBuffer * buffer)
{
  JNIEnv *env = g_tls_jni_get_env ();

  if (!buffer)
    return;

  if (buffer->free_func)
    buffer->free_func (buffer->data);

  g_tls_jni_object_unref (env, buffer->buffer);
  memset (buffer, 0, sizeof (*buffer));
  g_free (buffer);
}

void
g_tls_jni_buffer_set_limit (GTlsJniBuffer * buffer, gsize limit)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject result;

//  g_debug ("%p setting limit to %" G_GSIZE_FORMAT, buffer, limit);

  if (!g_tls_jni_call_object_method (env, &error,
            buffer->buffer,
            default_buffer.set_limit,
            &result,
            limit)) {
    g_critical ("Failed to set limit on buffer: %s", error->message);
    return;
  }

  g_tls_jni_object_local_unref (env, result);
}

gsize
g_tls_jni_buffer_get_limit (GTlsJniBuffer * buffer)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  gint limit = 0;

  if (!g_tls_jni_call_int_method (env, &error,
            buffer->buffer,
            default_buffer.get_limit,
            &limit)) {
    g_critical ("Failed to get limit of buffer: %s", error->message);
    return 0;
  }

  return (gsize) limit;
}

void
g_tls_jni_buffer_set_position (GTlsJniBuffer * buffer, gsize position)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject result;

//  g_debug ("%p setting position to %" G_GSIZE_FORMAT, buffer, position);

  if (!g_tls_jni_call_object_method (env, &error,
            buffer->buffer,
            default_buffer.set_position,
            &result,
            position)) {
    g_critical ("Failed to set position on buffer: %s", error->message);
    return;
  }

  g_tls_jni_object_local_unref (env, result);
}

gsize
g_tls_jni_buffer_get_position (GTlsJniBuffer * buffer)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  gint position = 0;

  if (!g_tls_jni_call_int_method (env, &error,
            buffer->buffer,
            default_buffer.get_position,
            &position)) {
    g_critical ("Failed to get position of buffer: %s", error->message);
    return 0;
  }

  return (gsize) position;
}

void
g_tls_jni_buffer_compact (GTlsJniBuffer * buffer)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject result;

  if (!g_tls_jni_call_object_method (env, &error,
            buffer->buffer,
            default_buffer.compact,
            &result)) {
    g_critical ("Failed to get compact buffer: %s", error->message);
    return;
  }

  g_tls_jni_object_local_unref (env, result);
}

guint8 *
g_tls_jni_buffer_get_data (GTlsJniBuffer * buffer, gsize * size)
{
  *size = buffer->size;
  return buffer->data;
}
