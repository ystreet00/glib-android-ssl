/*
 * gtlsclientconnection-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "glib.h"

#include <string.h>

#include "gtlsconnection-base.h"
#include "gtlsclientconnection-android.h"
#include "gtlsbackend-android.h"
#include "gtlscertificate-android.h"
#include "gtlsutils-android.h"

#include "gtlscontext-android.h"
#include "gtlsengine-android.h"
#include "gtlsjniutils.h"

typedef struct _GTlsClientConnectionAndroidPrivate
{
  GSocketConnectable *server_identity;
  GTlsCertificateFlags validation_flags;
  gboolean use_ssl3;

  GList *accepted_cas;
} GTlsClientConnectionAndroidPrivate;

enum
{
  PROP_0,
  PROP_SERVER_IDENTITY,
  PROP_VALIDATION_FLAGS,
  PROP_USE_SSL3,
  PROP_ACCEPTED_CAS
};

static void g_tls_client_connection_android_initable_interface_init (GInitableIface  *iface);

static void g_tls_client_connection_android_client_connection_interface_init (GTlsClientConnectionInterface *iface);

static GInitableIface *g_tls_client_connection_android_parent_initable_iface;

G_DEFINE_TYPE_WITH_CODE (GTlsClientConnectionAndroid, g_tls_client_connection_android, G_TYPE_TLS_CONNECTION_ANDROID,
                         G_ADD_PRIVATE (GTlsClientConnectionAndroid)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                g_tls_client_connection_android_initable_interface_init)
                         G_IMPLEMENT_INTERFACE (G_TYPE_TLS_CLIENT_CONNECTION,
                                                g_tls_client_connection_android_client_connection_interface_init))

static void
g_tls_client_connection_android_finalize (GObject *object)
{
  GTlsClientConnectionAndroid *android = G_TLS_CLIENT_CONNECTION_ANDROID (object);
  GTlsClientConnectionAndroidPrivate *priv;

  priv = g_tls_client_connection_android_get_instance_private (android);

  g_list_free_full (priv->accepted_cas, (GDestroyNotify) g_byte_array_unref);
  priv->accepted_cas = NULL;

  G_OBJECT_CLASS (g_tls_client_connection_android_parent_class)->finalize (object);
}

static void
g_tls_client_connection_android_get_property (GObject  *object,
    guint prop_id, GValue *value, GParamSpec *pspec)
{
  GTlsClientConnectionAndroid *android = G_TLS_CLIENT_CONNECTION_ANDROID (object);
  GTlsClientConnectionAndroidPrivate *priv;

  priv = g_tls_client_connection_android_get_instance_private (android);

  switch (prop_id) {
    case PROP_SERVER_IDENTITY:
      g_value_set_object (value, priv->server_identity);
      break;
    case PROP_VALIDATION_FLAGS:
      g_value_set_flags (value, priv->validation_flags);
      break;
    case PROP_USE_SSL3:
      g_value_set_boolean (value, priv->use_ssl3);
      break;
    case PROP_ACCEPTED_CAS:
      g_value_set_pointer (value, g_list_copy_deep (priv->accepted_cas, (GCopyFunc) g_byte_array_ref, NULL));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
g_tls_client_connection_android_set_property (GObject *object,
    guint prop_id, const GValue *value, GParamSpec   *pspec)
{
  GTlsClientConnectionAndroid *android = G_TLS_CLIENT_CONNECTION_ANDROID (object);
  GTlsClientConnectionAndroidPrivate *priv;

  priv = g_tls_client_connection_android_get_instance_private (android);

  switch (prop_id) {
    case PROP_SERVER_IDENTITY:
      if (priv->server_identity)
        g_object_unref (priv->server_identity);
      priv->server_identity = g_value_dup_object (value);
      break;
    case PROP_VALIDATION_FLAGS:
      priv->validation_flags = g_value_get_flags (value);
      break;
    case PROP_USE_SSL3:
      priv->use_ssl3 = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static GTlsConnectionBaseStatus
g_tls_client_connection_android_handshake (GTlsConnectionBase *tls,
    GCancellable *cancellable, GError **error)
{
  GTlsConnectionAndroid *android = G_TLS_CONNECTION_ANDROID (tls);
  JNIEnv *env = g_tls_jni_get_env ();

  g_assert (android->ssl_context != NULL);

  if (!android->ssl_engine) {
    GSocketConnectable *peer_identity = NULL;
    gchar *peer_name = NULL;
    gint peer_port = -1;
    jobject engine;

    g_tls_connection_android_configure_context_cert_and_key (android);

    peer_identity = g_tls_client_connection_get_server_identity (G_TLS_CLIENT_CONNECTION (android));
    peer_name = g_tls_android_socket_connectable_to_string (peer_identity);
    peer_port = g_tls_android_socket_connectable_get_port (peer_identity);

    if (peer_name != NULL && peer_port > 0) {
      jobject j_name = g_tls_jni_string_from_gchar (env, error, FALSE, peer_name);

      engine = context_create_ssl_engine_with_peer (android->ssl_context, j_name, peer_port);

      g_tls_jni_object_local_unref (env, j_name);
    } else {
      engine = context_create_ssl_engine (android->ssl_context);
    }

    engine_set_use_client_mode (engine, TRUE);

    if (!(android->ssl_engine = g_tls_jni_object_make_global (env, engine))) {
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Failed to make global engine reference");
      return G_TLS_CONNECTION_BASE_ERROR;
    }
  }

  return G_TLS_CONNECTION_BASE_CLASS (g_tls_client_connection_android_parent_class)->handshake (tls, cancellable, error);
}

static void
g_tls_client_connection_android_retrieve_cas (GTlsConnectionAndroid * android)
{
  GTlsClientConnectionAndroid *self = G_TLS_CLIENT_CONNECTION_ANDROID (android);
  GTlsClientConnectionAndroidPrivate *priv;
/*  GQueue accepted_cas = G_QUEUE_INIT;
  int i;*/

  priv = g_tls_client_connection_android_get_instance_private (self);

  g_list_free_full (priv->accepted_cas, (GDestroyNotify) g_byte_array_unref);
  priv->accepted_cas = NULL;

  /* FIXME: */

  g_object_notify (G_OBJECT (self), "accepted-cas");
}

static void
g_tls_client_connection_android_class_init (GTlsClientConnectionAndroidClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GTlsConnectionBaseClass *base_class = G_TLS_CONNECTION_BASE_CLASS (klass);
  GTlsConnectionAndroidClass *android_class = G_TLS_CONNECTION_ANDROID_CLASS (klass);

  gobject_class->finalize = g_tls_client_connection_android_finalize;
  gobject_class->get_property = g_tls_client_connection_android_get_property;
  gobject_class->set_property = g_tls_client_connection_android_set_property;

  base_class->handshake = g_tls_client_connection_android_handshake;

  android_class->retrieve_cas = g_tls_client_connection_android_retrieve_cas;

  g_object_class_override_property (gobject_class, PROP_SERVER_IDENTITY, "server-identity");
  g_object_class_override_property (gobject_class, PROP_VALIDATION_FLAGS, "validation-flags");
  g_object_class_override_property (gobject_class, PROP_USE_SSL3, "use-ssl3");
  g_object_class_override_property (gobject_class, PROP_ACCEPTED_CAS, "accepted-cas");
}

static void
g_tls_client_connection_android_init (GTlsClientConnectionAndroid *android)
{
  GTlsConnectionAndroid *conn = G_TLS_CONNECTION_ANDROID (android);

  conn->authentication_mode = G_TLS_AUTHENTICATION_REQUIRED;
}

static void
g_tls_client_connection_android_copy_session_state (GTlsClientConnection *conn,
                                                    GTlsClientConnection *source)
{
  /* unknown usage */
  g_assert_not_reached ();
}

static void
g_tls_client_connection_android_client_connection_interface_init (GTlsClientConnectionInterface *iface)
{
  iface->copy_session_state = g_tls_client_connection_android_copy_session_state;
}

static gboolean
g_tls_client_connection_android_initable_init (GInitable       *initable,
                                               GCancellable    *cancellable,
                                               GError         **error)
{


  if (!g_tls_client_connection_android_parent_initable_iface->
      init (initable, cancellable, error))
    return FALSE;

  return TRUE;
}

static void
g_tls_client_connection_android_initable_interface_init (GInitableIface  *iface)
{
  g_tls_client_connection_android_parent_initable_iface = g_type_interface_peek_parent (iface);

  iface->init = g_tls_client_connection_android_initable_init;
}
