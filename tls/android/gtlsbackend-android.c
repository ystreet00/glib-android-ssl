/*
 * gtlsbackend-android.c
 *
 * Copyright (C) 2018 Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"

#include "gtlsbackend-android.h"
#include "gtlsdatabase-android.h"
#include "gtlssystemdatabase-android.h"
#include "gtlscertificate-android.h"
#include "gtlsfiledatabase-android.h"
#include "gtlsclientconnection-android.h"
#include "gtlsserverconnection-android.h"

#include "gtlsbuffer-android.h"
#include "gtlscontext-android.h"
#include "gtlscustomtrustmanager-android.h"
#include "gtlsengine-android.h"
#include "gtlsenumeration-android.h"
#include "gtlskeymanagerfactory-android.h"
#include "gtlskeystore-android.h"
#include "gtlsthrowable-android.h"
#include "gtlstrustmanagerfactory-android.h"
#include "gtlsjniutils.h"

struct _GTlsBackendAndroid
{
  GObject parent;
};

static void g_tls_backend_android_interface_init (GTlsBackendInterface *iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED (GTlsBackendAndroid, g_tls_backend_android, G_TYPE_OBJECT, 0,
                                G_IMPLEMENT_INTERFACE_DYNAMIC (G_TYPE_TLS_BACKEND,
                                                               g_tls_backend_android_interface_init))

static void
g_tls_backend_android_init (GTlsBackendAndroid *backend)
{
}

static void
g_tls_backend_android_class_init (GTlsBackendAndroidClass *klass)
{
}

static void
g_tls_backend_android_class_finalize (GTlsBackendAndroidClass *backend_class)
{
}

static gboolean
g_tls_backend_android_supports_tls (GTlsBackend *backend)
{
  return TRUE;
}

static gboolean
g_tls_backend_android_supports_dtls (GTlsBackend *backend)
{
  return FALSE;
}

static GTlsDatabase *
g_tls_backend_android_get_default_database (GTlsBackend *backend)
{
  G_LOCK_DEFINE_STATIC (default_database);
  static GTlsDatabase *database = NULL;

  G_LOCK (default_database);
  if (!database)
    database = g_tls_system_database_android_new ();
  G_UNLOCK (default_database);

  return g_object_ref (database);
}

static void
g_tls_backend_android_interface_init (GTlsBackendInterface *iface)
{
  iface->supports_tls = g_tls_backend_android_supports_tls;
  iface->supports_dtls = g_tls_backend_android_supports_dtls;
  iface->get_default_database = g_tls_backend_android_get_default_database;
  iface->get_file_database_type = g_tls_file_database_android_get_type;
  iface->get_certificate_type = g_tls_certificate_android_get_type;
  iface->get_client_connection_type = g_tls_client_connection_android_get_type;
  iface->get_server_connection_type = g_tls_server_connection_android_get_type;
}

static gboolean
load_java_classes (void)
{
  GError *error = NULL;

  if (!g_tls_jni_initialize ()) {
    g_critical ("Failed to initialize JNI helpers");
    return FALSE;
  }

  if (!custom_trust_manager_initialize (&error)) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  if (!throwable_initialize (&error)) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  if (!buffer_initialize (&error)) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  if (!certificate_initialize (&error)) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  if (!context_initialize (&error)) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  if (!engine_initialize (&error)) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  if (!enumeration_initialize (&error)) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  if (!key_manager_factory_initialize (&error)) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  if (!keystore_initialize (&error)) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  if (!trust_manager_factory_initialize (&error)) {
    g_critical ("%s", error->message);
    return FALSE;
  }

  return TRUE;
}

void
g_tls_backend_android_register (GIOModule *module)
{
  GTypePlugin *plugin;

  if (!load_java_classes ())
    return;

  g_tls_backend_android_register_type (G_TYPE_MODULE (module));
  if (module == NULL) {
    g_io_extension_point_register (G_TLS_BACKEND_EXTENSION_POINT_NAME);
  }
  g_io_extension_point_implement (G_TLS_BACKEND_EXTENSION_POINT_NAME,
                                  g_tls_backend_android_get_type(),
                                  "android",
                                  100);

  plugin = g_type_get_plugin (G_TYPE_TLS_BACKEND_ANDROID);
  if (plugin)
    g_type_plugin_use (plugin);
}
