/*
 * gtlssystemdatabase-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This system is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This system is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlssystemdatabase-android.h"

#include <gio/gio.h>

#include "gtlskeystore-android.h"
#include "gtlsjniutils.h"

#define SYSTEM_CERT_HANDLE_PREFIX "system://"

typedef struct _GTlsSystemDatabaseAndroidPrivate
{
  jclass system_class;
  jmethodID system_get_property;

  jclass file_input_stream_class;
  jmethodID file_input_stream_constructor;
} GTlsSystemDatabaseAndroidPrivate;

enum
{
  PROP_0,
};

static void g_tls_system_database_android_initable_interface_init (GInitableIface *iface);

G_DEFINE_TYPE_WITH_CODE (GTlsSystemDatabaseAndroid, g_tls_system_database_android, G_TYPE_TLS_DATABASE_ANDROID,
                         G_ADD_PRIVATE (GTlsSystemDatabaseAndroid)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                g_tls_system_database_android_initable_interface_init))

static gchar *
system_get_property (GTlsSystemDatabaseAndroid * android, const gchar * prop_name, GError ** error)
{
  GTlsSystemDatabaseAndroidPrivate *priv;
  JNIEnv *env = g_tls_jni_get_env ();
  jobject j_prop_name = NULL, prop_val = NULL;
  gchar *ret = NULL;

  priv = g_tls_system_database_android_get_instance_private (android);

  g_assert (priv->system_class);
  g_assert (priv->system_get_property);

  if (!(j_prop_name = g_tls_jni_string_from_gchar (env, error, FALSE, prop_name))) {
    goto out;
  }

  if (!g_tls_jni_call_static_object_method (env,
              error,
              priv->system_class,
              priv->system_get_property,
              &prop_val,
              j_prop_name)) {
    g_critical ("Failed to retreive \'%s\' property", prop_name);
    goto out;
  }

  if (!prop_val) {
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "No \'%s\' value", prop_name);
    goto out;
  }

  if (!(ret = g_tls_jni_string_to_gchar (env, prop_val, TRUE))) {
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Failed to convert \'%s\' value to c string", prop_name);
    goto out;
  }

out:
  if (j_prop_name)
    g_tls_jni_object_local_unref (env, j_prop_name);
  return ret;
}

static jobject
_load_keystore_from_file (GTlsSystemDatabaseAndroid *system_database,
    gchar * key_path, gchar * key_type, gchar * key_password, GError ** error)
{
  GTlsSystemDatabaseAndroidPrivate *priv;
  JNIEnv *env = g_tls_jni_get_env ();
  jobject filename = NULL, stream = NULL, type = NULL;
  jcharArray password = NULL;
  jobject keystore = NULL;
  gchar *alloc_pw = NULL, *alloc_path = NULL;
  gchar *java_home;

  priv = g_tls_system_database_android_get_instance_private (system_database);

  if (!(java_home = system_get_property (system_database, "java.home", error)))
    return FALSE;

  if (!key_password)
    alloc_pw = key_password = g_strdup ("changeit");

  if (!key_path) {
    GFile *f;

    alloc_path = key_path = g_build_filename (java_home, "lib", "security", "jssecacerts", NULL);
    g_debug ("looking for certs at: %s", key_path);
    f = g_file_new_for_path (key_path);

    if (!g_file_query_exists (f, NULL)) {
      g_free (key_path);
      g_object_unref (f);

      alloc_path = key_path = g_build_filename (java_home, "lib", "security", "cacerts", NULL);
      g_debug ("looking for certs at: %s", key_path);
      f = g_file_new_for_path (key_path);
      if (!g_file_query_exists (f, NULL)) {
        g_object_unref (f);
        g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
            "Cannot find a default cert database");
        goto error;
      }
    }

    g_object_unref (f);
  }

  if (!key_type) {
    type = keystore_get_default_type ();
  } else if (!(type = g_tls_jni_string_from_gchar (env, error, FALSE, key_type))) {
    goto error;
  }

  if (!(filename = g_tls_jni_string_from_gchar (env, error, FALSE, key_path))) {
    goto error;
  }

  if (!(stream = g_tls_jni_new_object (env, error, FALSE,
                            priv->file_input_stream_class,
                            priv->file_input_stream_constructor,
                            filename))) {
    goto error;
  }

  if (!stream) {
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find cert database");
    goto error;
  }

  if (key_password) {
    gsize pw_len = strlen (key_password);
    jchar *pw = g_new0 (jchar, pw_len);
    gsize i;

    /* XXX: this is almost definitely wrong for non-ASCII values */
    for (i = 0; i < pw_len; i++)
      pw[i] = key_password[i];

    password = (*env)->NewCharArray (env, pw_len);
    (*env)->SetCharArrayRegion (env, password, 0, pw_len, pw);

    g_free (pw);
  }

  keystore = keystore_get_instance (type);
  if (!keystore) {
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not create %s keystore from %s", key_type, key_path);
    goto error;
  }

  keystore_load_stream (keystore, stream, password);

out:
  if (filename)
    g_tls_jni_object_local_unref (env, filename);
  if (type)
    g_tls_jni_object_local_unref (env, type);
  if (password)
    g_tls_jni_object_local_unref (env, password);
  if (stream)
    g_tls_jni_object_local_unref (env, stream);

  g_free (java_home);
  g_free (alloc_pw);
  g_free (alloc_path);

  return keystore;

error:
  if (keystore)
    g_tls_jni_object_local_unref (env, keystore);
  keystore = NULL;
  goto out;
}

static gboolean
_load_system_keystore (GTlsSystemDatabaseAndroid *system_database, GError ** error)
{
  GTlsDatabaseAndroid *android_database = G_TLS_DATABASE_ANDROID (system_database);
  JNIEnv *env = g_tls_jni_get_env ();
  jclass android_os_version_class;
  gchar *java_home = NULL;
  jobject keystore = NULL;
  GError *my_error = NULL;
  gboolean ret;

  if (!(java_home = system_get_property (system_database, "java.home", error)))
    return FALSE;

  if (!(android_os_version_class =
          g_tls_jni_get_class (env,
                                &my_error,
                                "android/os/Build$VERSION"))) {
    /* no android specifics, try the property definitions */
    gchar *path = system_get_property (system_database, "javax.net.ssl.trustStore", NULL);
    gchar *type = system_get_property (system_database, "javax.net.ssl.trustStoreType", NULL);
    gchar *password = system_get_property (system_database, "javax.net.ssl.trustStorePassword", NULL);

    g_debug ("Could not find class \'android/os/Build$VERSION\'. "
        "Assuming we\'re not running on android: %s", my_error->message);
    g_clear_error (&my_error);

    keystore = _load_keystore_from_file (system_database, path, type, password, error);

    g_free (type);
    g_free (path);
    g_free (password);
  } else {
    jfieldID android_field_sdk_int;
    gint android_sdk_int;

    if (!(android_field_sdk_int =
            g_tls_jni_get_static_field_id (env,
                                        error,
                                        android_os_version_class,
                                        "SDK_INT",
                                        "I"))) {
      g_critical ("Could not retrieve field \'SDK_INT\' "
          "from \'android/os/Build$VERSION\'");
      ret = FALSE;
      goto out;
    }

    if (!g_tls_jni_get_static_int_field (env,
                                        error,
                                        android_os_version_class,
                                        android_field_sdk_int,
                                        &android_sdk_int)) {
      g_critical ("Could not retrieve value of \'SDK_INT\' "
          "from \'android/os/Build$VERSION\'");
      ret = FALSE;
      goto out;
    }

    if (android_sdk_int >= 14) {
      jobject keystore_name;

      if (!(keystore_name = g_tls_jni_string_from_gchar (env, error, FALSE, "AndroidCAStore"))) {
        ret = FALSE;
        goto out;
      }

      keystore = keystore_get_instance (keystore_name);
      g_tls_jni_object_local_unref (env, keystore_name);

      if (!keystore) {
        g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
            "Could not create android specific keystore");
        ret = FALSE;
        goto out;
      }

      keystore_load (keystore, NULL);
    } else {
      gchar *type = g_strdup ("BKS");
      gchar *path = g_build_filename (java_home, "etc", "security", "cacerts.bks", NULL);

      keystore = _load_keystore_from_file (system_database, path, type, NULL, error);

      g_free (type);
      g_free (path);
    }
  }

  if (!keystore)
    goto out;

  ret = g_tls_database_android_load_keystore (android_database, keystore,
      SYSTEM_CERT_HANDLE_PREFIX, error);

out:
  if (keystore)
    g_tls_jni_object_local_unref (env, keystore);
  if (android_os_version_class)
    g_tls_jni_object_unref (env, android_os_version_class);
  g_free (java_home);

  return ret;
}

static void
g_tls_system_database_android_init (GTlsSystemDatabaseAndroid *system_database)
{
  GTlsSystemDatabaseAndroidPrivate *priv;
  JNIEnv *env = g_tls_jni_get_env ();

  g_debug ("system init");

  priv = g_tls_system_database_android_get_instance_private (system_database);

  if (!(priv->file_input_stream_class =
        g_tls_jni_get_class (env,
                            NULL,
                            "java/io/FileInputStream"))) {
    g_critical ("Failed to find class \'java/io/FileInputStream\'");
    return;
  }

  if (!(priv->file_input_stream_constructor =
        g_tls_jni_get_method_id (env,
                                NULL,
                                priv->file_input_stream_class,
                                "<init>",
                                "(Ljava/lang/String;)V"))) {
    g_critical ("Failed to find constructor \'FileInputStream (String)\' "
        " on \'java/io/FileInputStream\'");
    return;
  }

  if (!(priv->system_class = 
        g_tls_jni_get_class (env,
                            NULL,
                            "java/lang/System"))) {
    g_critical ("Could not find class \'java/lang/System\'");
    return;
  }

  if (!(priv->system_get_property =
      g_tls_jni_get_static_method_id (env,
                               NULL,
                               priv->system_class,
                               "getProperty",
                               "(Ljava/lang/String;)Ljava/lang/String;"))) {
    g_critical ("Could not find \'String getProperty(String)\' "
        "on \'java/lang/System\'");
    return;
  }

  g_debug ("system finished");
}

static gchar *
g_tls_system_database_android_create_certificate_handle (GTlsDatabase *database,
    GTlsCertificate *certificate)
{
  GTlsDatabaseAndroid *android_database = G_TLS_DATABASE_ANDROID (database);
  GBytes *der;
  gboolean contains;
  gchar *handle = NULL;

  der = g_tls_certificate_android_get_cert_bytes (G_TLS_CERTIFICATE_ANDROID (certificate));
  g_return_val_if_fail (der != NULL, FALSE);

  g_mutex_lock (&android_database->mutex);

  /* At the same time look up whether this certificate is in list */
  contains = g_hash_table_lookup (android_database->complete, der) ? TRUE : FALSE;

  g_mutex_unlock (&android_database->mutex);

  /* Certificate is in the database */
  if (contains)
    handle = g_tls_database_android_create_handle_for_certificate (SYSTEM_CERT_HANDLE_PREFIX, der);

  g_bytes_unref (der);
  return handle;
}

static void
g_tls_system_database_android_class_init (GTlsSystemDatabaseAndroidClass *klass)
{
  GTlsDatabaseClass *database_class = G_TLS_DATABASE_CLASS (klass);

  database_class->create_certificate_handle = g_tls_system_database_android_create_certificate_handle;
}

static gboolean
g_tls_system_database_android_initable_init (GInitable *initable,
    GCancellable *cancellable, GError **error)
{
  GTlsSystemDatabaseAndroid *system_database = G_TLS_SYSTEM_DATABASE_ANDROID (initable);
  gboolean result;

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return FALSE;

  result = _load_system_keystore (system_database, error);

  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    result = FALSE;

  return result;
}

static void
g_tls_system_database_android_initable_interface_init (GInitableIface *iface)
{
  iface->init = g_tls_system_database_android_initable_init;
}

GTlsDatabase *
g_tls_system_database_android_new (void)
{
  GTlsDatabase *system;
  GError *error = NULL;

  if (!(system = g_initable_new (G_TYPE_TLS_SYSTEM_DATABASE_ANDROID, NULL, &error, NULL))) {
    g_warning ("Failed to load system database: %s", error->message);
  }

  return system;
}
