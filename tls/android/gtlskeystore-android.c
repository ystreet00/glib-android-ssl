/*
 * gtlskeystore-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlskeystore-android.h"

#include "gtlsjniutils.h"

static GTlsKeystoreClass default_keystore;

gboolean
keystore_initialize (GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *my_error = NULL;

  /* java.security.KeyStore */
  if (!(default_keystore.klass =
      g_tls_jni_get_class (env,
                           &my_error,
                           "java/security/KeyStore"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find class \'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.get_default_type =
      g_tls_jni_get_static_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "getDefaultType",
                               "()Ljava/lang/String;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'static String getDefaultType()\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.get_instance =
      g_tls_jni_get_static_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "getInstance",
                               "(Ljava/lang/String;)Ljava/security/KeyStore;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'KeyStore getInstance(String)\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.load =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "load",
                               "(Ljava/security/KeyStore$LoadStoreParameter;)V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'void load(KeyStore.LoadStoreParameter)\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.load_stream =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "load",
                               "(Ljava/io/InputStream;[C)V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'void load(InputStream, char[])\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.aliases =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "aliases",
                               "()Ljava/util/Enumeration;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'Enumeration<> aliases()\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.is_certificate_entry =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "isCertificateEntry",
                               "(Ljava/lang/String;)Z"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'bool isCertificateEntry(String)\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.set_certificate_entry =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "setCertificateEntry",
                               "(Ljava/lang/String;Ljava/security/cert/Certificate;)V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'KeyStore setCertificateEntry(String, Certificate)\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.get_certificate =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "getCertificate",
                               "(Ljava/lang/String;)Ljava/security/cert/Certificate;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'Certificate getCertificate(String)\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.is_key_entry =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "isKeyEntry",
                               "(Ljava/lang/String;)Z"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'bool isKeyEntry(String)\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.get_key =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "getKey",
                               "(Ljava/lang/String;[C)Ljava/security/Key;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'Key getKey(String, char[])\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  if (!(default_keystore.set_key_entry =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_keystore.klass,
                               "setKeyEntry",
                               "(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'void setKeyEntry(String, Key, char[], Certificate[])\' on "
        "\'java.security.KeyStore\'");
    return FALSE;
  }

  return TRUE;
}

GTlsKeystoreClass *
keystore_get (void)
{
  return &default_keystore;
}

jobject
keystore_get_default_type (void)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject result;

  if (!g_tls_jni_call_static_object_method (env,
        &error,
        default_keystore.klass,
        default_keystore.get_default_type,
        &result)) {
    g_critical ("Failed to get default keystore type: %s", error->message);
    return NULL;
  }

  return result;
}

jobject
keystore_get_instance (jobject type)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject result;

  if (!g_tls_jni_call_static_object_method (env,
        &error,
        default_keystore.klass,
        default_keystore.get_instance,
        &result,
        type)) {
    g_critical ("Failed to get default instance type: %s", error->message);
    return NULL;
  }

  return result;
}

void
keystore_load (jobject keystore, jobject params)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
        &error,
        keystore,
        default_keystore.load,
        params)) {
    g_critical ("Failed to load keystore: %s", error->message);
    return;
  }
}

void
keystore_load_stream (jobject keystore, jobject input_stream, jcharArray password)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
        &error,
        keystore,
        default_keystore.load_stream,
        input_stream,
        password)) {
    g_critical ("Failed to load keystore from input stream: %s", error->message);
    return;
  }
}

jobject
keystore_aliases (jobject keystore)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject aliases;

  if (!g_tls_jni_call_object_method (env,
        &error,
        keystore,
        default_keystore.aliases,
        &aliases)) {
    g_critical ("Failed to get keystore aliases: %s", error->message);
    return NULL;
  }

  return aliases;
}

void
keystore_set_certificate_entry (jobject keystore, jobject alias, jobject cert)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
        &error,
        keystore,
        default_keystore.set_certificate_entry,
        alias,
        cert)) {
    g_critical ("Failed to get set certificate entry: %s", error->message);
    return;
  }
}

gboolean
keystore_is_certificate_entry (jobject keystore, jobject alias)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  gboolean result;

  if (!g_tls_jni_call_boolean_method (env,
        &error,
        keystore,
        default_keystore.is_certificate_entry,
        &result,
        alias)) {
    g_critical ("Failed to get check if certificate entry: %s", error->message);
    return FALSE;
  }

  return result;
}

jobject
keystore_get_certificate (jobject keystore, jobject alias)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject result;

  if (!g_tls_jni_call_object_method (env,
        &error,
        keystore,
        default_keystore.get_certificate,
        &result,
        alias)) {
    g_critical ("Failed to get check get certificate entry: %s", error->message);
    return NULL;
  }

  return result;
}

void
keystore_set_key_entry (jobject keystore, jobject alias, jobject key,
    jcharArray password, jobjectArray certificates)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;

  if (!g_tls_jni_call_void_method (env,
        &error,
        keystore,
        default_keystore.set_key_entry,
        alias,
        key,
        password,
        certificates)) {
    g_critical ("Failed to get set key entry: %s", error->message);
    return;
  }
}

gboolean
keystore_is_key_entry (jobject keystore, jobject alias)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  gboolean result;

  if (!g_tls_jni_call_boolean_method (env,
        &error,
        keystore,
        default_keystore.is_key_entry,
        &result,
        alias)) {
    g_critical ("Failed to get check if key entry: %s", error->message);
    return FALSE;
  }

  return result;
}

jobject
keystore_get_key (jobject keystore, jobject alias, jcharArray password)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject result;

  if (!g_tls_jni_call_object_method (env,
        &error,
        keystore,
        default_keystore.get_key,
        &result,
        alias,
        password)) {
    g_critical ("Failed to get check get key entry: %s", error->message);
    return NULL;
  }

  return result;
}
