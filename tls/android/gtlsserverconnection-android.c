/*
 * gtlsserverconnection-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "glib.h"
#include "gtlsserverconnection-android.h"
#include "gtlscertificate-android.h"

#include "gtlscontext-android.h"
#include "gtlsengine-android.h"
#include "gtlsjniutils.h"

typedef struct _GTlsServerConnectionAndroidPrivate
{
  GTlsAuthenticationMode authentication_mode;
} GTlsServerConnectionAndroidPrivate;

enum
{
  PROP_0,
  PROP_AUTHENTICATION_MODE
};

static void g_tls_server_connection_android_initable_interface_init (GInitableIface  *iface);

static void g_tls_server_connection_android_server_connection_interface_init (GTlsServerConnectionInterface *iface);

static GInitableIface *g_tls_server_connection_android_parent_initable_iface;

G_DEFINE_TYPE_WITH_CODE (GTlsServerConnectionAndroid, g_tls_server_connection_android, G_TYPE_TLS_CONNECTION_ANDROID,
                         G_ADD_PRIVATE (GTlsServerConnectionAndroid)
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                g_tls_server_connection_android_initable_interface_init)
                         G_IMPLEMENT_INTERFACE (G_TYPE_TLS_SERVER_CONNECTION,
                                                g_tls_server_connection_android_server_connection_interface_init))

static void
g_tls_server_connection_android_finalize (GObject *object)
{
//  GTlsServerConnectionAndroid *android = G_TLS_SERVER_CONNECTION_ANDROID (object);
//  GTlsServerConnectionAndroidPrivate *priv;

//  priv = g_tls_server_connection_android_get_instance_private (android);

  G_OBJECT_CLASS (g_tls_server_connection_android_parent_class)->finalize (object);
}

static void
g_tls_server_connection_android_get_property (GObject *object,
    guint prop_id, GValue *value, GParamSpec *pspec)
{
  GTlsConnectionAndroid *conn = G_TLS_CONNECTION_ANDROID (object);

  switch (prop_id) {
    case PROP_AUTHENTICATION_MODE:
      g_value_set_enum (value, conn->authentication_mode);
      break;
      
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
g_tls_server_connection_android_set_property (GObject *object,
    guint prop_id, const GValue *value, GParamSpec *pspec)
{
  GTlsConnectionAndroid *conn = G_TLS_CONNECTION_ANDROID (object);

  switch (prop_id) {
    case PROP_AUTHENTICATION_MODE:
      conn->authentication_mode = g_value_get_enum (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static GTlsConnectionBaseStatus
g_tls_server_connection_android_handshake (GTlsConnectionBase *tls,
    GCancellable *cancellable, GError **error)
{
  GTlsServerConnectionAndroid *server = G_TLS_SERVER_CONNECTION_ANDROID (tls);
  GTlsServerConnectionAndroidPrivate *priv;
  GTlsConnectionAndroid *android = G_TLS_CONNECTION_ANDROID (tls);
  JNIEnv *env = g_tls_jni_get_env ();

  priv = g_tls_server_connection_android_get_instance_private (server);

  if (!android->ssl_engine) {
    jobject ssl_engine;

    g_tls_connection_android_configure_context_cert_and_key (android);

    ssl_engine = context_create_ssl_engine (android->ssl_context);
    android->ssl_engine = g_tls_jni_object_make_global (env, ssl_engine);

    engine_set_use_client_mode (android->ssl_engine, FALSE);

    switch (priv->authentication_mode) {
      case G_TLS_AUTHENTICATION_REQUIRED:
        engine_set_need_client_auth (android->ssl_engine, TRUE);
        engine_set_want_client_auth (android->ssl_engine, TRUE);
        break;
      case G_TLS_AUTHENTICATION_REQUESTED:
        engine_set_need_client_auth (android->ssl_engine, FALSE);
        engine_set_want_client_auth (android->ssl_engine, TRUE);
        break;
      case G_TLS_AUTHENTICATION_NONE:
      default:
        engine_set_need_client_auth (android->ssl_engine, FALSE);
        engine_set_want_client_auth (android->ssl_engine, FALSE);
        break;
    }
  }

  return G_TLS_CONNECTION_BASE_CLASS (g_tls_server_connection_android_parent_class)->
    handshake (tls, cancellable, error);
}

static void
g_tls_server_connection_android_class_init (GTlsServerConnectionAndroidClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GTlsConnectionBaseClass *base_class = G_TLS_CONNECTION_BASE_CLASS (klass);

  gobject_class->finalize = g_tls_server_connection_android_finalize;
  gobject_class->get_property = g_tls_server_connection_android_get_property;
  gobject_class->set_property = g_tls_server_connection_android_set_property;

  base_class->handshake = g_tls_server_connection_android_handshake;

  g_object_class_override_property (gobject_class, PROP_AUTHENTICATION_MODE, "authentication-mode");
}

static void
g_tls_server_connection_android_init (GTlsServerConnectionAndroid *android)
{
}

static void
g_tls_server_connection_android_server_connection_interface_init (GTlsServerConnectionInterface *iface)
{
}

static gboolean
g_tls_server_connection_android_initable_init (GInitable *initable,
    GCancellable *cancellable, GError **error)
{
  GTlsCertificate *cert = g_tls_connection_get_certificate (G_TLS_CONNECTION (initable));
  jobject key;

  if (!(key = g_tls_certificate_android_get_key (G_TLS_CERTIFICATE_ANDROID (cert)))) {
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_BAD_CERTIFICATE,
                         "Certificate has no private key");
    return FALSE;
  }

  if (!g_tls_server_connection_android_parent_initable_iface->
      init (initable, cancellable, error))
    return FALSE;

  return TRUE;
}

static void
g_tls_server_connection_android_initable_interface_init (GInitableIface  *iface)
{
  g_tls_server_connection_android_parent_initable_iface = g_type_interface_peek_parent (iface);

  iface->init = g_tls_server_connection_android_initable_init;
}
