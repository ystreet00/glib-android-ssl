/*
 * gtlsenumeration-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "gtlsenumeration-android.h"

#include "gtlsjniutils.h"

static GTlsEnumerationClass default_enumeration;

gboolean
enumeration_initialize (GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *my_error = NULL;

  /* java.util.Enumeration */
  if (!(default_enumeration.klass =
      g_tls_jni_get_class (env,
                           &my_error,
                           "java/util/Enumeration"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'java.util.Enumeration\' class");
    return FALSE;
  }

  if (!(default_enumeration.has_more_elements =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_enumeration.klass,
                               "hasMoreElements",
                               "()Z"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'boolean hasMoreElements()\' on "
        "\'java.util.Enumeration\'");
    return FALSE;
  }

  if (!(default_enumeration.next_element =
      g_tls_jni_get_method_id (env,
                               &my_error,
                               default_enumeration.klass,
                               "nextElement",
                               "()Ljava/lang/Object;"))) {
    g_critical ("%s", my_error->message);
    g_clear_error (&my_error);
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
        "Could not find \'Object nextElement()\' on "
        "\'java.util.Enumeration\'");
    return FALSE;
  }

  return TRUE;
}

GTlsEnumerationClass *
enumeration_get (void)
{
  return &default_enumeration;
}

gboolean
enumeration_has_more_elements (jobject enumeration)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  gboolean more;

  if (!g_tls_jni_call_boolean_method (env, &error,
            enumeration,
            default_enumeration.has_more_elements,
            &more)) {
    g_critical ("Failed to ask if enumeration has more elements: %s", error->message);
    return FALSE;
  }

  return more;
}

jobject
enumeration_next_element (jobject enumeration)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GError *error = NULL;
  jobject element;

  if (!g_tls_jni_call_object_method (env, &error,
            enumeration,
            default_enumeration.next_element,
            &element)) {
    g_critical ("Failed to retrieve next element from enumeration: %s", error->message);
    return NULL;
  }

  return element;
}
