/*
 * gtlsconnection-android.c
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "glib.h"

#include "gtlsconnection-android.h"
#include "gtlsbackend-android.h"
#include "gtlscertificate-android.h"
#include "gtlsfiledatabase-android.h"
#include "gtlsjniutils.h"

#include "gtlsbuffer-android.h"
#include "gtlscontext-android.h"
#include "gtlscustomtrustmanager-android.h"
#include "gtlsengine-android.h"
#include "gtlsenumeration-android.h"
#include "gtlstrustmanagerfactory-android.h"
#include "gtlskeymanagerfactory-android.h"

/* draw the line at 1MB */
#define MAX_BUFFER_SIZE (1024 * 1024)

static GTlsConnectionBaseStatus
g_tls_connection_android_close (GTlsConnectionBase *tls,
    GCancellable *cancellable, GError **error);

typedef struct _GTlsConnectionAndroidPrivate
{
  GTlsCertificate *peer_certificate_tmp;
  GTlsCertificateFlags peer_certificate_errors_tmp;

  gboolean shutting_down;

  jobject ssl_session;

  jclass ssl_session_class;
  jmethodID ssl_session_get_peer_certificates;
  jmethodID ssl_session_get_application_buffer_size;
  jmethodID ssl_session_get_packet_buffer_size;
} GTlsConnectionAndroidPrivate;

static void g_tls_connection_android_initable_iface_init (GInitableIface *iface);

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (GTlsConnectionAndroid, g_tls_connection_android, G_TYPE_TLS_CONNECTION_BASE,
                                  G_ADD_PRIVATE (GTlsConnectionAndroid)
                                  G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                         g_tls_connection_android_initable_iface_init))

static void
g_tls_connection_android_finalize (GObject *object)
{
  GTlsConnectionAndroid *android = G_TLS_CONNECTION_ANDROID (object);
  GTlsConnectionAndroidPrivate *priv;
  JNIEnv *env = g_tls_jni_get_env ();

  priv = g_tls_connection_android_get_instance_private (android);

  g_clear_object (&priv->peer_certificate_tmp);

  if (android->ssl_context)
    g_tls_jni_object_unref (env, android->ssl_context);
  android->ssl_context = NULL;

  if (android->ssl_engine)
    g_tls_jni_object_unref (env, android->ssl_engine);
  android->ssl_engine = NULL;

  if (priv->ssl_session)
    g_tls_jni_object_unref (env, priv->ssl_session);
  priv->ssl_session = NULL;

  G_OBJECT_CLASS (g_tls_connection_android_parent_class)->finalize (object);
}

static GTlsConnectionBaseStatus
g_tls_connection_android_request_rehandshake (GTlsConnectionBase  *tls,
    GCancellable *cancellable, GError **error)
{
  /* On a client-side connection, SSLEngine.beginHandshake() will start
   * a rehandshake, so we only need to do something special here for
   * server-side connections.
   */
  if (!G_IS_TLS_SERVER_CONNECTION (tls))
    return G_TLS_CONNECTION_BASE_OK;

  if (tls->rehandshake_mode == G_TLS_REHANDSHAKE_NEVER) {
    g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
                         "Peer requested illegal TLS rehandshake");
    return G_TLS_CONNECTION_BASE_ERROR;
  }

  return G_TLS_CONNECTION_BASE_OK;
}

static GTlsJniBuffer *
reallocate_buffer (GTlsJniBuffer * old, gsize new_size)
{
  GTlsJniBuffer *new = g_tls_jni_buffer_new (new_size);
  gsize allocated_size = old ? MIN (old->size, new_size) : 0;
  gsize old_limit = MIN (new_size, old ? g_tls_jni_buffer_get_limit (old) : 0);

  if (new_size > MAX_BUFFER_SIZE)
    return old;

  g_debug ("reallocating from %p to %p to a size of %" G_GSIZE_FORMAT, old, new, new_size);

  if (old) {
    gsize old_pos = g_tls_jni_buffer_get_position (old);

    memcpy (new->data, old->data, allocated_size);
    g_tls_jni_buffer_free (old);

    g_tls_jni_buffer_set_position (new, old_pos);
  }
  g_tls_jni_buffer_set_limit (new, old_limit);

  return new;
}

static void
maybe_reallocate_buffers (GTlsConnectionAndroid * android)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsConnectionAndroidPrivate *priv;
  GError *error = NULL;
  gint application_buffer_size, packet_buffer_size;

  priv = g_tls_connection_android_get_instance_private (android);

  if (!priv->ssl_session) {
    /* default to twice the max TLS record size */
    application_buffer_size = packet_buffer_size = 16384 * 2;
  } else {
    if (!g_tls_jni_call_int_method (env, &error,
          priv->ssl_session,
          priv->ssl_session_get_application_buffer_size,
          &application_buffer_size)) {
      g_critical ("Failed to get application buffer size: %s", error->message);
      return;
    }

    if (!g_tls_jni_call_int_method (env, &error,
          priv->ssl_session,
          priv->ssl_session_get_packet_buffer_size,
          &packet_buffer_size)) {
      g_critical ("Failed to get packet buffer size: %s", error->message);
      return;
    }
  }

  if (!android->outbuf || android->outbuf->size < application_buffer_size) {
    android->outbuf = reallocate_buffer (android->outbuf, application_buffer_size);
  }

  if (!android->inbuf || android->inbuf->size < packet_buffer_size) {
    android->inbuf = reallocate_buffer (android->inbuf, packet_buffer_size);
  }

  if (!android->encbuf || android->encbuf->size < packet_buffer_size) {
    android->encbuf = reallocate_buffer (android->encbuf, packet_buffer_size);
  }
}

static GTlsCertificateFlags
verify_peer_certificate (GTlsConnectionAndroid *android,
    GTlsCertificate *peer_certificate)
{
  GTlsConnection *conn = G_TLS_CONNECTION (android);
  GSocketConnectable *peer_identity = NULL;
  GTlsDatabase *database;
  GTlsCertificateFlags errors;

  if (G_IS_TLS_CLIENT_CONNECTION (android))
    peer_identity = g_tls_client_connection_get_server_identity (G_TLS_CLIENT_CONNECTION (android));

  errors = 0;

  database = g_tls_connection_get_database (conn);
  if (database == NULL) {
    errors |= G_TLS_CERTIFICATE_UNKNOWN_CA;
    errors |= g_tls_certificate_verify (peer_certificate, peer_identity, NULL);
  } else {
    GError *error = NULL;

    errors |= g_tls_database_verify_chain (database, peer_certificate,
            G_IS_TLS_CLIENT_CONNECTION (android) ? G_TLS_DATABASE_PURPOSE_AUTHENTICATE_SERVER : G_TLS_DATABASE_PURPOSE_AUTHENTICATE_CLIENT,
            peer_identity, g_tls_connection_get_interaction (conn),
            G_TLS_DATABASE_VERIFY_NONE, NULL, &error);
    if (error) {
      g_warning ("failure verifying certificate chain: %s", error->message);
      g_assert (errors != 0);
      g_clear_error (&error);
    }
  }

  return errors;
}

static GTlsCertificate *
get_peer_certificate (GTlsConnectionAndroid * android)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsConnectionAndroidPrivate *priv;
  GTlsCertificate *ret = NULL;
  GError *error = NULL;
  jobjectArray certs;
  gssize i, n;

  priv = g_tls_connection_android_get_instance_private (android);

  if (!g_tls_jni_call_object_method (env, &error,
            priv->ssl_session,
            priv->ssl_session_get_peer_certificates,
            &certs)) {
    g_debug ("Failed to retrieve peer certificates: %s", error->message);
    g_clear_error (&error);
    return NULL;
  }

  if (!certs)
    return NULL;

  n = (*env)->GetArrayLength (env, certs);
  for (i = n - 1; i >= 0; i--) {
    jobject cert = (*env)->GetObjectArrayElement (env, certs, i);

    g_assert (i >= 0);

    if (!cert) {
      if (ret)
        g_object_unref (ret);
      ret = NULL;
      break;
    }

    ret = g_tls_certificate_android_new_with_cert (cert, ret);
  }
  g_tls_jni_object_local_unref (env, certs);

  return ret;
}

static jobjectArray
trust_manager_get_accepted_issuers (GTlsCustomTrustManager * manager,
    GTlsConnectionAndroid * android)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsDatabase *db;
  GTlsDatabaseAndroid *android_db;
  jobject aliases;
  jobjectArray ret;
  gsize n = 0;

  db = g_tls_connection_get_database (G_TLS_CONNECTION (android));
  if (!db)
    return NULL;

  android_db = G_TLS_DATABASE_ANDROID (db);
  if (!android_db->keystore)
    return NULL;

  aliases = keystore_aliases (android_db->keystore);

  while (enumeration_has_more_elements (aliases)) {
    jobject alias = enumeration_next_element (aliases);
    n++;    
    g_tls_jni_object_local_unref (env, alias);
  }

  if (aliases)
    g_tls_jni_object_local_unref (env, aliases);

  if (n < 1)
    goto out;

  ret = (*env)->NewObjectArray (env, n, g_tls_jni_get_class (env, NULL, "java/security/cert/X509Certificate"), NULL);

  aliases = keystore_aliases (android_db->keystore);

  n = 0;
  while (enumeration_has_more_elements (aliases)) {
    jobject alias = enumeration_next_element (aliases);

    if (keystore_is_certificate_entry (android_db->keystore, alias)) {
      jobject cert;

      cert = keystore_get_certificate (android_db->keystore, alias);

      (*env)->SetObjectArrayElement (env, ret, n, cert);
      n++;
    }

    g_tls_jni_object_local_unref (env, alias);
  }

out:
  if (aliases)
    g_tls_jni_object_local_unref (env, aliases);

  return ret;
}

void
g_tls_connection_android_configure_context_cert_and_key (GTlsConnectionAndroid * android)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsCertificate *cert;
  GTlsDatabase *db;
  jobject keystore;
  jobject default_type;

  /* FIXME: this probably doesn't work for renegotiation when changing keys */

  default_type = keystore_get_default_type ();
  keystore = keystore_get_instance (default_type);
  keystore_load (keystore, NULL);

  /* setup the private key in the keystore */
  cert = g_tls_connection_get_certificate (G_TLS_CONNECTION (android));
  if (cert != NULL) {
    jobject key;
    jobject alias;

    alias = g_tls_jni_string_from_gchar (env, NULL, FALSE, "cert");
    keystore_set_certificate_entry (keystore, alias, g_tls_certificate_android_get_cert (G_TLS_CERTIFICATE_ANDROID (cert)));
    g_tls_jni_object_local_unref (env, alias);

    key = g_tls_certificate_android_get_key (G_TLS_CERTIFICATE_ANDROID (cert));
    if (key) {
      GTlsCertificate *issuer;
      jobject certs;
      jcharArray password;
      gint i, n;

      n = 1;
      for (issuer = g_tls_certificate_get_issuer (G_TLS_CERTIFICATE (cert));
           issuer != NULL;
           issuer = g_tls_certificate_get_issuer (issuer)) {
        n++;
      }

      certs = (*env)->NewObjectArray (env, n, g_tls_jni_get_class (env, NULL, "java/security/cert/Certificate"), NULL);

      (*env)->SetObjectArrayElement (env, certs, 0, g_tls_certificate_android_get_cert (G_TLS_CERTIFICATE_ANDROID (cert)));
      issuer = g_tls_certificate_get_issuer (G_TLS_CERTIFICATE (cert));
      for (i = 1; i < n; i++) {
        (*env)->SetObjectArrayElement (env, certs, i, g_tls_certificate_android_get_cert (G_TLS_CERTIFICATE_ANDROID (issuer)));
        issuer = g_tls_certificate_get_issuer (G_TLS_CERTIFICATE (issuer));
      }

      password = (*env)->NewCharArray (env, 0);
      alias = g_tls_jni_string_from_gchar (env, NULL, FALSE, "key");
      keystore_set_key_entry (keystore, alias, key, password, certs);

      g_tls_jni_object_local_unref (env, alias);
      g_tls_jni_object_local_unref (env, password);
      g_tls_jni_object_local_unref (env, certs);
    }
  }

  /* copy trusted certs from the database */
  db = g_tls_connection_get_database (G_TLS_CONNECTION (android));
  if (db) {
    g_tls_database_android_copy_certs_to_keystore (G_TLS_DATABASE_ANDROID (db), keystore);
  }

  /* initialize trust/key managers for the ssl context */
  {
//    jobject default_trust_type;
//    jobject trust_factory;
    jobject trust_managers;
    jobject default_key_type;
    jobject key_factory;
    jobject key_managers;
    jcharArray password;
    GTlsCustomTrustManager *manager;

    password = (*env)->NewCharArray (env, 0);

//    default_trust_type = trust_manager_factory_get_default_algorithm ();
//    trust_factory = trust_manager_factory_get_instance (default_trust_type);
//    trust_manager_factory_init (trust_factory, keystore, password);
//    trust_managers = trust_manager_factory_get_trust_managers (trust_factory);

    trust_managers = (*env)->NewObjectArray (env, 1, g_tls_jni_get_class (env, NULL, "javax/net/ssl/X509TrustManager"), NULL);
    manager = custom_trust_manager_new (NULL, NULL, (GTlsCustomTrustManagerGetAcceptedIssuersFunc) trust_manager_get_accepted_issuers, android, NULL);
    (*env)->SetObjectArrayElement (env, trust_managers, 0, manager->manager);

    default_key_type = key_manager_factory_get_default_algorithm ();
    key_factory = key_manager_factory_get_instance (default_key_type);
    key_manager_factory_init (key_factory, keystore, password);
    key_managers = key_manager_factory_get_key_managers (key_factory);

    context_init (G_TLS_CONNECTION_ANDROID (android)->ssl_context, key_managers, trust_managers, NULL);

    g_tls_jni_object_local_unref (env, default_key_type);
    g_tls_jni_object_local_unref (env, key_factory);
    g_tls_jni_object_local_unref (env, key_managers);

//    g_tls_jni_object_local_unref (env, default_trust_type);
//    g_tls_jni_object_local_unref (env, trust_factory);
    g_tls_jni_object_local_unref (env, trust_managers);

    g_tls_jni_object_local_unref (env, password);
  }

  g_tls_jni_object_local_unref (env, default_type);
  g_tls_jni_object_local_unref (env, keystore);
}

static GTlsConnectionBaseStatus
wrap_data (GTlsConnectionAndroid *android, void *buffer, gsize count, gssize *nwrote, GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsConnectionBaseStatus ret;
  jobject result;
  GTlsJniBuffer *allocated_buffer = NULL, *jni_buffer;

  if (buffer && count > 0) {
    allocated_buffer = jni_buffer = g_tls_jni_buffer_new_wrapped (buffer, count, NULL);
    g_tls_jni_buffer_set_position (jni_buffer, count);
  } else {
    jni_buffer = android->inbuf;
  }

  g_tls_jni_buffer_set_limit (jni_buffer, g_tls_jni_buffer_get_position (jni_buffer));
  g_tls_jni_buffer_set_position (jni_buffer, 0);

  g_tls_jni_buffer_set_position (android->encbuf, g_tls_jni_buffer_get_limit (android->encbuf));
  g_tls_jni_buffer_set_limit (android->encbuf, android->encbuf->size);

again:
  result = engine_wrap (android->ssl_engine, jni_buffer->buffer, android->encbuf->buffer);

  if (nwrote)
    *nwrote = g_tls_jni_buffer_get_position (jni_buffer);

  switch (engine_result_get_status (result)) {
    case ENGINE_OK:
      ret = G_TLS_CONNECTION_BASE_OK;
      break;
    case ENGINE_BUFFER_OVERFLOW:{
      gsize new_size = MIN (MAX_BUFFER_SIZE, android->encbuf->size * 2);
      if (android->encbuf->size < new_size) {
        g_debug ("%p wrap: overflow, resizing output buffer", android);
        android->encbuf = reallocate_buffer (android->encbuf, new_size);
        g_tls_jni_buffer_set_limit (android->encbuf, android->encbuf->size);
        goto again;
      } else {
        g_debug ("%p wrap: overflow, also have a large output buffer, "
            "returning so we can empty it", android);
      }
    }
    case ENGINE_BUFFER_UNDERFLOW:
      g_debug ("%p wrap: underflow !! should never happen !!", android);
      g_assert_not_reached ();
      break;
    case ENGINE_CLOSED:{
      g_debug ("%p wrap: received engine close", android);
      ret = G_TLS_CONNECTION_BASE_ERROR;
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED,
          "SSLEngine was closed");
      break;
    }
    case ENGINE_ERROR:
      g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "SSLEngine produced an unspecified error");
      ret = G_TLS_CONNECTION_BASE_ERROR;
      break;
  }

  g_tls_jni_buffer_set_position (jni_buffer, g_tls_jni_buffer_get_limit (jni_buffer));
  g_tls_jni_buffer_set_limit (jni_buffer, jni_buffer->size);

  g_tls_jni_buffer_set_limit (android->encbuf, g_tls_jni_buffer_get_position (android->encbuf));
  g_tls_jni_buffer_set_position (android->encbuf, 0);

  g_tls_jni_object_local_unref (env, result);
  if (allocated_buffer)
    g_tls_jni_buffer_free (allocated_buffer);

  return ret;
}

static GTlsConnectionBaseStatus
unwrap_data (GTlsConnectionAndroid *android, gssize *nconsumed,
    gssize *nproduced, GError **error)
{
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsConnectionBaseStatus ret;
  jobject result;
  gssize consumed;

  g_tls_jni_buffer_set_limit (android->inbuf, g_tls_jni_buffer_get_position (android->inbuf));
  g_tls_jni_buffer_set_position (android->inbuf, 0);

  g_tls_jni_buffer_set_position (android->outbuf, g_tls_jni_buffer_get_limit (android->outbuf));
  g_tls_jni_buffer_set_limit (android->outbuf, android->outbuf->size);

again:
  g_debug ("%p unwrap data from pos %" G_GSIZE_FORMAT " limit %" G_GSIZE_FORMAT " into output pos %" G_GSIZE_FORMAT " limit %" G_GSIZE_FORMAT, android, g_tls_jni_buffer_get_position (android->inbuf), g_tls_jni_buffer_get_limit (android->inbuf), g_tls_jni_buffer_get_position (android->outbuf), g_tls_jni_buffer_get_limit (android->outbuf));

  result = engine_unwrap (android->ssl_engine, android->inbuf->buffer, android->outbuf->buffer);

  g_debug ("%p unwrap status 0x%x consumed %u produced %u", android, engine_result_get_status (result), engine_result_bytes_consumed (result), engine_result_bytes_produced (result));
  consumed = engine_result_bytes_consumed (result);
  if (nconsumed)
    *nconsumed = consumed;
  if (nproduced)
    *nproduced = engine_result_bytes_produced (result);

  switch (engine_result_get_status (result)) {
    case ENGINE_OK:
      ret = G_TLS_CONNECTION_BASE_OK;
      break;
    case ENGINE_CLOSED:
      g_debug ("%p unwrap: received engine close", android);
      if (!engine_is_outbound_done (android->ssl_engine)) {
        engine_close_outbound (android->ssl_engine);
      }
      ret = G_TLS_CONNECTION_BASE_OK;
      break;
    case ENGINE_BUFFER_UNDERFLOW:
      g_debug ("%p unwrap: received underflow", android);
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_WOULD_BLOCK,
          "IO Would block (non-fatal)");
      ret = G_TLS_CONNECTION_BASE_ERROR;
      break;
    case ENGINE_BUFFER_OVERFLOW:{
      gsize new_size = MIN (MAX_BUFFER_SIZE, android->outbuf->size * 2);
      if (android->outbuf->size < new_size) {
        g_debug ("%p unwrap: overflow, resizing output buffer", android);
        android->outbuf = reallocate_buffer (android->outbuf, new_size);
        g_tls_jni_buffer_set_limit (android->outbuf, android->outbuf->size);
        goto again;
      } else {
        g_debug ("%p unwrap: overflow, also have a large output buffer, "
            "returning so we can empty it", android);
      }
    }
    case ENGINE_ERROR:
      g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "SSLEngine produced an unspecified error");
      ret = G_TLS_CONNECTION_BASE_ERROR;
      break;
  }

  if (consumed == 0) {
    /* unwrapping did nothing. move along */
  } else if (g_tls_jni_buffer_get_position (android->inbuf) < g_tls_jni_buffer_get_limit (android->inbuf)) {
    /* otherwise engine_unwrap() gets confused if the data is not at the
     * beginning of the buffer anymore*/
    gssize new_limit = g_tls_jni_buffer_get_limit (android->inbuf) - consumed;

    /* otherwise the if condition wouldn't be true */
    g_assert (new_limit > 0);

    memcpy (android->inbuf->data, &android->inbuf->data[consumed], new_limit);
    g_tls_jni_buffer_set_limit (android->inbuf, new_limit);
  } else if (g_tls_jni_buffer_get_position (android->inbuf) == g_tls_jni_buffer_get_limit (android->inbuf)) {
    /* we've got no more data to read so we can read from the stream into
     * the beginning again */
    g_tls_jni_buffer_set_limit (android->inbuf, 0);
  }

  g_tls_jni_buffer_set_position (android->inbuf, g_tls_jni_buffer_get_limit (android->inbuf));
  g_tls_jni_buffer_set_limit (android->inbuf, android->inbuf->size);

  g_tls_jni_buffer_set_limit (android->outbuf, g_tls_jni_buffer_get_position (android->outbuf));
  g_tls_jni_buffer_set_position (android->outbuf, 0);

  g_tls_jni_object_local_unref (env, result);

  return ret;
}

/* attempt to write from position to limit of android->encbuf to the write
 * GIOStream returning what could be written or an error */
static GTlsConnectionBaseStatus
_write_encoded_to_stream (GTlsConnectionAndroid *android,
    gboolean blocking, gsize *nwrote, GCancellable *cancellable,
    GError **error)
{
  GTlsConnectionBase *tls = G_TLS_CONNECTION_BASE (android);
  gsize pos = g_tls_jni_buffer_get_position (android->encbuf);
  gsize limit = g_tls_jni_buffer_get_limit (android->encbuf);
  gsize buf_size = limit - pos;
  GTlsConnectionBaseStatus ret;
  gsize n_written;
  gboolean success;

  g_assert (pos < limit);

  g_tls_connection_base_push_io (tls, G_IO_OUT, blocking, cancellable);
  success = g_pollable_stream_write_all (g_io_stream_get_output_stream (tls->base_io_stream),
                                         &android->encbuf->data[pos], buf_size,
                                         blocking, &n_written, cancellable, &tls->write_error);
  ret = g_tls_connection_base_pop_io (tls, G_IO_OUT, success, error);
  g_debug ("%p wrote %" G_GSIZE_FORMAT " bytes", android, n_written);

  if (n_written < buf_size) {
    g_tls_jni_buffer_set_position (android->encbuf, pos + n_written);
  } else {
    g_tls_jni_buffer_set_position (android->encbuf, 0);
    g_tls_jni_buffer_set_limit (android->encbuf, 0);
  }

  if (nwrote)
    *nwrote = n_written;

  return ret;
}

/* reads as much data from the GIOStream as will fit in android->inbuf
 * starting from it's position */
static GTlsConnectionBaseStatus
_read_encoded_from_stream (GTlsConnectionAndroid *android, gboolean blocking,
    gssize * received, GCancellable * cancellable, GError ** error)
{
  GTlsConnectionBase *tls = G_TLS_CONNECTION_BASE (android);
  gsize in_pos = g_tls_jni_buffer_get_position (android->inbuf);
  GTlsConnectionBaseStatus ret;
  gssize n_read = 0;

  g_tls_connection_base_push_io (tls, G_IO_IN, blocking, cancellable);
  n_read = g_pollable_stream_read (g_io_stream_get_input_stream (tls->base_io_stream),
                                   &android->inbuf->data[in_pos], android->inbuf->size - in_pos, blocking, cancellable,
                                   &tls->read_error);
  g_debug ("%p have read %" G_GSSIZE_FORMAT " bytes into position %" G_GSIZE_FORMAT, android, n_read, in_pos);
  ret = g_tls_connection_base_pop_io (tls, G_IO_IN, n_read > 0, error);

  g_tls_jni_buffer_set_limit (android->inbuf, android->inbuf->size);
  if (n_read > 0)
    g_tls_jni_buffer_set_position (android->inbuf, in_pos + n_read);

  if (n_read == 0) {
    g_print ("received 0 bytes!\n");
    g_usleep (100000);
    g_set_error (error, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED, "Closed");
  }

  if (received)
    *received = n_read;

  return ret;
}

static gboolean
unwrap_all_data (GTlsConnectionAndroid *android, gboolean * no_data,
    GError ** error)
{
  gssize n_read = -1, total_consumed = 0;
  GTlsConnectionBaseStatus status;
  GError *my_error = NULL;

  if (no_data)
    *no_data = TRUE;

  /* attempts to unwrap all the data in the read buffer until unwrap does not
   * consume any more data */
  n_read = g_tls_jni_buffer_get_position (android->inbuf);
  while (total_consumed < n_read) {
    gssize consumed = 0;

    status = unwrap_data (android, &consumed, NULL, &my_error);
    if (no_data)
      *no_data = *no_data && consumed == 0;

    if (status != G_TLS_CONNECTION_BASE_OK &&
        g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_WOULD_BLOCK)) {
      /* nothing to do, we'll loop again and read more data */
      g_clear_error (&my_error);
      return TRUE;
    } else if (status != G_TLS_CONNECTION_BASE_OK) {
      g_propagate_error (error, my_error);
      return FALSE;
    }

    total_consumed += consumed;

    if (consumed == 0) {
      /* unwrapping did nothing. move along */
      return TRUE;
    }
  }

  return TRUE;
}

static GTlsConnectionBaseStatus
do_handshake (GTlsConnectionAndroid *android, GCancellable *cancellable,
    GError **error)
{
  GTlsConnectionBaseStatus status;
  EngineHandshakeStatus handshake_status = ENGINE_HANDSHAKE_NEED_TASK;
  GError *my_error = NULL;

  do {
    handshake_status = engine_get_handshake_status (android->ssl_engine);
    g_debug ("%p got handshake status 0x%x", android, handshake_status);

    if (handshake_status == ENGINE_HANDSHAKE_NEED_TASK) {
      engine_run_delegated_task (android->ssl_engine);
    } else if (handshake_status == ENGINE_HANDSHAKE_NEED_WRAP) {
      /* attempt to write encoded data to the GIOStream */
      gsize total_written = 0;
      gsize enc_limit;

      /* 1. we encode data to send to the peer and deal with any close
       * notifies we need to send */
      status = wrap_data (android, NULL, 0, NULL, &my_error);
      if (status != G_TLS_CONNECTION_BASE_OK) {
        if (g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CLOSED) ||
            g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED)) {
          g_clear_error (&my_error);
          if (engine_is_outbound_done (android->ssl_engine)) {
            return G_TLS_CONNECTION_BASE_OK;
          } else {
            /* need to write out any possible extra data and perform cleanup */
            engine_close_outbound (android->ssl_engine);
            continue;
          }
        } else {
          g_propagate_error (error, my_error);
          return status;
        }
      }

      enc_limit = g_tls_jni_buffer_get_limit (android->encbuf);

      while (total_written < enc_limit) {
        gsize n_written;

      /* 2. we write that data to the stream */
        status = _write_encoded_to_stream (android, TRUE, &n_written, cancellable, error);
        if (status != G_TLS_CONNECTION_BASE_OK) {
          g_debug ("%p handshake write not-ok", android);
          return status;
        }

        total_written += n_written;
      }
    } else if (handshake_status == ENGINE_HANDSHAKE_NEED_UNWRAP) {
      /* read data from the GIOStream */
      gssize n_read = -1;

      /* try unwrapping previously read data */
unwrap_start:
      if (g_tls_jni_buffer_get_position (android->inbuf) > 0) {
        gboolean no_data;

        if (!unwrap_all_data (android, &no_data, &my_error)) {
          if (g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CLOSED) ||
              g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED)) {
            g_clear_error (&my_error);
            if (engine_is_outbound_done (android->ssl_engine)) {
              /* normal shutdown */
              return G_TLS_CONNECTION_BASE_OK;
            } else {
              /* otherwise signal a shutdown which will attempt to send
               * the close notify to the peer */
              engine_close_outbound (android->ssl_engine);
              continue;
            }
          } else {
            return G_TLS_CONNECTION_BASE_ERROR;
          }
        }

        if (!no_data)
          /* only attempt a read if unwrapping did not advance */
          continue;
      }

      /* 1. read data from the stream */
      status = _read_encoded_from_stream (android, TRUE, &n_read, cancellable, &my_error);
      if (status != G_TLS_CONNECTION_BASE_OK) {
        if (g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CLOSED) ||
            g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED)) {
          g_clear_error (&my_error);
          if (engine_is_inbound_done(android->ssl_engine) && engine_is_outbound_done (android->ssl_engine)) {
            /* normal shutdown */
            g_debug ("%p connection closed", android);
          } else {
            /* inbound close can throw if the peer didn't send us a close notify */
            if (!engine_close_inbound (android->ssl_engine)) {
              g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_EOF,
                  "Connection was closed without notification");
              return G_TLS_CONNECTION_BASE_ERROR;
            } else {
              engine_close_outbound (android->ssl_engine);
              /* we need to loop again to possibly send the close notify to the
               * peer if the channel is still open */
              continue;
            }
          }
        }
        g_propagate_error (error, my_error);
        if (status != G_TLS_CONNECTION_BASE_OK)
          g_debug ("%p handshake read not-ok", android);
        return status;
      }

      /* XXX: for some reason, we need to unwrap before we read to make
       * client auth work */
      /* 2. decode data */
      if (n_read > 0)
        goto unwrap_start;
    }
  } while (handshake_status != ENGINE_HANDSHAKE_FINISHED &&
            handshake_status != ENGINE_HANDSHAKE_NOT_HANDSHAKING);

  return G_TLS_CONNECTION_BASE_OK;
}

static GTlsConnectionBaseStatus
g_tls_connection_android_handshake (GTlsConnectionBase *tls,
    GCancellable *cancellable, GError **error)
{
  GTlsConnectionAndroid *android = G_TLS_CONNECTION_ANDROID (tls);
  GTlsConnectionAndroidPrivate *priv;
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsConnectionBaseStatus status;

  priv = g_tls_connection_android_get_instance_private (android);

  /* subclass allocates this */
  g_assert (android->ssl_context != NULL);
  g_assert (android->ssl_engine != NULL);

  engine_begin_handshake (android->ssl_engine);

  maybe_reallocate_buffers (android);

  if ((status = do_handshake (android, cancellable, error) != G_TLS_CONNECTION_BASE_OK))
    return status;

  {
    jobject session = engine_get_session (android->ssl_engine);

    if (priv->ssl_session) {
      g_tls_jni_object_unref (env, priv->ssl_session);
      priv->ssl_session = NULL;
    }

    if (!(priv->ssl_session = g_tls_jni_object_make_global (env, session))) {
      g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_MISC,
          "Could not make SSLObject a global reference");
      return G_TLS_CONNECTION_BASE_ERROR;
    }
  }

  priv->peer_certificate_tmp = get_peer_certificate (android);
  if (priv->peer_certificate_tmp)
    priv->peer_certificate_errors_tmp = verify_peer_certificate (android, priv->peer_certificate_tmp);
  else if (android->authentication_mode == G_TLS_AUTHENTICATION_REQUIRED) {
    g_set_error (error, G_TLS_ERROR, G_TLS_ERROR_BAD_CERTIFICATE,
                 "%s did not return a valid TLS certificate",
                 G_IS_TLS_CLIENT_CONNECTION (android) ? "Server" : "Client");
  }

  return status;
}

static GTlsConnectionBaseStatus
g_tls_connection_android_complete_handshake (GTlsConnectionBase  *tls,
                                             GError             **error)
{
  GTlsConnectionAndroid *android = G_TLS_CONNECTION_ANDROID (tls);
  GTlsConnectionAndroidPrivate *priv;
  GTlsCertificate *peer_certificate;
  GTlsCertificateFlags peer_certificate_errors = 0;
  GTlsConnectionBaseStatus status = G_TLS_CONNECTION_BASE_OK;

  priv = g_tls_connection_android_get_instance_private (android);

  g_debug ("%p handshake completed", android);

  peer_certificate = priv->peer_certificate_tmp;
  priv->peer_certificate_tmp = NULL;
  peer_certificate_errors = priv->peer_certificate_errors_tmp;
  priv->peer_certificate_errors_tmp = 0;

  if (peer_certificate) {
    if (!g_tls_connection_base_accept_peer_certificate (tls, peer_certificate,
                                                        peer_certificate_errors)) {
      g_set_error_literal (error, G_TLS_ERROR, G_TLS_ERROR_BAD_CERTIFICATE,
                           "Unacceptable TLS certificate");
      status = G_TLS_CONNECTION_BASE_ERROR;
    }

    g_tls_connection_base_set_peer_certificate (G_TLS_CONNECTION_BASE (android),
                                                peer_certificate,
                                                peer_certificate_errors);
    g_clear_object (&peer_certificate);
  }

  return status;
}

static GTlsConnectionBaseStatus
_copy_outbuf_to_data (GTlsConnectionAndroid *android, void *buffer,
    gsize count, gssize * nread)
{
  gsize pos = g_tls_jni_buffer_get_position (android->outbuf);
  gsize limit = g_tls_jni_buffer_get_limit (android->outbuf);
  gsize buf_size = limit - pos;

  g_assert (pos <= limit);

  if (buf_size == 0)
    return G_TLS_CONNECTION_BASE_TRY_AGAIN;

  *nread = MIN (count, buf_size);
  g_debug ("%p read: copy %" G_GSIZE_FORMAT " from %" G_GSIZE_FORMAT " limit %" G_GSIZE_FORMAT, android, *nread, pos, limit);
  memcpy (buffer, &android->outbuf->data[pos], *nread);
  if (*nread < buf_size) {
    g_tls_jni_buffer_set_position (android->outbuf, pos + *nread);
  } else {
    g_tls_jni_buffer_set_position (android->outbuf, 0);
    g_tls_jni_buffer_set_limit (android->outbuf, 0);
  }

  return G_TLS_CONNECTION_BASE_OK;
}

static GTlsConnectionBaseStatus
check_for_handshake (GTlsConnectionAndroid *android, GCancellable * cancellable,
    GError ** error)
{
  if (engine_get_handshake_status (android->ssl_engine) != ENGINE_HANDSHAKE_NOT_HANDSHAKING) {
    GTlsConnectionBaseStatus ret;

    ret = do_handshake (android, cancellable, error);

    if (ret != G_TLS_CONNECTION_BASE_OK)
      return ret;
    return G_TLS_CONNECTION_BASE_TRY_AGAIN;
  }

  return G_TLS_CONNECTION_BASE_OK;
}

static GTlsConnectionBaseStatus
g_tls_connection_android_read (GTlsConnectionBase *tls, void *buffer,
    gsize count, gboolean blocking, gssize *nread, GCancellable *cancellable,
    GError **error)
{
  GTlsConnectionAndroid *android = G_TLS_CONNECTION_ANDROID (tls);
  GTlsConnectionBaseStatus ret;
  GError *my_error = NULL;
  gssize received;

  if (count == 0)
    return G_TLS_CONNECTION_BASE_OK;

again:
  ret = _copy_outbuf_to_data (android, buffer, count, nread);
  if (ret == G_TLS_CONNECTION_BASE_OK)
    return ret;
  if (ret == G_TLS_CONNECTION_BASE_TRY_AGAIN) {
    /* noop */
  } else {
    g_assert_not_reached ();
  }

  g_assert (g_tls_jni_buffer_get_position (android->outbuf) == g_tls_jni_buffer_get_limit (android->outbuf));

  if (g_tls_jni_buffer_get_position (android->inbuf) > 0) {
    gboolean no_data;

    /* try unwrapping previously read data and returning that */
    if (!unwrap_all_data (android, &no_data, error)) {
      if (g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CLOSED) ||
          g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED)) {
        g_clear_error (&my_error);
        if (engine_is_outbound_done (android->ssl_engine)) {
          /* normal shutdown */
          return G_TLS_CONNECTION_BASE_OK;
        } else {
          /* otherwise signal a shutdown which will attempt to send
           * the close notify to the peer */
          engine_close_outbound (android->ssl_engine);
        }
      } else {
        return G_TLS_CONNECTION_BASE_ERROR;
      }
    }

    if (!no_data) {
      ret = _copy_outbuf_to_data (android, buffer, count, nread);
      g_assert (ret != G_TLS_CONNECTION_BASE_TRY_AGAIN);
      return ret;
    }

    if (check_for_handshake (android, cancellable, error) == G_TLS_CONNECTION_BASE_TRY_AGAIN)
      goto again;
  }

  ret = _read_encoded_from_stream (android, blocking, &received, cancellable, &my_error);

  if (ret != G_TLS_CONNECTION_BASE_OK) {
    if (g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CLOSED) ||
        g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED)) {
      engine_close_inbound (android->ssl_engine);
      g_clear_error (&my_error);
      return g_tls_connection_android_close (tls, cancellable, error);
    }
    g_propagate_error (error, my_error);
    return ret;
  }

  if (!unwrap_all_data (android, NULL, error)) {
    if (g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CLOSED) ||
        g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED)) {
      g_clear_error (&my_error);
      if (engine_is_outbound_done (android->ssl_engine)) {
        /* normal shutdown */
        return G_TLS_CONNECTION_BASE_OK;
      } else {
        /* otherwise signal a shutdown which will attempt to send
         * the close notify to the peer */
        engine_close_outbound (android->ssl_engine);
      }
    } else {
       return G_TLS_CONNECTION_BASE_ERROR;
    }
  }

  if (check_for_handshake (android, cancellable, error) == G_TLS_CONNECTION_BASE_TRY_AGAIN)
    goto again;

  ret = _copy_outbuf_to_data (android, buffer, count, nread);
  if (ret == G_TLS_CONNECTION_BASE_OK) {
    /* noop */
  } else if (ret == G_TLS_CONNECTION_BASE_TRY_AGAIN) {
    if (blocking)
      goto again;
    g_set_error (error, G_IO_ERROR, G_IO_ERROR_WOULD_BLOCK,
        "Operation would block");
    return G_TLS_CONNECTION_BASE_ERROR;
  } else {
    g_assert_not_reached ();
  }

  return G_TLS_CONNECTION_BASE_OK;
}

static GTlsConnectionBaseStatus
g_tls_connection_android_write (GTlsConnectionBase *tls, const void *buffer,
    gsize count, gboolean blocking, gssize *nwrote, GCancellable *cancellable,
    GError **error)
{
  GTlsConnectionAndroid *android = G_TLS_CONNECTION_ANDROID (tls);
  GTlsConnectionBaseStatus ret;
  GError *my_error = NULL;

  if (count == 0)
    return G_TLS_CONNECTION_BASE_OK;

  if (g_tls_jni_buffer_get_position (android->encbuf) < g_tls_jni_buffer_get_limit (android->encbuf)) {
    /* still have data that's been encrypted but not written */
    *nwrote = 0;
    ret = _write_encoded_to_stream (android, blocking, NULL, cancellable, error);
    if (ret != G_TLS_CONNECTION_BASE_OK)
      return ret;
  }

  g_assert (g_tls_jni_buffer_get_position (android->encbuf) == g_tls_jni_buffer_get_limit (android->encbuf));

  ret = wrap_data (android, (void *) buffer, count, nwrote, &my_error);
  if (ret != G_TLS_CONNECTION_BASE_OK) {
    if (g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CLOSED) ||
        g_error_matches (my_error, G_IO_ERROR, G_IO_ERROR_CONNECTION_CLOSED)) {
      /* need to write out any possible extra data and perform cleanup */
      g_clear_error (&my_error);
      return g_tls_connection_android_close (tls, cancellable, error);
    } else {
      g_propagate_error (error, my_error);
      return ret;
    }
  }

  ret = _write_encoded_to_stream (android, blocking, NULL, cancellable, error);
  if (ret != G_TLS_CONNECTION_BASE_OK)
    return ret;

  return ret;
}

static GTlsConnectionBaseStatus
g_tls_connection_android_close (GTlsConnectionBase *tls,
    GCancellable *cancellable, GError **error)
{
  GTlsConnectionAndroid *android = G_TLS_CONNECTION_ANDROID (tls);
  JNIEnv *env = g_tls_jni_get_env ();
  GTlsConnectionAndroidPrivate *priv;
  GTlsConnectionBaseStatus ret;

  priv = g_tls_connection_android_get_instance_private (android);

  priv->shutting_down = TRUE;

  if (!android->ssl_engine)
    return G_TLS_CONNECTION_BASE_OK;

  engine_close_outbound (android->ssl_engine);
  ret = do_handshake (android, cancellable, error);

  g_tls_jni_object_unref (env, android->ssl_engine);
  android->ssl_engine = NULL;

  return ret;
}

static void
g_tls_connection_android_class_init (GTlsConnectionAndroidClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GTlsConnectionBaseClass *base_class = G_TLS_CONNECTION_BASE_CLASS (klass);

  gobject_class->finalize     = g_tls_connection_android_finalize;

  base_class->request_rehandshake = g_tls_connection_android_request_rehandshake;
  base_class->handshake = g_tls_connection_android_handshake;
  base_class->complete_handshake = g_tls_connection_android_complete_handshake;
  base_class->read_fn = g_tls_connection_android_read;
  base_class->write_fn = g_tls_connection_android_write;
  base_class->close_fn = g_tls_connection_android_close;
}

static gboolean
g_tls_connection_android_initable_init (GInitable *initable,
    GCancellable *cancellable, GError **error)
{
  return TRUE;
}

static void
g_tls_connection_android_initable_iface_init (GInitableIface *iface)
{
  iface->init = g_tls_connection_android_initable_init;
}

static void
g_tls_connection_android_init (GTlsConnectionAndroid *android)
{
  GTlsConnectionAndroidPrivate *priv;
  JNIEnv *env = g_tls_jni_get_env ();

  priv = g_tls_connection_android_get_instance_private (android);

  /* javax.net.ssl.SSLSession */
  if (!(priv->ssl_session_class =
      g_tls_jni_get_class (env,
                           NULL,
                           "javax/net/ssl/SSLSession"))) {
    g_critical ("Could not find \'javax.net.ssl.SSLSession\' class");
    return;
  }

  if (!(priv->ssl_session_get_peer_certificates =
      g_tls_jni_get_method_id (env,
                               NULL,
                               priv->ssl_session_class,
                               "getPeerCertificates",
                               "()[Ljava/security/cert/Certificate;"))) {
    g_critical ("Could not find \'Certificate[] getPeerCertificates()\' on "
        "\'javax.net.ssl.SSLSession\'");
    return;
  }

  if (!(priv->ssl_session_get_application_buffer_size =
      g_tls_jni_get_method_id (env,
                               NULL,
                               priv->ssl_session_class,
                               "getApplicationBufferSize",
                               "()I"))) {
    g_critical ("Could not find \'int getApplicationBufferSize()\' on "
        "\'javax.net.ssl.SSLSession\'");
    return;
  }

  if (!(priv->ssl_session_get_packet_buffer_size =
      g_tls_jni_get_method_id (env,
                               NULL,
                               priv->ssl_session_class,
                               "getPacketBufferSize",
                               "()I"))) {
    g_critical ("Could not find \'int getPacketBufferSize()\' on "
        "\'javax.net.ssl.SSLSession\'");
    return;
  }

  {
    jobject name = g_tls_jni_string_from_gchar (env, NULL, FALSE, "TLS");
    jobject context;

    context = context_get_instance (name);

    if (!(android->ssl_context = g_tls_jni_object_make_global (env, context))) {
      g_critical ("Failed to make SSLContext a global JNI reference");
    }

    g_tls_jni_object_local_unref (env, name);
  }
}

gboolean
g_tls_connection_android_request_certificate (GTlsConnectionAndroid  *android,
                                              GError                **error)
{
  GTlsInteractionResult res = G_TLS_INTERACTION_UNHANDLED;
  GTlsInteraction *interaction;
  GTlsConnection *conn;
  GTlsConnectionBase *tls;

  g_return_val_if_fail (G_IS_TLS_CONNECTION_ANDROID (android), FALSE);

  conn = G_TLS_CONNECTION (android);
  tls = G_TLS_CONNECTION_BASE (android);

  interaction = g_tls_connection_get_interaction (conn);
  if (!interaction)
    return FALSE;

  res = g_tls_interaction_invoke_request_certificate (interaction, conn, 0,
						      tls->read_cancellable, error);
  return res != G_TLS_INTERACTION_FAILED;
}
