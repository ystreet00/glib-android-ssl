/*
 * gtlscertificate-android.h
 *
 * Copyright (C) Matthew Waters <matthew@centricular.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __G_TLS_CERTIFICATE_ANDROID_H__
#define __G_TLS_CERTIFICATE_ANDROID_H__

#include <gio/gio.h>
#include <jni.h>

G_BEGIN_DECLS

#define G_TYPE_TLS_CERTIFICATE_ANDROID (g_tls_certificate_android_get_type ())
G_DECLARE_DERIVABLE_TYPE (GTlsCertificateAndroid, g_tls_certificate_android,
                          G, TLS_CERTIFICATE_ANDROID, GTlsCertificate)

struct _GTlsCertificateAndroidClass
{
  GTlsCertificateClass parent_class;
};

gboolean            certificate_initialize                      (GError ** error);


GTlsCertificate *   g_tls_certificate_android_new              (GBytes              *bytes,
                                                                GTlsCertificate     *issuer);
GTlsCertificate *   g_tls_certificate_android_new_with_cert    (jobject              cert,
                                                                GTlsCertificate     *issuer);

GBytes *            g_tls_certificate_android_get_cert_bytes        (GTlsCertificateAndroid *self);

GBytes *            g_tls_certificate_android_get_subject_sequence  (GTlsCertificateAndroid *self);
GBytes *            g_tls_certificate_android_get_issuer_sequence   (GTlsCertificateAndroid *self);

jobject             g_tls_certificate_android_get_cert      (GTlsCertificateAndroid * self);
jobject             g_tls_certificate_android_get_key       (GTlsCertificateAndroid * self);

GTlsCertificateFlags    g_tls_certificate_android_verify_with_keystore (GTlsCertificateAndroid *cert_android,
                                                                        GSocketConnectable *identity,
                                                                        jobject key_store,
                                                                        const gchar * purpose,
                                                                        GError ** error);

G_END_DECLS

#endif /* __G_TLS_CERTIFICATE_ANDROID_H___ */
