/* GIO TLS JNI test helper
 *
 * Copyright 2018 Matthew Waters <matthew@centricular.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <gio/gio.h>

#include <jni.h>
#include <pthread.h>

extern int ACTUAL_FUNC_TO_RUN (int argc, char *argv[]);

static pthread_key_t current_jni_env;
static JavaVM *java_vm;

/* Register this thread with the VM */
static JNIEnv *
attach_current_thread (void)
{
  JNIEnv *env;
  JavaVMAttachArgs args;

  args.version = JNI_VERSION_1_4;
  args.name = NULL;
  args.group = NULL;

  g_debug ("Attaching current thread to the Java VM\n");
  if ((*java_vm)->AttachCurrentThread (java_vm, &env, &args) < 0) {
    g_critical ("Failed to attach current thread");
    return NULL;
  }

  return env;
}

/* Unregister this thread from the VM */
static void
detach_current_thread (void *env)
{
  g_debug ("Detaching current thread from the Java VM\n");
  (*java_vm)->DetachCurrentThread (java_vm);
}

/* Retrieve the JNI environment for this thread */
static JNIEnv *
get_jni_env (void)
{
  JNIEnv *env;

  if ((env = pthread_getspecific (current_jni_env)) == NULL) {
    env = attach_current_thread ();
    pthread_setspecific (current_jni_env, env);
  }

  return env;
}

static int
native_run(JNIEnv * env, jclass klass, jobjectArray args)
{
  gsize n_args;
  char **c_args;
  int i, ret;

  n_args = (*env)->GetArrayLength (env, args);

  c_args = g_new0 (char *, n_args + 2);

  /* process name */
  c_args[0] = g_strdup ("process");
  /* for g_strfreev */
  c_args[n_args + 1] = NULL;
  for (i = 0; i < n_args; i++) {
    const gchar *s = NULL;
    jstring arg = (*env)->GetObjectArrayElement (env, args, i);

    s = (*env)->GetStringUTFChars (env, arg, NULL);
    if (!s) {
      g_error ("Failed to convert string to UTF8");
      return -1;
    }

    c_args[i+1] = g_strdup (s);
    (*env)->ReleaseStringUTFChars (env, arg, s);
  }

  ret = ACTUAL_FUNC_TO_RUN ((int) (n_args + 1), c_args);

  g_strfreev (c_args);

  return ret;
}

/* List of implemented native methods */
static JNINativeMethod native_methods[] = {
  {"nativeRun", "([Ljava/lang/String;)I", (void *) native_run},
};

/* Library initializer */
jint
JNI_OnLoad (JavaVM * vm, void *reserved)
{
  JNIEnv *env = NULL;

  java_vm = vm;

  if ((*vm)->GetEnv (vm, (void **) &env, JNI_VERSION_1_4) != JNI_OK) {
    g_error ("Could not retrieve JNIEnv\n");
    return 0;
  }
  jclass klass = (*env)->FindClass (env, "org/gnome/glib/ssl/Test");
  if (!klass) {
    g_error ("Could not retrieve test class\n");
    return 0;
  }
  if ((*env)->RegisterNatives (env, klass, native_methods,
          G_N_ELEMENTS (native_methods))) {
    g_error ("Could not register native methods for test class\n");
    return 0;
  }

  pthread_key_create (&current_jni_env, detach_current_thread);

  return JNI_VERSION_1_4;
}
