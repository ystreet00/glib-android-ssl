package org.gnome.glib.ssl;

import java.io.Closeable;
import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;
import javax.net.ssl.SSLEngine;
import java.net.Socket;

class CustomTrustManager extends X509ExtendedTrustManager implements Closeable
{
    private long native_manager;

    private native void nativeFree();
    @Override
    public void close() {
        nativeFree();
    }

    private native void nativeCheckClientTrusted(X509Certificate[] chain, String authType);
    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) {
//        System.out.println("client none" + chain.length);
        //nativeCheckClientTrusted (chain, authType);
    }
    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType, SSLEngine engine) {
//        System.out.println("client engine" + chain.length);
        //nativeCheckClientTrusted (chain, authType);
    }
    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType, Socket socket) {
//        System.out.println("client socket" + chain.length);
        //nativeCheckClientTrusted (chain, authType);
    }

    private native void nativeCheckServerTrusted(X509Certificate[] chain, String authType);
    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) {
//        System.out.println("server none" + chain.length);
        //nativeCheckServerTrusted (chain, authType);
    }
    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType, SSLEngine engine) {
//        System.out.println("server engine " + chain.length);
        //nativeCheckClientTrusted (chain, authType);
    }
    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType, Socket socket) {
//        System.out.println("server socket" + chain.length);
        //nativeCheckClientTrusted (chain, authType);
    }

    private native X509Certificate[] nativeGetAcceptedIssuers();
    @Override
    public X509Certificate[] getAcceptedIssuers() {
        System.out.println("get accepted issuers");
        return nativeGetAcceptedIssuers();
    }
}
