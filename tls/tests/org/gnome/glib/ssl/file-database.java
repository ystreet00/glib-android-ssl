package org.gnome.glib.ssl;

class Test
{
    static {
        /* XXX: Only works with testing. Installed jar's need to place the
         * shared libraries in the resources and extract at runtime.
         */
        System.loadLibrary ("file-database");
    }

    private native static int nativeRun(String[] args);

    public static void main(String[] args) {
        System.exit(nativeRun(args));
    }
}
